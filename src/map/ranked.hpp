#ifndef _RANKED_HPP_
#define _RANKED_HPP_

#include "../common/cbasetypes.hpp"
#include "../common/core.hpp" // CORE_ST_LAST

#include "../map/battleground.hpp" // BG_WARFARE

// Max Auras
#define RANKED_MAX_AURA 15

extern int ranked_first_id;
extern int ranked_last_id;

// Ranked System
enum ranked_mode {
	RANK_M_CHAR,
	RANK_M_ACC
};

enum ranked_show {
	RANK_SHOW_MAP,
	RANK_SHOW_AURA
};

enum ranked_display {
	RANK_SHOW_NONE,
	RANK_SHOW_RANKED,
	RANK_SHOW_GUILD,
	RANK_SHOW_CLAN,
	RANK_SHOW_BG
};

enum ranked_update {
	RANK_CHAR,
	RANK_ACCOUNT,
	RANK_BOTH
};

enum ranked_reset {
	RANK_RST_LOGOUT,
	RANK_RST_MAP,
	RANK_RST_DIE
};

enum ranked_type {
	RANK_P_NONE,
	RANK_PPK_KILL,
	RANK_PPK_DEATH,
	RANK_PPD_GIVEN,
	RANK_PPD_TAKEN,
	RANK_PSD_GIVEN,
	RANK_PSD_TAKEN,
	RANK_PSU_SUCCESS,
	RANK_PSU_FAIL,
	RANK_PSS_SUCCESS,
	RANK_PSS_FAIL,
	RANK_P_OTHERS
};

enum ranked_target {
	RANK_TARGET_NONE,
	RANK_TARGET_TEAM,
	RANK_TARGET_ENEMY
};

struct ranked_data {
	unsigned int rank_id, back_id, next_id;
	int points;
	int auras[RANKED_MAX_AURA];

	// Fake Guild
	struct guild *g;
};

// Cliff
void clif_ranked_belonginfo(struct map_session_data *sd, struct guild *g);
void clif_remove_belonginfo(struct map_session_data *sd);

// Ranked
struct ranked_data *ranked_search(int rank_id);
int ranked_get_last_id();
int ranked_getbl_rank(struct block_list *bl, enum ranked_mode type);
int ranked_getbl_points(struct block_list *bl, enum ranked_mode type);
int ranked_getbl_status(struct block_list *bl);
int ranked_getbl_disable(struct block_list *bl);
int ranked_get_guild_id(struct block_list *bl);
bool ranked_change_emblem_sub(struct map_session_data *sd, bool reload_timer);
TIMER_FUNC(ranked_change_emblem);
bool ranked_points_gained(struct map_session_data *sd, int amount, enum ranked_type type, enum ranked_update status);
bool ranked_points_lost(struct map_session_data *sd, int amount, enum ranked_type type, enum ranked_update status);
bool ranked_set_rank(struct map_session_data *sd, int rank_id, int status);
void ranked_show_aura(struct block_list *bl);
void ranked_show_list(struct map_session_data *sd);
bool ranked_check_zone(int16 m);
bool ranked_show_map(int16 m, enum ranked_show type);
enum ranked_target ranked_check_target(struct block_list *src, struct block_list *target);
bool ranked_player_kill(struct block_list *src, struct block_list *target, int amount);
bool ranked_player_heal(struct block_list *bl, int hp, int sp);
bool ranked_damage(struct block_list *src, struct block_list *target, int64 damage);
bool ranked_skill_damage(struct block_list *src, struct block_list *target, int64 damage);
bool ranked_skill_use(struct block_list *src, int amount, enum ranked_type type);
void ranked_reset(struct map_session_data *sd, enum ranked_reset type);
void ranked_destroy_timer(struct map_session_data *sd);
void ranked_refresh_timer(struct map_session_data *sd, int timer);
void ranked_readdb(void);
void do_reload_ranked(void);
void do_init_ranked(void);
void do_final_ranked(void);
#endif /* _RANKED_HPP_ */
