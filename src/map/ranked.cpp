#ifndef _WIN32
#include <unistd.h>
#else
#include "../common/winapi.hpp"
#endif
#include "../common/cbasetypes.hpp"
#include "../common/malloc.hpp"
#include "../common/nullpo.hpp"
#include "../common/showmsg.hpp"
#include "../common/socket.hpp"
#include "../common/sql.hpp"
#include "../common/strlib.hpp"
#include "../common/timer.hpp"
#include "../common/utils.hpp"

#include "ranked.hpp"

#include "atcommand.hpp"
#include "battleground.hpp"
#include "battle.hpp"
#include "date.hpp"
#include "chrif.hpp"
#include "clif.hpp"
#include "elemental.hpp"
#include "guild.hpp"
#include "homunculus.hpp"
#include "map.hpp"
#include "mercenary.hpp"
#include "npc.hpp"
#include "pc.hpp"
#include "pet.hpp"
#include "script.hpp"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// Atcommand output
static char atcmd_output[CHAT_SIZE_MAX];
static char atcmd_player_name[NAME_LENGTH];

// Ranked System
static DBMap* ranked_db = NULL;
static unsigned int ranked_counter = 0;
int ranked_first_id = 0;
int ranked_last_id = 0;

#define	RANKED_RESERVED_ID	(SHRT_MAX-3) // (o valor � MAX-3 para n�o entrar em conflitos com a BG Warfare)

// Cliff
void clif_ranked_belonginfo(struct map_session_data *sd, struct guild *g)
{
	int fd;
	nullpo_retv(sd);
	//nullpo_retv(g);

	fd=sd->fd;
	WFIFOHEAD(fd,packet_len(0x16c));
	WFIFOW(fd,0)=0x16c;
	WFIFOL(fd,2)=g->guild_id;
	WFIFOL(fd,6)=g->emblem_id;
	WFIFOL(fd,10)=0;
	WFIFOB(fd,14)=1;
	WFIFOL(fd,15)=0;  // InterSID (unknown purpose)
	memcpy(WFIFOP(fd,19),g->name,NAME_LENGTH);
	WFIFOSET(fd,packet_len(0x16c));
}

void clif_remove_belonginfo(struct map_session_data *sd)
{
	int fd;
	nullpo_retv(sd);

	fd=sd->fd;
	WFIFOHEAD(fd,packet_len(0x16c));
	WFIFOW(fd,0)=0x16c;
	WFIFOL(fd,2)=0;
	WFIFOL(fd,6)=0;
	WFIFOL(fd,10)=0;
	WFIFOB(fd,14)=0;
	WFIFOL(fd,15)=0;  // InterSID (unknown purpose)
	memcpy(WFIFOP(fd,19),"",NAME_LENGTH);
	WFIFOSET(fd,packet_len(0x16c));
}

struct ranked_data* ranked_search(int rank_id)
{
	if( !rank_id ) return NULL;
	return (struct ranked_data *)idb_get(ranked_db, rank_id);
}

struct ranked_data* ranked_search_next(int rank_id)
{
	DBIterator* iter = db_iterator(ranked_db);
	struct ranked_data* rank = NULL;

	if( !rank_id ) return NULL;

	for( rank = (struct ranked_data*)dbi_first(iter); dbi_exists(iter); rank = (struct ranked_data*)dbi_next(iter))
	{
		if( rank->next_id == rank_id ) {
			dbi_destroy(iter);
			return rank;
		}
	}
	dbi_destroy(iter);
	return NULL;
}

int ranked_get_last_id()
{
	return ranked_last_id;
}

int ranked_getbl_rank(struct block_list *bl, enum ranked_mode type)
{
	nullpo_ret(bl);

	switch( bl->type )
	{
		case BL_PC:
			return type ? ((TBL_PC*)bl)->status.ranked_account.id : ((TBL_PC*)bl)->status.ranked_char.id;
		case BL_PET:
			if( ((TBL_PET*)bl)->master )
				return type ? ((TBL_PET*)bl)->master->status.ranked_account.id : ((TBL_PET*)bl)->master->status.ranked_char.id;
			break;
		case BL_MOB:
		{
			struct map_session_data *msd;
			struct mob_data *md = (TBL_MOB*)bl;
			if( md->special_state.ai && (msd = map_id2sd(md->master_id)) != NULL )
				return type ? msd->status.ranked_account.id : msd->status.ranked_char.id;
			break;
		}
		case BL_HOM:
			if( ((TBL_HOM*)bl)->master )
				return type ? ((TBL_HOM*)bl)->master->status.ranked_account.id : ((TBL_HOM*)bl)->master->status.ranked_char.id;
			break;
		case BL_MER:
			if (((TBL_MER*)bl)->master)
				return type ? ((TBL_MER*)bl)->master->status.ranked_account.id : ((TBL_MER*)bl)->master->status.ranked_char.id;
			break;
		case BL_ELEM:
			if (((TBL_ELEM*)bl)->master)
				return type ? ((TBL_ELEM*)bl)->master->status.ranked_account.id : ((TBL_ELEM*)bl)->master->status.ranked_char.id;
			break;
	}

	return 0;
}

int ranked_getbl_points(struct block_list *bl, enum ranked_mode type)
{
	nullpo_ret(bl);

	switch( bl->type )
	{
		case BL_PC:
			return type ? ((TBL_PC*)bl)->status.ranked_account.points.current : ((TBL_PC*)bl)->status.ranked_char.points.current;
		case BL_PET:
			if( ((TBL_PET*)bl)->master )
				return type ? ((TBL_PET*)bl)->master->status.ranked_account.points.current : ((TBL_PET*)bl)->master->status.ranked_char.points.current;
			break;
		case BL_MOB:
		{
			struct map_session_data *msd;
			struct mob_data *md = (TBL_MOB*)bl;
			if( md->special_state.ai && (msd = map_id2sd(md->master_id)) != NULL )
				return type ? msd->status.ranked_account.points.current : msd->status.ranked_char.points.current;
			break;
		}
		case BL_HOM:
			if( ((TBL_HOM*)bl)->master )
				return type ? ((TBL_HOM*)bl)->master->status.ranked_account.points.current : ((TBL_HOM*)bl)->master->status.ranked_char.points.current;
			break;
		case BL_MER:
			if( ((TBL_MER*)bl)->master )
				return type ? ((TBL_MER*)bl)->master->status.ranked_account.points.current : ((TBL_MER*)bl)->master->status.ranked_char.points.current;
			break;
		case BL_ELEM:
			if (((TBL_ELEM*)bl)->master)
				return type ? ((TBL_ELEM*)bl)->master->status.ranked_account.points.current : ((TBL_ELEM*)bl)->master->status.ranked_char.points.current;
			break;
	}

	return 0;
}

int ranked_getbl_status(struct block_list *bl)
{
	nullpo_ret(bl);

	switch( bl->type )
	{
		case BL_PC:
			return ((TBL_PC*)bl)->ranked.display_pos;
		case BL_PET:
			if( ((TBL_PET*)bl)->master )
				return ((TBL_PET*)bl)->master->ranked.display_pos;
			break;
		case BL_MOB:
		{
			struct map_session_data *msd;
			struct mob_data *md = (TBL_MOB*)bl;
			if( md->special_state.ai && (msd = map_id2sd(md->master_id)) != NULL )
				return msd->ranked.display_pos;
			break;
		}
		case BL_HOM:
			if( ((TBL_HOM*)bl)->master )
				return ((TBL_HOM*)bl)->master->ranked.display_pos;
			break;
		case BL_MER:
			if( ((TBL_MER*)bl)->master )
				return ((TBL_MER*)bl)->master->ranked.display_pos;
			break;
		case BL_ELEM:
			if (((TBL_ELEM*)bl)->master)
				return ((TBL_ELEM*)bl)->master->ranked.display_pos;
			break;
	}

	return -1;
}

int ranked_getbl_disable(struct block_list *bl)
{
	nullpo_ret(bl);

	switch( bl->type )
	{
		case BL_PC:
			return ((TBL_PC*)bl)->ranked.disable;
		case BL_PET:
			if( ((TBL_PET*)bl)->master )
				return ((TBL_PET*)bl)->master->ranked.disable;
			break;
		case BL_MOB:
		{
			struct map_session_data *msd;
			struct mob_data *md = (TBL_MOB*)bl;
			if( md->special_state.ai && (msd = map_id2sd(md->master_id)) != NULL )
				return msd->ranked.disable;
			break;
		}
		case BL_HOM:
			if( ((TBL_HOM*)bl)->master )
				return ((TBL_HOM*)bl)->master->ranked.disable;
			break;
		case BL_MER:
			if( ((TBL_MER*)bl)->master )
				return ((TBL_MER*)bl)->master->ranked.disable;
			break;
		case BL_ELEM:
			if (((TBL_ELEM*)bl)->master)
				return ((TBL_ELEM*)bl)->master->ranked.disable;
			break;
	}

	return 0;
}

int ranked_get_guild_id(struct block_list *bl)
{
	nullpo_ret(bl);

	switch (bl->type) {
	case BL_PC:
		return ((TBL_PC*)bl)->status.guild_id;
	case BL_PET:
		if (((TBL_PET*)bl)->master)
			return ((TBL_PET*)bl)->master->status.guild_id;
		break;
	case BL_MOB:
	{
		struct map_session_data *msd;
		struct mob_data *md = (struct mob_data *)bl;
		if (md->guardian_data)	//Guardian's guild [Skotlex]
			return md->guardian_data->guild_id;
		if (md->special_state.ai && (msd = map_id2sd(md->master_id)) != NULL)
			return msd->status.guild_id; //Alchemist's mobs [Skotlex]
	}
		break;
	case BL_HOM:
	  	if (((TBL_HOM*)bl)->master)
			return ((TBL_HOM*)bl)->master->status.guild_id;
		break;
	case BL_MER:
		if (((TBL_MER*)bl)->master)
			return ((TBL_MER*)bl)->master->status.guild_id;
		break;
	case BL_NPC:
	  	if (((TBL_NPC*)bl)->subtype == NPCTYPE_SCRIPT)
			return ((TBL_NPC*)bl)->u.scr.guild_id;
		break;
	case BL_SKILL:
		return ((TBL_SKILL*)bl)->group->guild_id;
	default:
		break;
	}
	return 0;
}

static int ranked_create(int rank_id, const char *name, int points, const char *emblem, int aura[], int a)
{
	struct ranked_data *rank;
	int i;
	FILE* fp = NULL;
	char path[256];

	ranked_counter++;
	CREATE(rank, struct ranked_data, 1);
	rank->rank_id = rank_id;
	rank->points = points;
	rank->back_id = 0;
	rank->next_id = 0;

	// Create Guild
	rank->g = (struct guild*)aCalloc(1, sizeof(struct guild));
	rank->g->emblem_id = 1;
	rank->g->guild_id = RANKED_RESERVED_ID - rank->rank_id;
	rank->g->guild_lv = 1;
	rank->g->max_member = 1;
	strncpy(rank->g->name, name, NAME_LENGTH);
	sprintf(path, "%s/creativesd/emblems/%s.ebm", db_path, emblem);
	if( (fp = fopen(path, "rb")) != NULL )
	{
		fseek(fp, 0, SEEK_END);
		rank->g->emblem_len = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		fread(rank->g->emblem_data, 1, rank->g->emblem_len, fp);
		fclose(fp);
		ShowStatus("Done reading '" CL_WHITE "%s" CL_RESET "' ranked emblem data file.\n", path);
	}

	for( i=0; i < a && i < battle_config.ranked_max_aura; i++ )
		rank->auras[i] = aura[i];

	idb_put(ranked_db, rank_id, rank);
	return true;
}

bool ranked_change_emblem_sub(struct map_session_data *sd, bool reload_timer)
{
	struct guild *g = NULL;
#ifdef BG_WARFARE
	struct battleground_data *bg = NULL;
#endif
	int timer = battle_config.ranked_show_timer, flag = RANK_SHOW_NONE;

	nullpo_ret(sd);

#ifdef BG_WARFARE
	// Battleground Warfare
	if (battle_config.bgw_enable && battle_config.ranked_bg_crossover && sd->bg_id && (bg = bg_team_search(sd->bg_id)) != NULL && bg->g != NULL) {
		g = bg->g;
		flag = RANK_SHOW_BG;
	}
	else
#endif
#if PACKETVER >= 20131223
	// Clan System
	if (sd->clan)
		flag = RANK_SHOW_CLAN;
	else
#endif
	// Guild System
	if (sd->status.guild_id && (g = guild_search(sd->status.guild_id)) != NULL)
		flag = RANK_SHOW_GUILD;
	else
		flag = RANK_SHOW_NONE;

	// Show Ranked System
	if( !sd->ranked.disable && (sd->ranked.display_pos != RANK_SHOW_RANKED || flag == RANK_SHOW_NONE) ) {
		struct ranked_data *rank;
		int rank_id = battle_config.ranked_show_mode ? sd->status.ranked_account.id : sd->status.ranked_char.id;
		if( ranked_show_map(sd->bl.m,RANK_SHOW_MAP) && rank_id && (rank = ranked_search(rank_id)) != NULL ) {
			g = rank->g;
			flag = RANK_SHOW_RANKED;
			timer = battle_config.ranked_duration_timer;
		}
	}

	// Set Names
	sd->ranked.display_pos = flag;
	clif_name_self(&sd->bl);
	clif_name_area(&sd->bl);
	if( g != NULL ) {
		clif_guild_emblem(sd, g);
		if( flag == RANK_SHOW_RANKED )
			clif_ranked_belonginfo(sd,g);
		else
			clif_guild_belonginfo(sd);
		clif_guild_emblem_area(&sd->bl);
	}
#if PACKETVER >= 20131223
	else if( sd->clan ) {
		clif_remove_belonginfo(sd);
		clif_clan_basicinfo(sd);
	}
#endif
	else
		clif_remove_belonginfo(sd); // Remove Belonginfo

	if( reload_timer )
		ranked_refresh_timer(sd, timer);
	return 1;
}

TIMER_FUNC(ranked_change_emblem)
{
	struct map_session_data *sd = map_id2sd(id);

	nullpo_ret(sd);

	if( tid == INVALID_TIMER )
		return 1;

	if( sd->ranked.display_timer != tid ) {
		ranked_refresh_timer(sd, battle_config.ranked_show_timer);
		return 1;
	}

	ranked_change_emblem_sub(sd,true);
	return 1;
}

bool ranked_points_gained(struct map_session_data *sd, int amount, enum ranked_type type, enum ranked_update status)
{
	struct ranked_data *rank, *nrank = NULL;
	int rank_id, last_id;
	char output[CHAT_SIZE_MAX];

	nullpo_ret(sd);

	if( amount <= 0 )
		return 0;

	// Char Points
	if( status == RANK_CHAR || status == RANK_BOTH ) {
		sd->status.ranked_char.points.current += amount;
		sd->status.ranked_char.points.gained += amount;

		if( !battle_config.ranked_show_mode )
			last_id = sd->status.ranked_char.id;

		while( sd->status.ranked_char.points.current > 0 ) {
			// Is Last ID
			if (sd->status.ranked_char.id == ranked_last_id) {
				sd->status.ranked_char.points.current = 0;
				break;
			}

			if( ranked_first_id && sd->status.ranked_char.id <= 0 && (nrank = ranked_search(ranked_first_id)) != NULL ) {
				if( sd->status.ranked_char.points.current < (unsigned int)nrank->points )
					break;

				sd->status.ranked_char.id = nrank->rank_id;
				sd->status.ranked_char.points.current -= nrank->points;
			}
			else if( (rank = ranked_search(sd->status.ranked_char.id)) != NULL ) {
				if( (nrank = ranked_search(rank->next_id)) != NULL ) {
					if( sd->status.ranked_char.points.current < (unsigned int)nrank->points )
						break;

					sd->status.ranked_char.id = nrank->rank_id;
					sd->status.ranked_char.points.current -= nrank->points;
				}
				else if( ranked_last_id && (nrank = ranked_search(ranked_last_id))) {
					sd->status.ranked_char.id = nrank->rank_id;
					sd->status.ranked_char.points.current = 0;
				}
				else {
					sd->status.ranked_char.id = 0;
					sd->status.ranked_char.points.current = 0;
				}
			}
			else if( ranked_last_id && (nrank = ranked_search(ranked_last_id))) {
				sd->status.ranked_char.id = nrank->rank_id;
				sd->status.ranked_char.points.current = 0;
			}
			else {
				sd->status.ranked_char.id = 0;
				sd->status.ranked_char.points.current = 0;
			}
		}
	}
		
	// Account Points
	if( status == RANK_ACCOUNT || status == RANK_BOTH ) {
		sd->status.ranked_account.points.current += amount;
		sd->status.ranked_account.points.gained += amount;

		if( battle_config.ranked_show_mode )
			last_id = sd->status.ranked_account.id;

		while( sd->status.ranked_account.points.current > 0 ) {
			// Is Last ID
			if (sd->status.ranked_account.id == ranked_last_id) {
				sd->status.ranked_account.points.current = 0;
				break;
			}

			if( ranked_first_id && sd->status.ranked_account.id <= 0 && (nrank = ranked_search(ranked_first_id)) != NULL ) {
				if( sd->status.ranked_char.points.current < (unsigned int)nrank->points )
					break;

				sd->status.ranked_account.id = nrank->rank_id;
				sd->status.ranked_account.points.current -= nrank->points;
			}
			else if( (rank = ranked_search(sd->status.ranked_account.id)) != NULL ) {
				if( (nrank = ranked_search(rank->next_id)) != NULL ) {
					if( sd->status.ranked_account.points.current < (unsigned int)nrank->points )
						break;

					sd->status.ranked_account.id = nrank->rank_id;
					sd->status.ranked_account.points.current -= nrank->points;
				}
				else if( ranked_last_id && (nrank = ranked_search(ranked_last_id))) {
					sd->status.ranked_account.id = nrank->rank_id;
					sd->status.ranked_account.points.current = 0;
				}
				else {
					sd->status.ranked_account.id = 0;
					sd->status.ranked_account.points.current = 0;
				}
			}
			else if( ranked_last_id && (nrank = ranked_search(ranked_last_id))) {
				sd->status.ranked_account.id = nrank->rank_id;
				sd->status.ranked_account.points.current = 0;
			}
			else {
				sd->status.ranked_account.id = 0;
				sd->status.ranked_account.points.current = 0;
			}
		}
	}
	
	// Message Points Lost
	if( type ) {
		switch( type ) {
		case RANK_PPK_KILL:
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+25), amount);
			break;
		case RANK_PSS_SUCCESS:
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+27), amount);
			break;
		case RANK_PPD_GIVEN:
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+31), amount);
			break;
		case RANK_PSD_GIVEN:
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+33), amount);
			break;
		default:
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+29), amount);
			break;
		}
		clif_messagecolor(&sd->bl, color_table[COLOR_DEFAULT], output, false, SELF);
	}

	rank_id = battle_config.ranked_show_mode ? sd->status.ranked_account.id : sd->status.ranked_char.id;
	if( last_id != rank_id )
	{
		rank = ranked_search(rank_id);
		if( rank == NULL ) {
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+2));
			clif_messagecolor(&sd->bl, color_table[COLOR_RED], output, false, SELF);
		}
		else {
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+3), rank->g->name);
			clif_messagecolor(&sd->bl, color_table[COLOR_DEFAULT], output, false, SELF);
		}
		
		// Update Rank
		if( rank_id <= 0 )
			clif_remove_belonginfo(sd);

		if( ranked_show_map(sd->bl.m,RANK_SHOW_AURA) )
			ranked_show_aura(&sd->bl);

		sd->ranked.display_pos = RANK_SHOW_NONE;
		ranked_refresh_timer(sd, battle_config.ranked_show_timer);

		// Special Effect
		clif_specialeffect(&sd->bl, EF_ANGEL2, AREA);
	}

	return 1;
}

bool ranked_points_lost(struct map_session_data *sd, int amount, enum ranked_type type, enum ranked_update status)
{
	struct ranked_data *rank, *nrank = NULL;
	int rank_id, last_id;
	char output[CHAT_SIZE_MAX];

	nullpo_ret(sd);

	if( amount <= 0 )
		return 0;

	// Char Points
	if( status == RANK_CHAR || status == RANK_BOTH ) {
		sd->status.ranked_char.points.current -= amount;
		sd->status.ranked_char.points.lost += amount;

		if( !battle_config.ranked_show_mode )
			last_id = sd->status.ranked_char.id;

		while( sd->status.ranked_char.points.current < 0 ) {
			// Is First ID
			if( sd->status.ranked_char.id <= 0 || sd->status.ranked_char.id == ranked_first_id ) {
				sd->status.ranked_char.points.current = 0;
				break;
			}

			if( (rank = ranked_search(sd->status.ranked_char.id)) != NULL ) {
				if( (nrank = ranked_search(rank->back_id)) != NULL ) {
					sd->status.ranked_char.id = nrank->rank_id;
					sd->status.ranked_char.points.current += nrank->points;
				}
				else if( ranked_first_id && (nrank = ranked_search(ranked_first_id))) {
					sd->status.ranked_char.id = nrank->rank_id;
					sd->status.ranked_char.points.current += nrank->points;
				}
				else {
					sd->status.ranked_char.id = 0;
					sd->status.ranked_char.points.current = 0;
				}
			}
			else if( ranked_first_id && (nrank = ranked_search(ranked_first_id))) {
				sd->status.ranked_char.id = nrank->rank_id;
				sd->status.ranked_char.points.current += nrank->points;
			}
			else {
				sd->status.ranked_char.id = 0;
				sd->status.ranked_char.points.current = 0;
			}
		}
	}
		
	// Account Points
	if( status == RANK_ACCOUNT || status == RANK_BOTH ) {
		sd->status.ranked_account.points.current -= amount;
		sd->status.ranked_account.points.lost += amount;

		if( battle_config.ranked_show_mode )
			last_id = sd->status.ranked_account.id;

		while( sd->status.ranked_account.points.current < 0 ) {
			// Is First ID
			if( sd->status.ranked_account.id <= 0 || sd->status.ranked_account.id == ranked_first_id ) {
				sd->status.ranked_account.points.current = 0;
				break;
			}

			if( (rank = ranked_search(sd->status.ranked_account.id)) != NULL ) {
				if( (nrank = ranked_search(rank->back_id)) != NULL ) {
					sd->status.ranked_account.id = nrank->rank_id;
					sd->status.ranked_account.points.current += nrank->points;
				}
				else if( ranked_first_id && (nrank = ranked_search(ranked_first_id))) {
					sd->status.ranked_account.id = nrank->rank_id;
					sd->status.ranked_account.points.current += nrank->points;
				}
				else {
					sd->status.ranked_account.id = 0;
					sd->status.ranked_account.points.current = 0;
				}
			}
			else if( ranked_first_id && (nrank = ranked_search(ranked_first_id))) {
				sd->status.ranked_account.id = nrank->rank_id;
				sd->status.ranked_account.points.current += nrank->points;
			}
			else {
				sd->status.ranked_account.id = 0;
				sd->status.ranked_account.points.current = 0;
			}
		}
	}

	// Message Points Lost
	if( type ) {
		switch( type ) {
		case RANK_PPK_DEATH:
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+26), amount);
			break;
		case RANK_PSS_FAIL:
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+28), amount);
			break;
		case RANK_PPD_TAKEN:
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+32), amount);
			break;
		case RANK_PSD_TAKEN:
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+34), amount);
			break;
		default:
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+30), amount);
			break;
		}
		clif_messagecolor(&sd->bl, color_table[COLOR_RED], output, false, SELF);
	}

	rank_id = battle_config.ranked_show_mode ? sd->status.ranked_account.id : sd->status.ranked_char.id;
	if( last_id != rank_id )
	{
		rank = ranked_search(rank_id);
		if( rank == NULL )
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+2));
		else
			sprintf(output, msg_txt(sd, battle_config.ranked_msg_txt+4), rank->g->name);
		clif_messagecolor(&sd->bl, color_table[COLOR_RED], output, false, SELF);

		// Update Rank
		if( rank_id <= 0 )
			clif_remove_belonginfo(sd);

		if( ranked_show_map(sd->bl.m,RANK_SHOW_AURA) )
			ranked_show_aura(&sd->bl);

		sd->ranked.display_pos = RANK_SHOW_NONE;
		ranked_refresh_timer(sd, battle_config.ranked_show_timer);

		// Special Effect
		clif_specialeffect(&sd->bl, EF_DECAGILITY, AREA);
	}
	return 1;
}

bool ranked_set_rank(struct map_session_data *sd, int rank_id, int status)
{
	struct ranked_data *rank = ranked_search(rank_id);
	int new_points;

	nullpo_ret(sd);

	if( !rank_id || rank == NULL )
		new_points = 0;
	else
		new_points = rank->points;

	// Update Rank
	if( rank_id <= 0 )
		clif_remove_belonginfo(sd);

	if( status&1 ) {
		sd->status.ranked_char.id = rank_id;
		sd->status.ranked_char.points.current = new_points;
	}

	if( status&2 ) {
		sd->status.ranked_account.id = rank_id;
		sd->status.ranked_account.points.current = new_points;
	}

	if( ranked_show_map(sd->bl.m,RANK_SHOW_AURA) )
		ranked_show_aura(&sd->bl);

	if( status )
		sd->ranked.display_pos = RANK_SHOW_NONE;
	ranked_refresh_timer(sd, battle_config.ranked_show_timer);

	// Special Effect
	clif_specialeffect(&sd->bl, EF_ANGEL2, AREA);
	return 1;
}

void ranked_show_aura(struct block_list *bl)
{
	struct ranked_data *rank;
	struct status_change *sc = NULL;
	int i, current_rank = ranked_getbl_rank(bl, (enum ranked_mode)battle_config.ranked_show_mode);

	if( bl->type != BL_PC )
		return;

	sc = status_get_sc(bl);
	if( sc->option&(OPTION_HIDE|OPTION_CLOAK|OPTION_CHASEWALK|OPTION_INVISIBLE) ) // ranger ->|| sc->data[SC_CAMOUFLAGE] )
		return;

	// Show Aura
	if( (rank = ranked_search(current_rank)) != NULL && ranked_show_map(bl->m, RANK_SHOW_AURA)) {
		for( i = 0; i < battle_config.ranked_max_aura; i++ ) {
			if( rank->auras[i] > 0 )
				clif_specialeffect(bl, rank->auras[i], AREA);
		}
	}
}

void ranked_show_list(struct map_session_data *sd)
{
	DBIterator* iter = db_iterator(ranked_db);
	struct ranked_data* rank;
	char output[200];

	nullpo_retv(sd);

	clif_displaymessage(sd->fd, msg_txt(sd,battle_config.ranked_msg_txt));
	for( rank = (struct ranked_data*)dbi_first(iter); dbi_exists(iter); rank = (struct ranked_data*)dbi_next(iter) )
	{
		sprintf(output, msg_txt(sd,battle_config.ranked_msg_txt+1), rank->rank_id, rank->g->name, rank->points);
		clif_displaymessage(sd->fd, output);
	}
	dbi_destroy(iter);
}

bool ranked_check_zone(int16 m)
{
	struct map_data *mapdata = map_getmapdata(m);

	if( mapdata == NULL )
		return false;
	if( mapdata->flag[MF_NORANKED] )
		return false;
	if( mapdata->flag[MF_RANKED] )
		return true;
	if( mapdata->flag[MF_PVP] && battle_config.ranked_mode&0x01 )
		return true;
	if( mapdata->flag[MF_GVG] && battle_config.ranked_mode&10 )
		return true;
	if( mapdata->flag[MF_BATTLEGROUND] && battle_config.ranked_mode&0x100 )
		return true;
	if( mapdata->flag[MF_GVG_CASTLE] && battle_config.ranked_mode&0x200 )
		return true;
	if( mapdata->flag[MF_GVG_CASTLE] && (agit_flag || agit2_flag || agit3_flag) && battle_config.ranked_mode&0x400 )
		return true;
	return false;
}

bool ranked_show_map(int16 m, enum ranked_show type)
{
	struct map_data *mapdata = map_getmapdata(m);

	if( mapdata == NULL )
		return false;
	if( type == RANK_SHOW_MAP ) {
		if( !battle_config.ranked_show_maps )
			return false;
		if( !mapdata->flag[MF_PVP] && !mapdata_flag_gvg2(mapdata) && !mapdata->flag[MF_BATTLEGROUND] && battle_config.ranked_show_maps&0x1 )
			return true;
		if( mapdata->flag[MF_PVP] && battle_config.ranked_show_maps&0x10 )
			return true;
		if( mapdata->flag[MF_GVG] && !mapdata->flag[MF_GVG_CASTLE] && battle_config.ranked_show_maps&0x100 )
			return true;
		if( mapdata->flag[MF_BATTLEGROUND] && battle_config.ranked_show_maps&0x200 )
			return true;
		if( mapdata->flag[MF_GVG_CASTLE] && battle_config.ranked_show_maps&0x400 )
			return true;
		if( mapdata->flag[MF_GVG_CASTLE] && (agit_flag || agit2_flag || agit3_flag) && battle_config.ranked_show_maps&0x1000 )
			return true;
	}
	else if( type == RANK_SHOW_AURA ) {
		if( !battle_config.ranked_show_aura )
			return false;
		if( !mapdata->flag[MF_PVP] && !mapdata_flag_gvg2(mapdata) && !mapdata->flag[MF_BATTLEGROUND] && battle_config.ranked_show_aura&0x1 )
			return true;
		if( mapdata->flag[MF_PVP] && battle_config.ranked_show_aura&0x10 )
			return true;
		if( mapdata->flag[MF_GVG] && !mapdata->flag[MF_GVG_CASTLE] && battle_config.ranked_show_aura&0x100 )
			return true;
		if( mapdata->flag[MF_BATTLEGROUND] && battle_config.ranked_show_aura&0x200 )
			return true;
		if( mapdata->flag[MF_GVG_CASTLE] && battle_config.ranked_show_aura&0x400 )
			return true;
		if( mapdata->flag[MF_GVG_CASTLE] && (agit_flag || agit2_flag || agit3_flag) && battle_config.ranked_show_aura&0x1000 )
			return true;
	}
	return false;
}

// Ranked (Rank)
enum ranked_target ranked_check_target(struct block_list *src, struct block_list *target)
{
	struct map_data *mapdata;
	int team_id1, team_id2;

	if( src == NULL || target == NULL )
		return RANK_TARGET_NONE;

	mapdata = map_getmapdata(src->m);
	if( mapdata == NULL )
		return RANK_TARGET_NONE;

	if( map_getmapflag(mapdata->m,MF_NORANKED) )
		return RANK_TARGET_NONE;

	if( (map_getmapflag(mapdata->m,MF_BATTLEGROUND) && (team_id1 = bg_team_get_id(src)) &&  (team_id2 = bg_team_get_id(target)) && team_id1 == team_id2) ||
		(mapdata_flag_gvg2(mapdata) && (team_id1 = status_get_guild_id(src,true)) && (team_id2 = status_get_guild_id(target,true)) && team_id1 == team_id2) ||
		(map_getmapflag(mapdata->m,MF_PVP) && !map_getmapflag(mapdata->m,MF_PVP_NOGUILD) && (team_id1 = status_get_guild_id(src,true)) && (team_id2 = status_get_guild_id(target,true)) && team_id1 == team_id2) ||
		(map_getmapflag(mapdata->m,MF_PVP) && !map_getmapflag(mapdata->m,MF_PVP_NOPARTY) && (team_id1 = status_get_guild_id(src,true)) && (team_id2 = status_get_party_id(target)) && team_id1 == team_id2)
	) {
		return RANK_TARGET_TEAM;
	}

	//if( !mapdata_flag_gvg2(mapdata) && map_getmapflag(mapdata->m,MF_PVP) && map_getmapflag(mapdata->m,MF_PVP_NOPARTY) && map_getmapflag(mapdata->m,MF_PVP_NOGUILD) )
	return RANK_TARGET_ENEMY;
}

bool ranked_player_kill(struct block_list *src, struct block_list *target, int amount)
{
	struct block_list *s_bl, *t_bl;
	struct map_session_data *sd, *tsd;
	unsigned int points = 0, val;

	if( amount <= 0 || src == NULL || target == NULL )
		return 0;

	if( (s_bl = battle_get_master(src)) == NULL )
		s_bl = src;

	if( (t_bl = battle_get_master(target)) == NULL )
		t_bl = target;

	if( s_bl->type != BL_PC && t_bl->type != BL_PC )
		return 0;

	sd = BL_CAST(BL_PC, s_bl);
	tsd = BL_CAST(BL_PC, t_bl);

	if( sd != NULL ) {
		val = map_getmapflag(sd->bl.m, MF_RANKED_PK_GAINED);
		points = val > 0 ? val : battle_config.ranked_pk_gained;
		increment_limit(sd->status.ranked_char.player.kills, (unsigned int)amount, UINT_MAX);
		increment_limit(sd->status.ranked_account.player.kills, (unsigned int)amount, UINT_MAX);
		ranked_points_gained(sd, points, RANK_PPK_KILL, RANK_BOTH);
	}

	if( tsd != NULL ) {
		val = map_getmapflag(tsd->bl.m, MF_RANKED_PK_LOST);
		points = val > 0 ? val : battle_config.ranked_pk_lost;
		increment_limit(tsd->status.ranked_char.player.deaths, (unsigned int)amount, UINT_MAX);
		increment_limit(tsd->status.ranked_account.player.deaths, (unsigned int)amount, UINT_MAX);
		ranked_points_lost(tsd, points, RANK_PPK_DEATH, RANK_BOTH);
	}
	return 1;
}

bool ranked_player_heal(struct block_list *bl, int hp, int sp)
{
	struct block_list *s_bl;
	struct map_session_data *sd;

	nullpo_ret(bl);

	if( (hp <= 0 && sp <= 0) || bl == NULL )
		return 0;

	if( (s_bl = battle_get_master(bl)) == NULL )
		s_bl = bl;

	if( s_bl->type != BL_PC || (sd = BL_CAST(BL_PC, s_bl)) == NULL )
		return 0;

	if( hp )
		increment_limit(sd->status.ranked_char.heal.hp , (unsigned int)hp, UINT_MAX);
	if( sp )
		increment_limit(sd->status.ranked_account.heal.sp, (unsigned int)sp, UINT_MAX);
	return 1;
}

bool ranked_damage(struct block_list *src, struct block_list *target, int64 damage)
{
	struct block_list *s_bl, *t_bl;
	struct map_session_data *sd, *tsd;
	unsigned int points = 0, total = 0;
	unsigned int val1, val2;

	if( damage <= 0 || src == NULL || target == NULL )
		return 0;

	if( (s_bl = battle_get_master(src)) == NULL )
		s_bl = src;

	if( (t_bl = battle_get_master(target)) == NULL )
		t_bl = target;

	if( s_bl->type != BL_PC && t_bl->type != BL_PC )
		return 0;

	sd = BL_CAST(BL_PC, s_bl);
	tsd = BL_CAST(BL_PC, t_bl);

	if( sd != NULL ) {
		val1 = map_getmapflag(sd->bl.m, MF_RANKED_PD_GIVEN);
		val2 = map_getmapflag(sd->bl.m, MF_RANKED_PD_GAINED);
		val1 = val1 > 0 ? val1 : battle_config.ranked_pd_given;
		val2 = val2 > 0 ? val2 : battle_config.ranked_pd_gained;
		increment_limit(sd->ranked.player_damage_given, (unsigned int)damage, UINT_MAX);
		increment_limit(sd->status.ranked_char.player.damage_given, (unsigned int)damage, UINT_MAX);
		increment_limit(sd->status.ranked_account.player.damage_given, (unsigned int)damage, UINT_MAX);
		if( sd->ranked.player_damage_given >= val1 ) {
			points = sd->ranked.player_damage_given/val1;
			total = points*val1;
			decrement_limit(sd->ranked.player_damage_given, (unsigned int)(total), UINT_MAX);
			decrement_limit(sd->status.ranked_char.player.damage_given, (unsigned int)(total), UINT_MAX);
			decrement_limit(sd->status.ranked_account.player.damage_given, (unsigned int)(total), UINT_MAX);
			ranked_points_gained(sd, (points*val2), RANK_PPD_GIVEN, RANK_BOTH);
		}
	}

	if( tsd != NULL ) {
		val1 = map_getmapflag(tsd->bl.m, MF_RANKED_PD_TAKEN);
		val2 = map_getmapflag(tsd->bl.m, MF_RANKED_PD_LOST);
		val1 = val1 > 0 ? val1 : battle_config.ranked_pd_taken;
		val2 = val2 > 0 ? val2 : battle_config.ranked_pd_lost;
		increment_limit(tsd->ranked.player_damage_taken, (unsigned int)damage, UINT_MAX);
		increment_limit(tsd->status.ranked_char.player.damage_taken, (unsigned int)damage, UINT_MAX);
		increment_limit(tsd->status.ranked_account.player.damage_taken, (unsigned int)damage, UINT_MAX);
		if( tsd->ranked.player_damage_taken >= val1 ) {
			points = tsd->ranked.player_damage_taken/val1;
			total = points*val1;
			decrement_limit(tsd->ranked.player_damage_taken, (unsigned int)(total), UINT_MAX);
			decrement_limit(tsd->status.ranked_char.player.damage_taken, (unsigned int)(total), UINT_MAX);
			decrement_limit(tsd->status.ranked_account.player.damage_taken, (unsigned int)(total), UINT_MAX);
			ranked_points_lost(tsd, (points*val2), RANK_PPD_TAKEN, RANK_BOTH);
		}
	}
	return 1;
}

bool ranked_skill_damage(struct block_list *src, struct block_list *target, int64 damage)
{
	struct block_list *s_bl, *t_bl;
	struct map_session_data *sd, *tsd;
	unsigned int points = 0, total = 0;
	unsigned int val1, val2;

	if( damage <= 0 || src == NULL || target == NULL )
		return 0;

	if( (s_bl = battle_get_master(src)) == NULL )
		s_bl = src;

	if( (t_bl = battle_get_master(target)) == NULL )
		t_bl = target;

	if( s_bl->type != BL_PC && t_bl->type != BL_PC )
		return 0;

	sd = BL_CAST(BL_PC, s_bl);
	tsd = BL_CAST(BL_PC, t_bl);

	if( sd != NULL ) {
		val1 = map_getmapflag(sd->bl.m, MF_RANKED_SD_GIVEN);
		val2 = map_getmapflag(sd->bl.m, MF_RANKED_SD_GAINED);
		val1 = val1 > 0 ? val1 : battle_config.ranked_sd_given;
		val2 = val2 > 0 ? val2 : battle_config.ranked_sd_gained;
		increment_limit(sd->ranked.skill_damage_given, (unsigned int)damage, UINT_MAX);
		increment_limit(sd->status.ranked_char.skill.damage_given, (unsigned int)damage, UINT_MAX);
		increment_limit(sd->status.ranked_account.skill.damage_given, (unsigned int)damage, UINT_MAX);
		if( sd->ranked.skill_damage_given >= val1 ) {
			points = sd->ranked.skill_damage_given/val1;
			total = points*val1;
			decrement_limit(sd->ranked.skill_damage_given, (unsigned int)(total), UINT_MAX);
			decrement_limit(sd->status.ranked_char.skill.damage_given, (unsigned int)(total), UINT_MAX);
			decrement_limit(sd->status.ranked_account.skill.damage_given, (unsigned int)(total), UINT_MAX);
			ranked_points_gained(sd, (points*val2), RANK_PSD_GIVEN, RANK_BOTH);
		}
	}

	if( tsd != NULL ) {
		val1 = map_getmapflag(tsd->bl.m, MF_RANKED_SD_TAKEN);
		val2 = map_getmapflag(tsd->bl.m, MF_RANKED_SD_LOST);
		val1 = val1 > 0 ? val1 : battle_config.ranked_sd_taken;
		val2 = val2 > 0 ? val2 : battle_config.ranked_sd_lost;
		increment_limit(tsd->ranked.skill_damage_taken, (unsigned int)damage, UINT_MAX);
		increment_limit(tsd->status.ranked_char.skill.damage_taken, (unsigned int)damage, UINT_MAX);
		increment_limit(tsd->status.ranked_account.skill.damage_taken, (unsigned int)damage, UINT_MAX);
		if( tsd->ranked.skill_damage_taken >= val1 ) {
			points = tsd->ranked.skill_damage_taken/val1;
			total = points*val1;
			decrement_limit(tsd->ranked.skill_damage_taken, (unsigned int)(total), UINT_MAX);
			decrement_limit(tsd->status.ranked_char.skill.damage_taken, (unsigned int)(total), UINT_MAX);
			decrement_limit(tsd->status.ranked_account.skill.damage_taken, (unsigned int)(total), UINT_MAX);
			ranked_points_lost(tsd, (points*val2), RANK_PSD_TAKEN, RANK_BOTH);
		}
	}
	return 1;
}

bool ranked_skill_use(struct block_list *src, int amount, enum ranked_type type)
{
	struct block_list *s_bl;
	struct map_session_data *sd;

	if( amount <= 0 || src == NULL )
		return 0;

	if( (s_bl = battle_get_master(src)) == NULL )
		s_bl = src;

	if( s_bl->type != BL_PC )
		return 0;

	if( (sd = BL_CAST(BL_PC, s_bl)) == NULL )
		return 0;

	switch(type) {
		case RANK_PSS_SUCCESS:
			{
				unsigned int points = 0, total = 0;
				unsigned int val1, val2;

				val1 = map_getmapflag(sd->bl.m, MF_RANKED_SS_SUCCESS);
				val2 = map_getmapflag(sd->bl.m, MF_RANKED_SS_GAINED);
				val1 = val1 > 0 ? val1 : battle_config.ranked_ss_success;
				val2 = val2 > 0 ? val2 : battle_config.ranked_ss_gained;
				increment_limit(sd->ranked.skill_support_success, (unsigned int)amount, UINT_MAX);
				increment_limit(sd->status.ranked_char.skill.support_success, (unsigned int)amount, UINT_MAX);
				increment_limit(sd->status.ranked_account.skill.support_success, (unsigned int)amount, UINT_MAX);
				if( sd->ranked.skill_support_success >= val1 ) {
					points = sd->ranked.skill_support_success/val1;
					total = points*val1;
					decrement_limit(sd->ranked.skill_support_success, (unsigned int)(total), UINT_MAX);
					decrement_limit(sd->status.ranked_char.skill.support_success, (unsigned int)(total), UINT_MAX);
					decrement_limit(sd->status.ranked_account.skill.support_success, (unsigned int)(total), UINT_MAX);
					ranked_points_gained(sd, (points*val2), RANK_PSS_SUCCESS, RANK_BOTH);
				}
			}
			break;
		case RANK_PSS_FAIL:
			{
				unsigned int points = 0, total = 0;
				unsigned int val1, val2;

				val1 = map_getmapflag(sd->bl.m, MF_RANKED_SS_FAIL);
				val2 = map_getmapflag(sd->bl.m, MF_RANKED_SS_LOST);
				val1 = val1 > 0 ? val1 : battle_config.ranked_ss_fail;
				val2 = val2 > 0 ? val2 : battle_config.ranked_ss_lost;
				increment_limit(sd->ranked.skill_support_fail, (unsigned int)amount, UINT_MAX);
				increment_limit(sd->status.ranked_char.skill.support_fail, (unsigned int)amount, UINT_MAX);
				increment_limit(sd->status.ranked_account.skill.support_fail, (unsigned int)amount, UINT_MAX);
				if( sd->ranked.skill_support_fail >= val1 ) {
					points = sd->ranked.skill_support_fail/val1;
					total = points*val1;
					decrement_limit(sd->ranked.skill_support_fail, (unsigned int)(total), UINT_MAX);
					decrement_limit(sd->status.ranked_char.skill.support_fail, (unsigned int)(total), UINT_MAX);
					decrement_limit(sd->status.ranked_account.skill.support_fail, (unsigned int)(total), UINT_MAX);
					ranked_points_lost(sd, (points*val2), RANK_PSS_FAIL, RANK_BOTH);
				}
			}
			break;
		case RANK_PSU_SUCCESS:
			increment_limit(sd->status.ranked_char.skill.success, (unsigned int)amount, UINT_MAX);
			increment_limit(sd->status.ranked_account.skill.success, (unsigned int)amount, UINT_MAX);
			break;
		case RANK_PSU_FAIL:
			increment_limit(sd->status.ranked_char.skill.fail, (unsigned int)amount, UINT_MAX);
			increment_limit(sd->status.ranked_account.skill.fail, (unsigned int)amount, UINT_MAX);
			break;
		default:
			return 0;
	}
	return 1;
}

void ranked_reset(struct map_session_data *sd, enum ranked_reset type)
{
	nullpo_retv(sd);

	if( type == RANK_RST_LOGOUT || (type == RANK_RST_MAP && battle_config.ranked_pd_reset&0x01) || (type == RANK_RST_DIE && battle_config.ranked_pd_reset&0x10) ) {
		sd->ranked.player_damage_given = 0;
		sd->ranked.player_damage_taken = 0;
	}

	if( type == RANK_RST_LOGOUT || (type == RANK_RST_MAP && battle_config.ranked_sd_reset&0x01) || (type == RANK_RST_DIE && battle_config.ranked_sd_reset&0x10) ) {
		sd->ranked.skill_damage_given = 0;
		sd->ranked.skill_damage_taken = 0;
	}

	if( type == RANK_RST_LOGOUT || (type == RANK_RST_MAP && battle_config.ranked_ss_reset&0x01) || (type == RANK_RST_DIE && battle_config.ranked_ss_reset&0x10) ) {
		sd->ranked.skill_support_success = 0;
		sd->ranked.skill_support_fail = 0;
	}
}

void ranked_destroy_timer(struct map_session_data *sd)
{
	if( sd->ranked.display_timer != INVALID_TIMER )
		delete_timer(sd->ranked.display_timer, ranked_change_emblem);
	sd->ranked.display_timer = INVALID_TIMER;
}

void ranked_refresh_timer(struct map_session_data *sd, int timer)
{
	ranked_destroy_timer(sd);
	sd->ranked.display_timer = add_timer(gettick()+timer,ranked_change_emblem,sd->bl.id,0);
}

static void ranked_reorder()
{
	DBIterator* iter = db_iterator(ranked_db);
	struct ranked_data* rank;
	struct ranked_data *nrank;
	int back_id, next_id;

	for(rank = (struct ranked_data*)dbi_first(iter); dbi_exists(iter); rank = (struct ranked_data*)dbi_next(iter))
	{
		back_id = rank->rank_id - 1;
		next_id = rank->rank_id + 1;

		while( back_id > 0 ) {
			if( (nrank = ranked_search(back_id)) != NULL ) {
				rank->back_id = back_id;
				break;
			}
			back_id--;
		}

		if( back_id < 0 )
			rank->back_id = 0;

		while( next_id <= ranked_last_id ) {
			if( (nrank = ranked_search(next_id)) != NULL ) {
				rank->next_id = next_id;
				break;
			}
			next_id++;
		}

		// Last ID
		if( next_id > ranked_last_id )
			rank->next_id = 0;
	}
	dbi_destroy(iter);
}

static bool ranked_parse_dbrow(char** str, const char* source, int line)
{
	struct ranked_data *rank;
	int rank_id, i, a = 0;
	int points;
	char name[NAME_LENGTH];
	char path[1024];
	char *aura_tmp;
	const char *emblem;
	int auras[15];

	rank_id = atoi(str[0]);
	safestrncpy(name, str[1], sizeof(name));
	points = atoi(str[2]);
	emblem = str[3];

	aura_tmp = str[4];
	aura_tmp = strchr(aura_tmp+1,'#');
	for( i=0; i < battle_config.ranked_max_aura && aura_tmp; i++ )
	{
		if( !sscanf(aura_tmp, "%d", &auras[a]) && !sscanf(aura_tmp, "#%d", &auras[a]) )
		{
			ShowWarning("ranked_parse_dbrow: Error parsing aura effects in line %d of \"%s\" (rank id: %d), skipping effect\n", line, source, rank_id);
			aura_tmp = strchr(aura_tmp+1,'#');
			continue;
		}
		aura_tmp = strchr(aura_tmp+1,'#');
		a++;
	}

	if( rank_id <= 0 ) {
		ShowWarning("ranked_parse_dbrow: Invalid  ID %d in line %d of \"%s\", skipping.\n", rank_id, line, source);
		return false;
	}

	rank = ranked_search(rank_id);

	if( rank != NULL ) {
		ShowWarning("ranked_parse_dbrow: Duplicate ID %d in line %d of \"%s\", skipping.\n", rank_id, line, source);
		return false;
	}

	if( strlen(name) <= 0 )
	{
		ShowWarning("ranked_parse_dbrow: No name defined in line %d of \"%s\" (rank id: %d), skipping\n", line, source, rank_id);
		return false;
	}

	if( points <= 0 )
	{
		ShowWarning("ranked_parse_dbrow: No points defined in line %d of \"%s\" (rank id: %d), skipping\n", line, source, rank_id);
		return false;
	}

	if( strlen(emblem) <= 0 )
	{
		ShowWarning("ranked_parse_dbrow: No emblem defined in line %d of \"%s\" (rank id: %d), skipping\n", line, source, rank_id);
		return false;
	}

	sprintf(path, "%s/creativesd/emblems/%s.ebm", db_path, emblem);
	if( !exists(path) )
	{
		ShowWarning("ranked_parse_dbrow: Emblem \"%s\" not found in line %d of \"%s\" (rank id: %d), skipping\n", path, line, source, rank_id);
		return false;
	}

	ranked_create(rank_id, name, points, emblem, auras, a);

	if( rank_id > ranked_last_id )
		ranked_last_id = rank_id;
	if( ranked_first_id <= 0 || rank_id < ranked_first_id )
		ranked_first_id = rank_id;
	return true;
}

void ranked_readdb(void)
{
	uint32 lines = 0, count = 0;
	char line[1024];
	char path[256];
	FILE* fp;

	sprintf(path, "%s/creativesd/ranked_db.txt", db_path);
	if( (fp = fopen(path, "r")) != NULL ) {
		// process rows one by one
		while(fgets(line, sizeof(line), fp))
		{
			char *str[32], *p;
			int i;

			lines++;
			if(line[0] == '/' && line[1] == '/')
				continue;
			memset(str, 0, sizeof(str));

			p = line;
			while( ISSPACE(*p) )
				++p;
			if( *p == '\0' )
				continue;// empty line
			for( i = 0; i < 4; ++i )
			{
				str[i] = p;
				p = strchr(p,',');
				if( p == NULL )
					break;// comma not found
				*p = '\0';
				++p;
			}

			if( p == NULL )
			{
				ShowError("ranked_readdb: Insufficient columns in line %d of \"%s\" (rank id %d), skipping.\n", lines, path, atoi(str[0]));
				continue;
			}

			// Script
			if( *p != '{' )
			{
				ShowError("ranked_readdb: Invalid format (Aura column) in line %d of \"%s\" (rank id %d), skipping.\n", lines, path, atoi(str[0]));
				continue;
			}
			str[4] = p;

			if (!ranked_parse_dbrow(str, path, lines))
				continue;
			count++;
		}
		fclose(fp);
		ShowStatus("Done reading '" CL_WHITE "%lu" CL_RESET "' entries in '" CL_WHITE "%s" CL_RESET "'.\n", count, path);
		ranked_reorder();
	}
	else {
		ShowWarning("ranked_readdb: File not found \"%s\", skipping.\n", path);
	}
}

void do_reload_ranked(void)
{
	ranked_db = idb_alloc(DB_OPT_RELEASE_DATA);
	ranked_counter = 0;
	ranked_readdb();
}

void do_init_ranked(void)
{
	ranked_db = idb_alloc(DB_OPT_RELEASE_DATA);
	ranked_readdb();

	add_timer_func_list(ranked_change_emblem,"ranked_change_emblem");

	ShowMessage(CL_WHITE"[Ranked System]:" CL_RESET " Ranked System (version: 3.0.00) successfully initialized.\n");
	ShowMessage(CL_WHITE"[Ranked System]:" CL_RESET " by (c) CreativeSD, suport in " CL_GREEN "www.creativesd.com.br" CL_RESET "\n");
}

void do_final_ranked(void)
{
	DBIterator* iter = db_iterator(ranked_db);
	struct ranked_data* rank;

	for (rank = (struct ranked_data*)dbi_first(iter); dbi_exists(iter); rank = (struct ranked_data*)dbi_next(iter))
		aFree(rank->g);

	dbi_destroy(iter);

	ranked_counter = 0;
	db_destroy(ranked_db);
}
