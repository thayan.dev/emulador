// © Creative Services and Development
// Site Oficial: www.creativesd.com.br
// Termos de Contrato e Autoria em: http://creativesd.com.br/?p=termos

ACMD_FUNC(reloadrankdb)
{
	nullpo_retr(-1, sd);
	do_final_ranked();
	do_reload_ranked();
	clif_displaymessage(fd, msg_txt(sd, (battle_config.ranked_msg_txt+6)));
	return 0;
}

ACMD_FUNC(rankinfo)
{
	struct ranked_data *rank;
	char output[200];
	int rank_id, points;

	nullpo_retr(-1,sd);

	if( battle_config.ranked_show_mode )
	{
		rank_id = sd->status.ranked_account.id;
		sd->status.ranked_account.points.current;
	}
	else {
		rank_id = sd->status.ranked_char.id;
		points = sd->status.ranked_char.points.current;
	}

	if( rank_id <= 0 || (rank = ranked_search(rank_id)) == NULL )
	{
		clif_displaymessage(fd, msg_txt(sd,(battle_config.ranked_msg_txt+7)));
		sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+8)), points);
		clif_displaymessage(fd, output);
		return 0;
	}

	sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+9)), rank->g->name);
	clif_displaymessage(fd, output);
	sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+8)), points);
	clif_displaymessage(fd, output);
	return 0;
}

ACMD_FUNC(rankinfo2)
{
	struct ranked_data *rank, *nrank;
	char output[200];
	int rank_id, points;

	nullpo_retr(-1,sd);

	if( battle_config.ranked_show_mode ) {
		rank_id = sd->status.ranked_account.id;
		points = sd->status.ranked_account.points.current;
	}
	else {
		rank_id = sd->status.ranked_char.id;
		points = sd->status.ranked_char.points.current;
	}

	if( rank_id <= 0 || (rank = ranked_search(rank_id)) == NULL )
	{
		clif_displaymessage(fd, msg_txt(sd,(battle_config.ranked_msg_txt+7)));
		sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+8)), points);
		clif_displaymessage(fd, output);

		// More Infos
		if( ranked_first_id && (nrank = ranked_search(ranked_first_id)) != NULL )
		{
			sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+10)), (nrank->points-points), nrank->g->name);
			clif_displaymessage(fd, output);
		}
		return 0;
	}

	sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+9)), rank->g->name);
	clif_displaymessage(fd, output);
	sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+8)), points);
	clif_displaymessage(fd, output);

	// More Infos
	if( rank->next_id && (nrank = ranked_search(rank->next_id)) != NULL )
	{
		sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+10)), (nrank->points-points), nrank->g->name);
		clif_displaymessage(fd, output);
	}
	return 0;
}

ACMD_FUNC(rankinfo3)
{
	struct ranked_data *rank, *nrank;
	struct map_session_data *pl_sd;
	char output[200];
	int rank_id, points;

	nullpo_retr(-1,sd);

	memset(atcmd_player_name, '\0', sizeof(atcmd_player_name));

	if (!message || !*message || sscanf(message, "%23[^\n]", atcmd_player_name) < 1) {
		clif_displaymessage(fd, msg_txt(sd,(battle_config.ranked_msg_txt+11)));
		return -1;
	}

	if ( (pl_sd = map_nick2sd(atcmd_player_name,true)) == NULL )
	{
		clif_displaymessage(fd, msg_txt(sd,3)); // Character not found.
		return -1;
	}

	if( battle_config.ranked_show_mode ) {
		rank_id = sd->status.ranked_account.id;
		points = sd->status.ranked_account.points.current;
	}
	else {
		rank_id = sd->status.ranked_char.id;
		points = sd->status.ranked_char.points.current;
	}

	if( rank_id <= 0 || (rank = ranked_search(rank_id)) == NULL )
	{
		sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+12)), pl_sd->status.name);
		clif_displaymessage(fd, output);
		sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+8)), points);
		clif_displaymessage(fd, output);

		// More Infos
		if(ranked_first_id && (nrank = ranked_search(ranked_first_id)) != NULL )
		{
			sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+10)), (nrank->points-points), nrank->g->name);
			clif_displaymessage(fd, output);
		}
		return 0;
	}

	sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+13)), pl_sd->status.name);
	clif_displaymessage(fd, output);
	sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+9)), rank->g->name);
	clif_displaymessage(fd, output);
	sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+8)), points);
	clif_displaymessage(fd, output);

	// More Infos
	if( rank->next_id && (nrank = ranked_search(rank->next_id)) != NULL )
	{
		sprintf(output, msg_txt(sd,(battle_config.ranked_msg_txt+10)), (nrank->points-points), nrank->g->name);
		clif_displaymessage(fd, output);
	}
	return 0;
}

ACMD_FUNC(rankshow)
{
	int16 m;
	nullpo_retr(-1,sd);

	m = map_id2index(sd->bl.m); // Get Mapindex
	if( !map_getmapflag(sd->bl.m,MF_TOWN) ) {
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+14)));
	}
	else if( sd->ranked.disable && ranked_show_map(sd->bl.m,RANK_SHOW_MAP) ) {
		//int rk_chg_timer = sd->status.guild_id ? battle_config.ranked_show_timer : 1;
		sd->ranked.disable = 0;
		//sd->ranked.display_timer = add_timer(gettick()+rk_chg_timer,ranked_change_emblem,sd->bl.id,0);
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+15)));
	}
	else {
		// Free Timers
		sd->ranked.disable = 1;
		sd->ranked.display_pos = RANK_SHOW_NONE;
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+16)));
		ranked_change_emblem_sub(sd,false);
		clif_refresh(sd);
	}
	return 0;
}

ACMD_FUNC(ranklist)
{
	nullpo_retr(-1, sd);
	ranked_show_list(sd);
	return 0;
}

ACMD_FUNC(setrank)
{
	struct ranked_data *rank;
	int rank_id, type;

	nullpo_retr(-1,sd);

	if (!message || !*message || sscanf(message, "%d %d", &rank_id, &type) < 2 || (type < RANK_CHAR || type > RANK_BOTH) ) {
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+17)));
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+18)));
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+19)));
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+20)));
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+21)));
		ranked_show_list(sd);
		return -1;
	}

	if( (rank = ranked_search(rank_id)) == NULL ) {
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+23)));
		return -1;
	}

	//if( sd->ranked.rank_id == rank->rank_id ) {
	//	clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+35)));
	//	return -1;
	//}
	
	type++;
	if( ranked_set_rank(sd,rank_id,type) )
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+22)));
	else
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+36)));
	return 0;	
}

ACMD_FUNC(setrankpoints)
{
	int type;
	int points;

	nullpo_retr(-1,sd);

	if (!message || !*message || sscanf(message, "%d %d", &type, &points) < 2 || (type < RANK_CHAR || type > RANK_BOTH) ) {
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+24)));
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+18)));
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+19)));
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+20)));
		clif_displaymessage(sd->fd, msg_txt(sd,(battle_config.ranked_msg_txt+21)));
		return -1;
	}

	if( points <= 0 )
		ranked_points_lost(sd,abs(points), RANK_P_OTHERS, (enum ranked_update)type);
	else
		ranked_points_gained(sd,points, RANK_P_OTHERS, (enum ranked_update)type);
	return 0;
}
