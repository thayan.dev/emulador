// © Creative Services and Development
// Site Oficial: www.creativesd.com.br
// Termos de Contrato e Autoria em: http://creativesd.com.br/?p=termos

	// Ranked System
	int ranked_enable;
	int ranked_max_aura;
	int ranked_msg_txt;
	int ranked_bg_crossover;
	int ranked_rank_reset;
	int ranked_show_mode;
	int ranked_show_timer;
	int ranked_duration_timer;
	int ranked_mode;
	int ranked_show_maps;
	int ranked_show_aura;
	int ranked_heal;
	int ranked_pk_gained;
	int ranked_pk_lost;
	int ranked_pd_reset;
	int ranked_pd_given;
	int ranked_pd_gained;
	int ranked_pd_taken;
	int ranked_pd_lost;
	int ranked_sd_reset;
	int ranked_sd_given;
	int ranked_sd_gained;
	int ranked_sd_taken;
	int ranked_sd_lost;
	int ranked_ss_reset;
	int ranked_ss_success;
	int ranked_ss_gained;
	int ranked_ss_fail;
	int ranked_ss_lost;
