// � Creative Services and Development
// Site Oficial: www.creativesd.com.br
// Termos de Contrato e Autoria em: http://creativesd.com.br/?p=termos

#ifndef _WIN32
#include <unistd.h>
#else
#include "../common/winapi.hpp"
#endif
#include "../common/cbasetypes.hpp"
#include "../common/db.hpp"
#include "../common/malloc.hpp"
#include "../common/nullpo.hpp"
#include "../common/sql.hpp"
#include "../common/strlib.hpp"
#include "../common/showmsg.hpp"
#include "../common/socket.hpp"
#include "../common/timer.hpp"
#include "../common/random.hpp"
#include "../common/utils.hpp"

#include "../creativesd/config.hpp"

#include "creativesd.hpp"

#include "map.hpp"
#include "atcommand.hpp"
#include "battleground.hpp"
#include "battle.hpp"
#include "chrif.hpp"
#include "clif.hpp"
#include "elemental.hpp"
#include "guild.hpp"
#include "intif.hpp"
#include "homunculus.hpp"
#include "mercenary.hpp"
#include "mob.hpp"
#include "npc.hpp"
#include "pc.hpp"
#include "pet.hpp"
#include "party.hpp"
#include "pc_groups.hpp"
#include "script.hpp"
#include "storage.hpp"
#include "log.hpp"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// Atcommand output
static char atcmd_output[CHAT_SIZE_MAX];
static char atcmd_player_name[NAME_LENGTH];

#ifdef BG_WARFARE_ENABLE
// Battleground Warfare
static DBMap* bg_reward_db; // int id -> struct battleground_rewards
struct guild bg_guild[2];

// clif.cpp functions
//

// Adiciona o icone de Batalhas no topo dos monstros.
void clif_sendbgmobemblem_single(int fd, struct mob_data *md)
{
	nullpo_retv(md);

	if (!(battle_config.bg_enable_cross_swords&0x01))
		return;

	WFIFOHEAD(fd, 32);
	WFIFOW(fd, 0) = 0x2dd;
	WFIFOL(fd, 2) = md->bl.id;
	safestrncpy((char*)WFIFOP(fd, 6), md->name, NAME_LENGTH);
	WFIFOW(fd, 30) = md->bg_id;
	WFIFOSET(fd, packet_len(0x2dd));
}

// Envia informa��es b�sicas do Cl� nas Batalhas.
void clif_bg_basicinfo(struct map_session_data *sd)
{
	int fd;
	struct battleground_data *bg;
#if PACKETVER < 20160622
	int cmd = 0x1b6;
	int offset = NAME_LENGTH;
#else
	int cmd = 0xa84;
	int offset = 0;
#endif

	nullpo_retv(sd);
	fd = sd->fd;

	if (!sd->bg_id || (bg = bg_team_search(sd->bg_id)) == NULL)
		return;

	WFIFOHEAD(fd, packet_len(cmd));
	WFIFOW(fd, 0) = cmd;
	WFIFOL(fd, 2) = bg->g->guild_id;
	WFIFOL(fd, 6) = bg->g->guild_lv;
	WFIFOL(fd, 10) = bg->count;
	WFIFOL(fd, 14) = bg->g->max_member;
	WFIFOL(fd, 18) = 0;
	WFIFOL(fd, 22) = 0;
	WFIFOL(fd, 26) = 0;
	WFIFOL(fd, 30) = 0;	// Tax Points
	WFIFOL(fd, 34) = 0;	// Honor: (left) Vulgar [-100,100] Famed (right)
	WFIFOL(fd, 38) = 0;	// Virtue: (down) Wicked [-100,100] Righteous (up)
	WFIFOL(fd, 42) = bg->g->emblem_id;
	safestrncpy(WFIFOCP(fd, 46), bg->g->name, NAME_LENGTH);
#if PACKETVER < 20160622
	safestrncpy(WFIFOCP(fd, 70), bg->g->master, NAME_LENGTH);
#endif
	safestrncpy(WFIFOCP(fd, 70 + offset), bg->g->master, 16);
	WFIFOL(fd, 70 + offset + 16) = 0;
#if PACKETVER >= 20160622
	WFIFOL(fd, 70 + offset + 20) = bg->master_id;  // leader
#endif

	WFIFOSET(fd, packet_len(cmd));
}

// Envia informa��es do Cl� nas Batalhas.
void clif_bg_belonginfo(struct map_session_data *sd)
{
	int fd;
	struct battleground_data *bg;
	nullpo_retv(sd);

	if (!sd->bg_id || (bg = bg_team_search(sd->bg_id)) == NULL)
		return;

	fd = sd->fd;
	WFIFOHEAD(fd, packet_len(0x16c));
	WFIFOW(fd, 0) = 0x16c;
	WFIFOL(fd, 2) = bg->g->guild_id;
	WFIFOL(fd, 6) = bg->g->emblem_id;
	WFIFOL(fd, 10) = 0x10;
	WFIFOB(fd, 14) = 0;
	WFIFOL(fd, 15) = 0;  // InterSID (unknown purpose)
	memcpy(WFIFOP(fd, 19), bg->g->name, NAME_LENGTH);
	WFIFOSET(fd, packet_len(0x16c));
}

// Cria e envia a lista de membros das Batalhas.
void clif_bg_memberlist(struct map_session_data *sd)
{
	int fd;
	int i, c;
	struct battleground_data *bg;
	struct map_session_data *psd;
#if PACKETVER < 20161026
	int cmd = 0x154;
	int size = 104;
#else
	int cmd = 0xaa5;
	int size = 34;
#endif

	nullpo_retv(sd);

	if ((fd = sd->fd) == 0)
		return;

	if (!(sd->bg_id && (bg = bg_team_search(sd->bg_id)) != NULL))
		return;

	WFIFOHEAD(fd, MAX_BG_MEMBERS * size + 4);
	WFIFOW(fd, 0) = cmd;
	for (i = 0, c = 0; i < MAX_BG_MEMBERS; i++)
	{
		if ((psd = bg->members[i].sd) == NULL)
			continue;

		WFIFOL(fd, c*size + 4) = psd->status.account_id;
		WFIFOL(fd, c*size + 8) = psd->status.char_id;
		WFIFOW(fd, c*size + 12) = psd->status.hair;
		WFIFOW(fd, c*size + 14) = psd->status.hair_color;
		WFIFOW(fd, c*size + 16) = psd->status.sex;
		WFIFOW(fd, c*size + 18) = psd->status.class_;
		WFIFOW(fd, c*size + 20) = psd->status.base_level;
		WFIFOL(fd, c*size + 22) = (int)cap_value(psd->status.base_exp, 0, INT32_MAX);
		WFIFOL(fd, c*size + 26) = 1;
		WFIFOL(fd, c*size + 30) = psd->bg_position;
#if PACKETVER < 20161026
		memset(WFIFOP(fd, c*size + 34), 0, 50);	//[Ind] - This is displayed in the 'note' column but being you can't edit it it's sent empty.
		safestrncpy(WFIFOCP(fd, c*size + 84), psd->status.name, NAME_LENGTH);
#else
		WFIFOL(fd, c*size + 34) = 0;
#endif
		c++;
	}
	WFIFOW(fd, 2) = c * size + 4;
	WFIFOSET(fd, WFIFOW(fd, 2));
}

// battleground.cpp functions
//

/*==========================================
 * CreativeSD: Battleground Warfare
 *------------------------------------------*/
struct guild* bg_get_guild(int idx)
{
	if( idx > 1 )
		return NULL;
	return &bg_guild[idx];
}

// Atualiza Patentes
int bg_refresh_patent(int bg_id)
{
	struct battleground_data *bg = bg_team_search(bg_id);
	struct map_session_data *sd;
	char output[256];
	int i, p, j = 0;
	int army;

	if (bg == NULL)
	{
		ShowError("bg_refresh_patent: Team not found.\n");
		return false;
	}

	army = bg->army;
	if (army < 0 || army > 1)
	{
		ShowError("bg_refresh_patent: Unknown Army.\n");
		return false;
	}

	for (i = 0; i < MAX_BG_MEMBERS; i++)
	{
		if ((sd = bg->members[i].sd) == NULL)
			continue;

		p = j>=MAX_BG_POSITION?(MAX_BG_POSITION - 1):j;
		if( p != sd->bg_position )
		{
			sd->bg_position = p;

			sprintf(output, msg_txt(sd,(BG_MSG_TXT+4)), bg->g->position[j].name, bg->g->name);
			clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
			if (j == 0 ) {
				bg->master_id = sd->status.char_id;
				bg_block_skill_change(sd);
			}
		}

		bg_guild_requestinfo(sd);
		j++;
	}
	return true;
}

// Limpa as habilidades de cl� das batalhas campais.
void bg_clean_skill(struct battleground_data *bg)
{
	memset(bg->skill_block_timer, 0, sizeof(bg->skill_block_timer));
}

// Bloqueio de Habilidades
void bg_block_skill(struct map_session_data *sd, int time)
{
	struct battleground_data *bg = NULL;
	uint16 skill_id[] = { GD_BATTLEORDER, GD_REGENERATION, GD_RESTORE, GD_EMERGENCYCALL };
	int idx, i;

	if( !sd->bg_id || (bg = bg_team_search(sd->bg_id)) == NULL )
		return;

	for (i = 0; i < 4; i++) {
		idx = skill_id[i] - GD_SKILLBASE;
		skill_blockpc_start(sd, skill_id[i], time);
		bg->skill_block_timer[idx] = gettick()+time;
	}
}

// Altera o delay de habilidades de Cl� das Batalhas Campais para outro jogador.
void bg_block_skill_change(struct map_session_data *sd)
{
	struct battleground_data *bg;
	uint16 skill_id[] = { GD_BATTLEORDER, GD_REGENERATION, GD_RESTORE, GD_EMERGENCYCALL };
	int i, idx;
	unsigned int timer;

	if( !sd->bg_id || (bg = bg_team_search(sd->bg_id)) == NULL )
		return;

	for (i=0; i < 4; i++) {
		idx = skill_id[i] - GD_SKILLBASE;
		timer = bg->skill_block_timer[idx];

		if( timer >= gettick() ) {
			bg->skill_block_timer[idx] = 0;
			continue;
		}

		timer -= gettick();
		skill_blockpc_start(sd, skill_id[i], timer);
	}
}

// Remove os delays das habilidades de Cl� das Batalhas Campais.
void bg_block_skill_end(struct map_session_data *sd)
{
	uint16 skill_id[] = { GD_BATTLEORDER, GD_REGENERATION, GD_RESTORE, GD_EMERGENCYCALL };
	int i;

	for (i=0; i < 4; i++) {
		//idx = skill_id[i] - GD_SKILLBASE;
		//sd->blockskill[skill_id[i]] = 0;
		
		if( battle_config.display_status_timers )
			clif_skill_cooldown(sd, skill_id[i], 0);
	}
}

// Reportagem de AFK
int bg_report_afk(int tid, unsigned int tick, int id, intptr_t data)
{
	struct map_session_data *sd;

	sd = map_id2sd(id);
	if (sd)
	{
		if (sd->bg_afk_timer != INVALID_TIMER && sd->bg_id && !sd->npc_id && DIFF_TICK(last_tick, sd->idletime))
		{
			bg_team_leave(sd, BGTL_AFK);
			if (battle_config.bg_afk_warp_save_point)
				pc_setpos(sd, sd->status.save_point.map, sd->status.save_point.x, sd->status.save_point.y, CLR_TELEPORT);
			else {
				unsigned short map_index = mapindex_name2id("bat_room");
				if (map_index)
					pc_setpos(sd, map_index, 154, 150, CLR_TELEPORT);
			}
		}
		sd->bg_afk_timer = INVALID_TIMER;
		return 1;
	}
	return 0;
}

// Expuls�o de Jogadores
int bg_kick_player(struct map_session_data* sd, struct map_session_data *pl_sd, const char* mes)
{
	struct battleground_data *bg;
	int i, c = 0;
	char output[200];

	if( (bg = bg_team_search(sd->bg_id)) == NULL )
		return false;

	ARR_FIND(0, MAX_BG_MEMBERS, i, pl_sd->bg_kick.char_id[i] == sd->status.char_id);
	if( i < MAX_BG_MEMBERS ) {
		if( i == 0 ) {
			sprintf(output, msg_txt(sd,(BG_MSG_TXT+5)), pl_sd->status.name);
			clif_messagecolor(&sd->bl, color_table[COLOR_RED], output, false, SELF);
		}
		else {
			sprintf(output, msg_txt(sd,(BG_MSG_TXT+6)), pl_sd->status.name);
			clif_messagecolor(&sd->bl, color_table[COLOR_RED], output, false, SELF);
		}
		sprintf(output, msg_txt(sd,(BG_MSG_TXT+7)), pl_sd->bg_kick.mes);
		clif_messagecolor(&sd->bl, color_table[COLOR_RED], output, false, SELF);
		return false;
	}

	ARR_FIND(0, MAX_BG_MEMBERS, i, pl_sd->bg_kick.char_id[i] == 0 );
	if( i == 0 && strlen(mes) == 0 )
	{
		sprintf(output, msg_txt(sd,(BG_MSG_TXT+8)), pl_sd->status.name);
		clif_messagecolor(&sd->bl, color_table[COLOR_RED], output, false, SELF);
		return false;
	}

	// Ceil is not working in this formula, make the manual rounding...
	c = (bg->count/2)+1;
	while( c >= bg->count ) {
		c--;
	}
	
	pl_sd->bg_kick.char_id[i] = sd->status.char_id;
	pl_sd->bg_kick.count++;

	// Notify Members
	if( i == 0 )
	{
		safestrncpy(pl_sd->bg_kick.mes, mes, sizeof(pl_sd->bg_kick.mes));
		sprintf(output, msg_txt(sd,(BG_MSG_TXT+9)), sd->status.name, pl_sd->status.name);
		clif_bg_message(bg, 0, "Server", output, strlen(output)+1);
	}

	sprintf(output, msg_txt(sd,(BG_MSG_TXT+10)), pl_sd->status.name);
	clif_bg_message(bg, 0, "Server", output, strlen(output)+1);
	sprintf(output, msg_txt(sd,(BG_MSG_TXT+11)), mes);
	clif_bg_message(bg, 0, "Server", output, strlen(output)+1);
	sprintf(output, msg_txt(sd,(BG_MSG_TXT+12)), pl_sd->bg_kick.count, c);
	clif_bg_message(bg, 0, "Server", output, strlen(output)+1);

	if( pl_sd->bg_kick.count >= c ) {
		bg_team_leave(pl_sd, BGTL_KICK);
		pc_setpos(pl_sd,pl_sd->status.save_point.map,pl_sd->status.save_point.x,pl_sd->status.save_point.y,CLR_TELEPORT);
	}
	return true;
}

// Altera o Capit�o da Batalha.
int bg_change_master(int bg_id, int char_id, struct map_session_data *sd)
{
	struct battleground_data *bg = bg_team_search(bg_id);
	struct map_session_data *tsd;
	int i, x1, y1, x2, y2, m_afk, n_afk, j = 0;
	if (bg == NULL)
		return 0;

	for (i = 0; i < MAX_BG_MEMBERS; i++)
	{
		if ((tsd = bg->members[i].sd) == NULL)
			continue;

		if (tsd->status.char_id == char_id)
		{
			m_afk = bg->members[0].afk;
			n_afk = bg->members[i].afk;
			x1 = bg->members[0].x;
			y1 = bg->members[0].y;
			x2 = bg->members[i].x;
			y2 = bg->members[i].y;

			bg->members[0].sd = tsd;
			bg->members[0].x = x2;
			bg->members[0].y = y2;
			bg->members[0].afk = m_afk;

			bg->members[i].sd = sd;
			bg->members[i].x = x1;
			bg->members[i].y = y1;
			bg->members[i].afk = n_afk;
			bg_refresh_patent(bg_id);
			return 1;
		}
		j++;
	}
	return 0;
}

// Respawn autom�tico.
int bg_respawn_timer(int tid, unsigned int tick, int id, intptr_t data)
{
	struct map_session_data *sd;
	struct battleground_data *bg;
	char cutin[NAME_LENGTH];
	char sound[NAME_LENGTH];
	sd = map_id2sd(id);

	if (sd)
	{
		if (sd->bg_id && (bg = bg_team_search(sd->bg_id)) != NULL && sd->bg_respawn_timer != INVALID_TIMER )
		{
			if( sd->bg_respawn_timer_count < 0 )
			{
				clif_cutin(sd,"",255);
				sd->bg_respawn_timer = INVALID_TIMER;
				sd->bg_respawn_timer_count = 0;
				pc_percentheal(sd,100,100);
				pc_setpos(sd, bg->mapindex, bg->respawn_x, bg->respawn_y, CLR_OUTSIGHT);
				bg_guild_requestinfo(sd);
			}
			else {
				sd->bg_respawn_timer_count--;
				sprintf(cutin,"respawn_%d",sd->bg_respawn_timer_count+1);
				sprintf(sound,"respawn_%s.wav",sd->bg_respawn_timer_count<0?"go":"beep");
				clif_cutin(sd,cutin,2);
				clif_soundeffect(sd,&sd->bl,sound,0);
				sd->bg_respawn_timer = add_timer(gettick() + 1000, bg_respawn_timer, sd->bl.id, 0);
			}
		}
		else {
			sd->bg_respawn_timer = INVALID_TIMER;
			sd->bg_respawn_timer_count = 0;
		}
		return 1;
	}
	return 0;
}

void bg_respawn_timer_delete(struct map_session_data *sd)
{
	sd->bg_respawn_timer = INVALID_TIMER;
	sd->bg_respawn_timer_count = 0;
	clif_cutin(sd,"",255);
}

int bg_digit_timer(int tid, unsigned int tick, int id, intptr_t data)
{
	struct map_session_data *sd = NULL;
	struct battleground_data *bg = bg_team_search(id);
	int i;
	
	if( bg == NULL && bg->timerdigit != INVALID_TIMER )
		return 0;

	for (i = 0; i < MAX_BG_MEMBERS; i++)
	{
		if ((sd = bg->members[i].sd) == NULL)
			continue;

		if( bg->timerdigit_count > 0 )
			clif_showdigit(sd, (unsigned char)2, -(bg->timerdigit_count));
		else
			clif_showdigit(sd, (unsigned char)3, 0);
	}

	if( bg->timerdigit_count <= 0 )
		bg->timerdigit = INVALID_TIMER;
	else {
		bg->timerdigit_count--;
		bg->timerdigit = add_timer(gettick() + 1000, bg_digit_timer, bg->bg_id, 0);
	}

	return 1;
}

// Cria o Cl� dentro da estrutura das Batalhas.
void bg_create_guild()
{
	int j;
	memset(&bg_guild, 0, sizeof(bg_guild));
	for( j=0; j <= 1; j++ )
	{
		FILE* fp = NULL;
		char path[256];
		int i, skill;

		bg_guild[j].emblem_id = 1;
		bg_guild[j].guild_id = SHRT_MAX-j;
		bg_guild[j].guild_lv = 50;
		bg_guild[j].max_member = MAX_BG_MEMBERS;

		for( i=0; i < MAX_GUILDSKILL; i++ )
		{
			skill = i + GD_SKILLBASE;
			bg_guild[j].skill[i].id = skill;
			switch( skill )
			{
				case GD_GLORYGUILD:
					bg_guild[j].skill[i].lv = 0;
					break;
				case GD_APPROVAL:
				case GD_KAFRACONTRACT:
				case GD_GUARDRESEARCH:
				case GD_BATTLEORDER:
				case GD_RESTORE:
				case GD_EMERGENCYCALL:
				case GD_DEVELOPMENT:
					bg_guild[j].skill[i].lv = 1;
					break;
				case GD_GUARDUP:
				case GD_REGENERATION:
					bg_guild[j].skill[i].lv = 3;
					break;
				case GD_LEADERSHIP:
				case GD_GLORYWOUNDS:
				case GD_SOULCOLD:
				case GD_HAWKEYES:
					bg_guild[j].skill[i].lv = 5;
					break;
				case GD_EXTENSION:
					bg_guild[j].skill[i].lv = 10;
					break;
			}
		}

		if( j == 1 )
		{
			// Guild Data - Croix
			strncpy(bg_guild[j].name, msg_txt(NULL,(BG_MSG_TXT+58)), NAME_LENGTH);
			strncpy(bg_guild[j].master, msg_txt(NULL,(BG_MSG_TXT+59)), NAME_LENGTH);
			memcpy(bg_guild[j].mes1, msg_txt(NULL,(BG_MSG_TXT+60)),MAX_GUILDMES1);
			memcpy(bg_guild[j].mes2, msg_txt(NULL,(BG_MSG_TXT+61)),MAX_GUILDMES2);
		}
		else {
			// Ex�rcito de Guillaume
			strncpy(bg_guild[j].name, msg_txt(NULL,(BG_MSG_TXT+54)), NAME_LENGTH);
			strncpy(bg_guild[j].master, msg_txt(NULL,(BG_MSG_TXT+55)), NAME_LENGTH);
			memcpy(bg_guild[j].mes1, msg_txt(NULL,(BG_MSG_TXT+56)),MAX_GUILDMES1);
			memcpy(bg_guild[j].mes2, msg_txt(NULL,(BG_MSG_TXT+57)),MAX_GUILDMES2);
		}

		strncpy(bg_guild[j].position[0].name, msg_txt(NULL,(BG_MSG_TXT+62)), NAME_LENGTH);
		strncpy(bg_guild[j].position[1].name, msg_txt(NULL,(BG_MSG_TXT+63)), NAME_LENGTH);
		strncpy(bg_guild[j].position[2].name, msg_txt(NULL,(BG_MSG_TXT+64)), NAME_LENGTH);
		strncpy(bg_guild[j].position[3].name, msg_txt(NULL,(BG_MSG_TXT+65)), NAME_LENGTH);

		sprintf(path, "%s/creativesd/emblems/bg_%d.ebm", db_path, (j+1));
		if( (fp = fopen(path, "rb")) != NULL )
		{
			fseek(fp, 0, SEEK_END);
			bg_guild[j].emblem_len = ftell(fp);
			fseek(fp, 0, SEEK_SET);
			fread(bg_guild[j].emblem_data, 1, bg_guild[j].emblem_len, fp);
			fclose(fp);
			ShowStatus("Done reading '" CL_WHITE "%s" CL_RESET "' battleground emblem data file.\n", path);
		}
	}
	ShowStatus("Create '" CL_WHITE "%d" CL_RESET "' guild for Battleground.\n", j);
}

// Faz requisi��o de informa��es do Cl� da Batalha.
void bg_guild_requestinfo(struct map_session_data *sd)
{
	nullpo_retv(sd);
	guild_send_dot_remove(sd);
	clif_name_area(&sd->bl);
	clif_name_self(&sd->bl);
	clif_bg_belonginfo(sd);
	clif_bg_basicinfo(sd);
	clif_guild_skillinfo(sd);
	clif_guild_memberlist(sd);
	clif_guild_emblem_area(&sd->bl);
}

// Computa o dano nas Batalhas Campais.
bool bg_score_damage(struct block_list *src, struct block_list *dst, int damage)
{
	struct block_list *s_bl, *t_bl;

	if( damage <= 0 || src == NULL || dst == NULL || !map_getmapflag(src->m,MF_BATTLEGROUND) )
		return 0;

	if( (s_bl = battle_get_master(src)) == NULL )
		s_bl = src;

	if( (t_bl = battle_get_master(dst)) == NULL )
		t_bl = dst;

	if( s_bl->type == BL_PC ) {
		struct map_session_data *sd = BL_CAST(BL_PC, s_bl);

		if( sd == NULL || !sd->bg_id )
			return 0;

		if( t_bl->type == BL_PC ) {
			struct map_session_data *tsd = BL_CAST(BL_PC, t_bl);

			if( tsd == NULL || !tsd->bg_id || tsd->bg_id == sd->bg_id )
				return 0;

			increment_limit(sd->bg_score.player_damage_given, (unsigned int)damage, USHRT_MAX);
			increment_limit(tsd->bg_score.player_damage_taken, (unsigned int)damage, USHRT_MAX);
			return 1;
		}
		else if( t_bl->type == BL_MOB ) {
			struct mob_data *md = BL_CAST(BL_MOB, t_bl);

			if( md == NULL )
				return 0;

			switch(md->mob_id)
			{
				// Emperium
				case MOBID_EMPERIUM:
					increment_limit(sd->bg_score.emperium_damage, (unsigned int)damage, USHRT_MAX);
					break;
				// Rune Stones
				case MOBID_GUARDIAN_STONE1:
				case MOBID_GUARDIAN_STONE2:
					increment_limit(sd->bg_score.runestone_damage, (unsigned int)damage, USHRT_MAX);
					break;
				// Guardians
				case 1285:
				case 1286:
				case 1287:
				case 1949:
				case 1950:
					increment_limit(sd->bg_score.guardian_damage_given, (unsigned int)damage, USHRT_MAX);
					break;
				// Barrier
				case 1905:
				case 1906:
					increment_limit(sd->bg_score.barrier_damage, (unsigned int)damage, USHRT_MAX);
					break;
				// Objectives
				case 1909:
				case 1910:
					increment_limit(sd->bg_score.objective_damage, (unsigned int)damage, USHRT_MAX);
					break;
				// Flags
				case 1911:
				case 1912:
				case 1913:
					increment_limit(sd->bg_score.flag_damage, (unsigned int)damage, USHRT_MAX);
					break;
				// Crystals
				case 1914:
				case 1915:
					increment_limit(sd->bg_score.crystal_damage, (unsigned int)damage, USHRT_MAX);
					break;
				default:
					return 0;
			}
			return 1;
		}
	}
	else if( s_bl->type == BL_MOB ) {
		struct map_session_data *sd;
		struct mob_data *md = BL_CAST(BL_MOB, s_bl);

		if( md == NULL || t_bl->type != BL_PC || (sd = BL_CAST(BL_PC, t_bl)) == NULL )
			return 0;

		if( sd == NULL || !sd->bg_id )
			return 0;

		switch(md->mob_id)
		{
			// Emperium
			case MOBID_EMPERIUM:
				increment_limit(sd->bg_score.emperium_damage, (unsigned)damage, USHRT_MAX);
				break;
			// Rune Stones
			case MOBID_GUARDIAN_STONE1:
			case MOBID_GUARDIAN_STONE2:
				increment_limit(sd->bg_score.runestone_damage, (unsigned)damage, USHRT_MAX);
				break;
			// Guardians
			case 1285:
			case 1286:
			case 1287:
			case 1949:
			case 1950:
				increment_limit(sd->bg_score.guardian_damage_given, (unsigned)damage, USHRT_MAX);
				break;
			// Barricades
			case 1905:
			case 1906:
				increment_limit(sd->bg_score.barrier_damage, (unsigned)damage, USHRT_MAX);
				break;
			// Objectives
			case 1909:
			case 1910:
				increment_limit(sd->bg_score.objective_damage, (unsigned)damage, USHRT_MAX);
				break;
			// Flags
			case 1911:
			case 1912:
			case 1913:
				increment_limit(sd->bg_score.flag_damage, (unsigned)damage, USHRT_MAX);
				break;
			// Crystals
			case 1914:
			case 1915:
				increment_limit(sd->bg_score.crystal_damage, (unsigned)damage, USHRT_MAX);
				break;
			default:
				break;
		}
		return 1;
	}
	return 0;
}

// Computa elemina��es nas Batalhas Campais.
bool bg_score_kills(struct block_list *src, struct block_list *dst)
{
	struct block_list *s_bl;
	struct map_session_data *sd;

	if( !src || !dst || src == dst || !map_getmapflag(src->m,MF_BATTLEGROUND) )
		return 0;

	if( (s_bl = battle_get_master(src)) == NULL )
		s_bl = src;

	sd = BL_CAST(BL_PC, s_bl);
	switch( dst->type )
	{
		case BL_PC:
			{
				struct map_session_data *tsd = BL_CAST(BL_PC, dst);
				if( s_bl->type == BL_MOB )
				{
					if( tsd != NULL && tsd->bg_id )
						increment_limit(tsd->bg_score.guardian_deaths, 1, USHRT_MAX);
				}
				else {
					if( tsd != NULL && sd != NULL && tsd->bg_id && sd->bg_id && tsd->bg_id != sd->bg_id ) {
						increment_limit(tsd->bg_score.player_deaths, 1, USHRT_MAX);
						increment_limit(sd->bg_score.player_kills, 1, USHRT_MAX);
					}
				}
			}
			break;
		case BL_MOB:
			{
				struct mob_data *md = BL_CAST(BL_MOB, dst);

				if( md == NULL || sd == NULL || !sd->bg_id )
					return 0;
				
				switch(md->mob_id)
				{
					case MOBID_EMPERIUM:
						increment_limit(sd->bg_score.emperium_kills, 1, USHRT_MAX);
						break;
					case MOBID_GUARDIAN_STONE1:
					case MOBID_GUARDIAN_STONE2:
						increment_limit(sd->bg_score.runestone_kills, 1, USHRT_MAX);
						break;
					case 1285:
					case 1286:
					case 1287:
					case 1949:
					case 1950:
						increment_limit(sd->bg_score.guardian_kills, 1, USHRT_MAX);
						break;
					case 1906:
						increment_limit(sd->bg_score.barrier_kills, 1, USHRT_MAX);
						break;
					case 1909:
					case 1910:
						increment_limit(sd->bg_score.objective_kills, 1, USHRT_MAX);
						break;
					case 1911:
					case 1912:
					case 1913:
						increment_limit(sd->bg_score.flag_kills, 1, USHRT_MAX);
						break;
					case 1914:
					case 1915:
						increment_limit(sd->bg_score.crystal_kills, 1, USHRT_MAX);
						break;
					default:
						return 0;
				}
			}
			break;
		default:
			return 0;
	}
	return 1;
}

// Computa as curas nas Batalhas Campais.
bool bg_score_heal(struct block_list *bl, int hp, int sp)
{
	struct block_list *s_bl = battle_get_master(bl);
	struct map_session_data *sd;

	if( s_bl == NULL )
		s_bl = bl;

	sd = BL_CAST(BL_PC, s_bl);

	if( sd == NULL || !sd->bg_id || !map_getmapflag(sd->bl.m,MF_BATTLEGROUND) )
		return 0;

	if( hp > 0 )
		increment_limit(sd->bg_score.heal_hp, (unsigned int)hp, USHRT_MAX);
	if( sp > 0 )
		increment_limit(sd->bg_score.heal_sp, (unsigned int)sp, USHRT_MAX);
	return 1;
}

// Computa utiliza��o de itens de cura nas Batalhas Campais.
bool bg_score_item_heal(struct map_session_data *sd, int amount, int hp, int sp)
{
	if( sd == NULL || !sd->bg_id || !map_getmapflag(sd->bl.m,MF_BATTLEGROUND) )
		return 0;

	if( amount > 0 )
		increment_limit(sd->bg_score.item_heal, (unsigned int)amount, USHRT_MAX);
	if( hp > 0 )
		increment_limit(sd->bg_score.item_heal_hp, (unsigned int)hp, USHRT_MAX);
	if( sp > 0 )
		increment_limit(sd->bg_score.item_heal_sp, (unsigned int)sp, USHRT_MAX);
	return 1;
}

// Computa utiliza��o de itens de requerimentos nas Batalhas Campais.
// Itens utilizados em NPC's e Habilidades.
bool bg_score_del_item(struct map_session_data *sd, int item_id, int amount)
{
	if( item_id <= 0 || amount <= 0 )
		return 0;

	switch(item_id)
	{
		// Poision Bottle (Garrafa de Veneno)
		case 678:
			increment_limit(sd->bg_score.poison_bottle, (unsigned int)amount, USHRT_MAX);
			break;
		// Yellow Gemstone (Gema Amarela)
		case 715:
			increment_limit(sd->bg_score.yellow_gemstone, (unsigned int)amount, USHRT_MAX);
			break;
		// Red Gemstone (Gema Vermelha)
		case 716:
			increment_limit(sd->bg_score.red_gemstone, (unsigned int)amount, USHRT_MAX);
			break;
		// Blue Gemstone (Gema Azul)
		case 717:
			increment_limit(sd->bg_score.blue_gemstone, (unsigned int)amount, USHRT_MAX);
			break;
		// Oridecon (Oridecon)
		case 984:
			increment_limit(sd->bg_score.oridecon, (unsigned int)amount, USHRT_MAX);
			break;
		// Elunium (Elunium)
		case 985:
			increment_limit(sd->bg_score.elunium, (unsigned int)amount, USHRT_MAX);
			break;
		// Steel (A�o)
		case 999:
			increment_limit(sd->bg_score.steel, (unsigned int)amount, USHRT_MAX);
			break;
		// Emveretarcon (Emveretarcon)
		case 1011:
			increment_limit(sd->bg_score.emveretarcon, (unsigned int)amount, USHRT_MAX);
			break;
		// Wooden Block (Tronco)
		case 1019:
			increment_limit(sd->bg_score.wooden_block, (unsigned int)amount, USHRT_MAX);
			break;
		// Stone (Pedra)
		case 7049:
			increment_limit(sd->bg_score.stone, (unsigned int)amount, USHRT_MAX);
			break;
		// Fire Bottle (Frasco de Fogo Grego)
		case 7135:
			increment_limit(sd->bg_score.fire_bottle, (unsigned int)amount, USHRT_MAX);
			break;
		// Acid Bottle (Frasco de �cido)
		case 7136:
			increment_limit(sd->bg_score.acid_bottle, (unsigned int)amount, USHRT_MAX);
			break;
		default:
			return 0;
	}
	return 1;
}

static int bg_rate_adjust(int baserate, int rate_adjust, unsigned short rate_min, unsigned short rate_max)
{
	double rate = baserate;

	if (battle_config.logarithmic_drops && rate_adjust > 0 && rate_adjust != 100 && baserate > 0) //Logarithmic drops equation by Ishizu-Chan
		//Equation: Droprate(x,y) = x * (5 - log(x)) ^ (ln(y) / ln(5))
		//x is the normal Droprate, y is the Modificator.
		rate = rate * pow((5.0 - log10(rate)), (log(rate_adjust/100.) / log(5.0))) + 0.5;
	else
		//Classical linear rate adjustment.
		rate = rate * rate_adjust/100;

	return (unsigned int)cap_value(rate,rate_min,rate_max);
}

struct battleground_rewards* bg_search_reward(int arena_id)
{ // Search Rewards by Arena ID
	if( !arena_id ) return NULL;
	return (struct battleground_rewards *)idb_get(bg_reward_db, arena_id);
}

struct battleground_rewards* bg_create_reward(int arena_id)
{
	struct battleground_rewards *bgr;
	CREATE(bgr, struct battleground_rewards, 1);
	bgr->arena_id = arena_id;
	bgr->count = 0;
	memset(&bgr->items, 0, sizeof(bgr->items));
	idb_put(bg_reward_db, arena_id, bgr);
	return bgr;
}

void bg_reload_rewards()
{
	bg_reward_db->clear(bg_reward_db, NULL);
	bg_load_rewards_from_sql();
}

int bg_load_rewards_from_sql()
{
	SqlStmt* stmt = SqlStmt_Malloc(mmysql_handle);
	StringBuf buff;
	struct battleground_rewards *bgr;
	struct battleground_rewards_objects tmp_reward;
	int i, arena_id, count = 0;

	StringBuf_Init(&buff);
	StringBuf_AppendStr(&buff, "SELECT `bg_id`, `object`, `value`, `type`, `for`, `rate`, `desc` FROM `bg_rewards` WHERE `status`='1'");

	if( SQL_ERROR == SqlStmt_PrepareStr(stmt, StringBuf_Value(&buff))
		|| SQL_ERROR == SqlStmt_Execute(stmt)
		|| SQL_ERROR == SqlStmt_BindColumn(stmt, 0, SQLDT_INT, &arena_id, 0, NULL, NULL)
		|| SQL_ERROR == SqlStmt_BindColumn(stmt, 1, SQLDT_STRING, &tmp_reward.object, sizeof(tmp_reward.object), NULL, NULL) 
		|| SQL_ERROR == SqlStmt_BindColumn(stmt, 2, SQLDT_INT, &tmp_reward.value, 0, NULL, NULL)
		|| SQL_ERROR == SqlStmt_BindColumn(stmt, 3, SQLDT_INT, &tmp_reward.object_type, 0, NULL, NULL)
		|| SQL_ERROR == SqlStmt_BindColumn(stmt, 4, SQLDT_INT, &tmp_reward.flag_type, 0, NULL, NULL)
		|| SQL_ERROR == SqlStmt_BindColumn(stmt, 5, SQLDT_INT, &tmp_reward.rate, 0, NULL, NULL)
		|| SQL_ERROR == SqlStmt_BindColumn(stmt, 6, SQLDT_STRING, &tmp_reward.desc, sizeof(tmp_reward.desc), NULL, NULL)
	) {
		SqlStmt_ShowDebug(stmt);
		SqlStmt_Free(stmt);
		return 0;
	}

	if( SqlStmt_NumRows(stmt) <= 0 )
	{
		SqlStmt_FreeResult(stmt);
		return 0;
	}

	while( SQL_SUCCESS == SqlStmt_NextRow(stmt) ) {
		if( (bgr = bg_search_reward(arena_id)) == NULL )
			bgr = bg_create_reward(arena_id);

		i = bgr->count;
		if( i >= MAX_BG_REWARDS )
			continue;

		memcpy(&bgr->items[i], &tmp_reward, sizeof(tmp_reward));
		bgr->count++;
		count++;
	}

	StringBuf_Destroy(&buff);
	SqlStmt_FreeResult(stmt);
	ShowStatus("Load '" CL_WHITE "%d" CL_RESET "' rewards for Battleground.\n", count);
	return count;
}

int bg_reward(struct map_session_data *sd, int arena_id, enum bg_reward_type flag) {
	struct battleground_rewards *bgr;
	int i, rate, rate_adjust, rate_min, rate_max, count = 0;
	char output[200];
	
	if( sd == NULL || (bgr = bg_search_reward(arena_id)) == NULL || bgr->count <= 0 )
		return 0;
	
	rate_adjust = battle_config.bg_reward_rate;
	rate_min = battle_config.bg_reward_rate_min;
	rate_max = battle_config.bg_reward_rate_max;

	for( i = 0; i < MAX_BG_REWARDS; i++ )
	{
		if( strlen(bgr->items[i].object) <= 0 )
			continue;

		if( bgr->items[i].flag_type != flag )
			continue;
			
		if( battle_config.bg_reward_max_given && count >= battle_config.bg_reward_max_given )
			break;

		rate = bg_rate_adjust(bgr->items[i].rate, rate_adjust, rate_min, rate_max);
		if( battle_config.bg_reward_rate_enable && rate <= rand()%10000+1 )
			continue;

		// Selecte Reward type
		switch(bgr->items[i].object_type)
		{
			// Item Reward
			case BGRI_ITEM:
				{
					struct item it;
					struct item_data *i_data = NULL;
					int nameid = atoi(bgr->items[i].object), amount = bgr->items[i].value, n, get_count, weight = 0, i_flag = 0, a_flag;

					if( !nameid || !amount )
						break;

					if( !(i_data = itemdb_exists(nameid)) )
						break;

					memset(&it,0,sizeof(it));
					it.nameid = nameid;
					it.identify = 1;
					it.bound = BOUND_NONE;

					get_count = itemdb_isstackable2(i_data)?amount:1;
					weight += itemdb_weight(nameid)*amount;

					if( (weight+sd->weight) < sd->max_weight )
					{
						for (n = 0; n < amount; n += get_count) {
							if (!pet_create_egg(sd, nameid)) {
								if( (a_flag = pc_additem(sd, &it, get_count, LOG_TYPE_OTHER)) )
									clif_additem(sd, 0, 0, flag);
								else
									i_flag = 1;
							}
						}
					}
					else if( sd->storage.max_amount < MAX_STORAGE && itemdb_canstore(&it,pc_get_group_level(sd)) ) {
						for (n = 0 ; i < amount; n += get_count)
						{
							if( storage_additem(sd, &sd->storage, &it, get_count) == 0 )
								i_flag = 2;
						}

						storage_storageclose(sd);
					}

					switch(i_flag)
					{
						case 1:
							count++;
							sprintf(output, msg_txt(sd,(BG_MSG_TXT+13)), amount, i_data->jname);
							clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
							break;
						case 2:
							count++;
							sprintf(output, msg_txt(sd,(BG_MSG_TXT+14)), amount, i_data->jname);
							clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
							break;
						default:
							break;
					}
				}
				break;
			// Zeny Reward
			case BGRI_ZENY:
				{
					int amount = bgr->items[i].value;

					if( amount > MAX_ZENY-sd->status.zeny )
						amount = MAX_ZENY - sd->status.zeny;

					if( amount > 0 ) {
						count++;
						pc_getzeny(sd, amount, LOG_TYPE_OTHER, NULL);
						sprintf(output, msg_txt(sd,(BG_MSG_TXT+15)), amount, bgr->items[i].desc);
						clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
					}
				}
				break;
			// Cash Point
			case BGRI_CASH:
				{
					int amount = bgr->items[i].value;
				
					if( amount > MAX_ZENY-sd->cashPoints )
						amount = MAX_ZENY - sd->cashPoints; 
					
					if( amount > 0 ) {
						count++;
						pc_setaccountreg(sd, add_str("#CASHPOINTS"), sd->cashPoints+amount);
						sprintf(output, msg_txt(sd,(BG_MSG_TXT+15)), amount, bgr->items[i].desc);
						clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
					}
				}
				break;
			// Kafra Point
			case BGRI_KAFRA:
				{
					int amount = bgr->items[i].value;
			
					if( amount > MAX_ZENY-sd->kafraPoints )
						amount = MAX_ZENY - sd->kafraPoints; 

					if( amount > 0 ) {
						count++;
						pc_setaccountreg(sd, add_str("#KAFRAPOINTS"), sd->kafraPoints+amount);
						sprintf(output, msg_txt(sd,(BG_MSG_TXT+15)), amount, bgr->items[i].desc);
						clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
					}
				}
				break;
			// Base EXP Reward
			case BGRI_BEXP:
				{
					int amount = bgr->items[i].value;
					
					count++;
					pc_gainexp(sd,NULL,amount,0,true);
					sprintf(output, msg_txt(sd,(BG_MSG_TXT+15)), amount, bgr->items[i].desc);
					clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
				}
				break;
			// Job EXP Reward
			case BGRI_JEXP:
				{
					int amount = bgr->items[i].value;
			
					count++;
					pc_gainexp(sd,NULL,0,amount,true);
					sprintf(output, msg_txt(sd,(BG_MSG_TXT+15)), amount, bgr->items[i].desc);
					clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
				}
				break;
			// Base Level Reward
			case BGRI_BLVL:
				{
					int status_point = 0, level = bgr->items[i].value, n;

					if( sd->status.base_level >= pc_maxbaselv(sd) )
						break;

					if( (unsigned int)level > pc_maxbaselv(sd) || (unsigned int)level > (pc_maxbaselv(sd)-sd->status.base_level) )
						level = pc_maxbaselv(sd) - sd->status.base_level;

					for( n = 0; n < level; n++ )
						status_point += pc_gets_status_point(sd->status.base_level + n);
				
					sd->status.status_point += status_point;
					sd->status.base_level += (unsigned int)level;
					status_percent_heal(&sd->bl, 100, 100);
					clif_misceffect(&sd->bl, 0);

					clif_updatestatus(sd, SP_STATUSPOINT);
					clif_updatestatus(sd, SP_BASELEVEL);
					clif_updatestatus(sd, SP_BASEEXP);
					clif_updatestatus(sd, SP_NEXTBASEEXP);
					pc_baselevelchanged(sd);
					if(sd->status.party_id)
						party_send_levelup(sd);

					count++;
					sprintf(output, msg_txt(sd,(BG_MSG_TXT+16)), level, bgr->items[i].desc);
					clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
				}
				break;
			// Job Level Reward
			case BGRI_JLVL:
				{
					int level = bgr->items[i].value;

					if( sd->status.base_level >= pc_maxbaselv(sd) )
						break;

					if( (unsigned int)level > pc_maxjoblv(sd) || (unsigned int)level > (pc_maxjoblv(sd)-sd->status.job_level) )
						level = pc_maxjoblv(sd) - sd->status.job_level;

					sd->status.skill_point += (unsigned int)level;
					sd->status.job_level += (unsigned int)level;
					clif_misceffect(&sd->bl, 1);

					clif_updatestatus(sd, SP_JOBLEVEL);
					clif_updatestatus(sd, SP_JOBEXP);
					clif_updatestatus(sd, SP_NEXTJOBEXP);
					clif_updatestatus(sd, SP_SKILLPOINT);
					status_calc_pc(sd, SCO_FORCE);

					count++;
					sprintf(output, msg_txt(sd,(BG_MSG_TXT+16)), level, bgr->items[i].desc);
					clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
				}
				break;
			// Status
			case BGRI_BONUS_STATUS:
				{
					unsigned short *status[6], max_status[6];
					int s = atoi(bgr->items[i].object), amount = bgr->items[i].value, new_value;

					if( i < 0 || i > MAX_STATUS_TYPE )
						break;

					status[0] = &sd->status.str;
					status[1] = &sd->status.agi;
					status[2] = &sd->status.vit;
					status[3] = &sd->status.int_;
					status[4] = &sd->status.dex;
					status[5] = &sd->status.luk;

					if( pc_has_permission(sd, PC_PERM_BYPASS_MAX_STAT) )
						max_status[0] = max_status[1] = max_status[2] = max_status[3] = max_status[4] = max_status[5] = SHRT_MAX;

					if( amount > 0 && *status[s] + amount >= max_status[s] )
						new_value = max_status[s];
					else if( amount < 0 && *status[s] <= -amount )
						new_value = 1;
					else
						new_value = *status[s] + amount;

					if( new_value != *status[s] )
					{
						*status[s] = new_value;
						clif_updatestatus(sd, SP_STR+s);
						clif_updatestatus(sd, SP_USTR+s);
						status_calc_pc(sd, SCO_FORCE);
						count++;
						sprintf(output, msg_txt(sd,(BG_MSG_TXT+17)), new_value, bgr->items[i].desc);
						clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
					}
				}
				break;
			// Character Variable
			case BGRI_CHAR_VAR:
				{
					int value = pc_readglobalreg(sd, add_str(bgr->items[i].object));

					count++;
					pc_setglobalreg(sd, add_str(bgr->items[i].object), (value+bgr->items[i].value));
					sprintf(output, msg_txt(sd,(BG_MSG_TXT+16)), bgr->items[i].value, bgr->items[i].object);
					clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
				}
				break;
			// Character Variable
			case BGRI_ACC_VAR:
				{
					int value = pc_readaccountreg(sd, add_str(bgr->items[i].object));

					count++;
					pc_setaccountreg(sd, add_str(bgr->items[i].object), (value+bgr->items[i].value));
					sprintf(output, msg_txt(sd,(BG_MSG_TXT+16)), bgr->items[i].value, bgr->items[i].object);
					clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
				}
				break;
			// Unknown type!
			default:
				ShowDebug("bg_reward: Type %d is unknown in Object %s from (arena_id: %d). (CID/AID: %d, %d)\n", bgr->items[i].flag_type, bgr->items[i].object, arena_id, sd->status.char_id, sd->status.account_id);
				return 0;
		}
	}
	
	if( count <= 0 ) {
		sprintf(output, msg_txt(sd,(BG_MSG_TXT+18)));
		clif_messagecolor(&sd->bl, BG_COLOR, output, true, SELF);
	}
	
	return count;
}

int bg_flag_alarm_npc(int tid, unsigned int tick, int id, intptr_t data)
{
	struct npc_data* nd=(struct npc_data *)map_id2bl(id);
	struct battleground_data *bg;

	if( nd && nd->battleground.bg_id && (bg = bg_team_search(nd->battleground.bg_id)) != NULL )
	{
		struct map_session_data *sd;
		struct s_mapiterator* iter = mapit_getallusers();
		int flag = 2;

		if( nd->bl.x != nd->battleground.xs || nd->bl.y != nd->battleground.ys )
			flag = 1;

		for( sd = (TBL_PC*)mapit_first(iter); mapit_exists(iter); sd = (TBL_PC*)mapit_next(iter) )
		{
			if( sd->bl.m != nd->bl.m )
				continue;

			if( sd->bg_flags.nd == nd )
				continue;

			clif_viewpoint(sd,nd->battleground.npc_id,flag,nd->bl.x,nd->bl.y,nd->battleground.n,bg->army?0xFF0000:0x0000FF);
		}

		mapit_free(iter);
		nd->battleground.target_timer = add_timer(gettick()+5000, bg_flag_alarm_npc, nd->bl.id, 0);
		return 1;
	}
	return 0;
}

int bg_flag_alarm_player(int tid, unsigned int tick, int id, intptr_t data)
{
	struct map_session_data *sd = map_id2sd(id);
	struct npc_data *nd = NULL;
	int flag = 0;

	if( sd && sd->bg_flags.nd && sd->bg_flags.target_timer != INVALID_TIMER && (nd = sd->bg_flags.nd) != NULL )
	{
		if( nd->battleground.bg_id == sd->bg_id )
			flag = 1;

		sd->bg_flags.nd->bl.x = sd->bl.x;
		sd->bg_flags.nd->bl.y = sd->bl.y;
		clif_specialeffect(&sd->bl, 60, AREA);
		clif_viewpoint(sd,nd->battleground.npc_id,1,flag?nd->battleground.x:nd->battleground.xs,flag?nd->battleground.y:nd->battleground.ys,nd->battleground.n,0xFFFF00);
		sd->bg_flags.target_timer = add_timer(gettick()+5000, bg_flag_alarm_player, sd->bl.id, 0);
		return 1;
	}
	return 0;
}

int bg_set_npc(int bg_id, struct npc_data *nd, int npc_id, short x, short y, const char *ev, const char *rev)
{
	struct battleground_data *bg = bg_team_search(bg_id);
	int flag = 0;

	if( bg == NULL || nd == NULL )
		return 0;

	if( nd->battleground.bg_id > 0 )
		flag = 1;

	nd->battleground.npc_id = npc_id;
	nd->battleground.bg_id = bg_id;
	nd->battleground.x = nd->bl.x;
	nd->battleground.y = nd->bl.y;
	nd->battleground.xs = x;
	nd->battleground.ys = y;
	nd->battleground.dir = nd->ud.dir;
	nd->battleground.status = 0;
	safestrncpy(nd->battleground.capture_event, ev, sizeof(nd->battleground.capture_event));
	safestrncpy(nd->battleground.recapture_event, rev, sizeof(nd->battleground.recapture_event));

	// Set Touch events.
	if( x || y )
	{
		if( flag <= 0 ) {
			map[nd->bl.m].bg_flags_touch++;
			nd->battleground.n = map[nd->bl.m].bg_flags_touch;
		}
		nd->battleground.target_timer = add_timer(gettick() + 1, bg_flag_alarm_npc, nd->bl.id, 0);
	}
	return nd->bl.id;
}

int bg_flag_drop(struct map_session_data *sd)
{
	struct npc_data *nd;
	nullpo_retr(1, sd);

	if( sd->bg_flags.nd == NULL )
		return 0;

	// Move Npc
	nd = sd->bg_flags.nd;
	nd->bl.x = sd->bl.x;
	nd->bl.y = sd->bl.y;
	nd->ud.dir = sd->ud.dir;
	nd->battleground.status = 0;
	npc_enable(nd->exname, 1);
	npc_movenpc(nd,nd->bl.x,nd->bl.y);

	// Clear Player Events
	sd->bg_flags.nd = NULL;
	sd->bg_flags.target_timer = INVALID_TIMER;
	clif_viewpoint(sd,nd->battleground.npc_id,2,nd->battleground.xs,nd->battleground.ys,nd->battleground.n,0xFFFF00);
	return 1;
}

int bg_flag_capture_area(struct map_session_data* sd, int m, int x, int y)
{
	struct battleground_data *bg;
	int i;
	int f = 1;
	int xs = battle_config.bg_capture_flag_path,ys = battle_config.bg_capture_flag_path;
	char output[200];
	
	nullpo_retr(1, sd);

	if( sd->bg_flags.nd != NULL || pc_isdead(sd) || sd->state.warping )
		return 0;

	for( i=0; i<map[m].npc_num; i++ )
	{
		if( map[m].npc[i]->sc.option&OPTION_INVISIBLE ||
			!map[m].npc[i]->battleground.bg_id ||
			!map[m].npc[i]->battleground.xs ||
			!map[m].npc[i]->battleground.ys ||
			map[m].npc[i]->battleground.status ||
			map[m].npc[i]->subtype != NPCTYPE_SCRIPT
		) {
			f=0; // a npc was found, but it is disabled function, don't print warning
			continue;
		}

		if( x >= map[m].npc[i]->bl.x-xs && x <= map[m].npc[i]->bl.x+xs
		&&  y >= map[m].npc[i]->bl.y-ys && y <= map[m].npc[i]->bl.y+ys )
			break;
	}

	if( i == map[m].npc_num )
	{
		if( f == 1 ) // no npc found
			ShowError("bg_check_flag_areanpc : stray NPC cell on coordinates '%s',%d,%d\n", map[m].name, x, y);
		return 0;
	}
	
	if( map[m].npc[i]->subtype == NPCTYPE_SCRIPT )
	{
		// members do not capture the flag at the base.
		if( map[m].npc[i]->battleground.bg_id == sd->bg_id
			&& map[m].npc[i]->bl.x == map[m].npc[i]->battleground.x
			&& map[m].npc[i]->bl.y == map[m].npc[i]->battleground.y )
			return 0;

		// no battleground found
		if( (bg = bg_team_search(map[m].npc[i]->battleground.bg_id)) == NULL )
			return 0;

		// no battleground map
		if( sd->bl.m != map[m].npc[i]->bl.m )
			return 0;

		sd->bg_flags.nd = map[m].npc[i];
		map[m].npc[i]->battleground.status = 1;
		npc_enable(map[m].npc[i]->exname, 0);
		sd->bg_flags.target_timer = add_timer(gettick() + 1, bg_flag_alarm_player, sd->bl.id, 0);
		clif_viewpoint(sd,map[m].npc[i]->battleground.npc_id,1,map[m].npc[i]->battleground.xs,map[m].npc[i]->battleground.ys,map[m].npc[i]->battleground.n,0xFFFF00);

		// output messages
		sprintf(output, msg_txt(sd,(BG_MSG_TXT+19)), map[m].npc[i]->name);
		clif_messagecolor(&sd->bl, 0x00BFFF, output, true, SELF);
		//sprintf(output, msg_txt(sd,(BG_MSG_TXT+20)), sd->status.name, map[m].npc[i]->name);
		return 1;
	}
	return 0;
}

int bg_flag_catch_area(struct map_session_data* sd, int m, int x, int y)
{
	struct npc_data *nd;
	struct battleground_data *bg;
	char output[200];
	int xs = battle_config.bg_capture_flag_path,ys = battle_config.bg_capture_flag_path;

	nullpo_retr(1, sd);

	if( sd->bg_id <= 0 || pc_isdead(sd) || sd->bg_flags.nd == NULL || sd->state.warping  )
		return 0;
	
	if((nd = sd->bg_flags.nd) == NULL || nd->battleground.bg_id <= 0 || (bg = bg_team_search(nd->battleground.bg_id)) == NULL )
		return 0;

	if( nd->battleground.xs <= 0 && nd->battleground.ys <= 0 )
		return 0;

	// no battleground map
	if( sd->bl.m != nd->bl.m )
		return 0;

	if( sd->bg_id != nd->battleground.bg_id )
	{
		if( x >= nd->battleground.xs-xs && x <= nd->battleground.xs+xs
			&& y >= nd->battleground.ys-ys && y <= nd->battleground.ys+ys )
		{
			nd->bl.x = nd->battleground.xs;
			nd->bl.y = nd->battleground.ys;
			sd->bg_flags.nd = NULL;
			sd->bg_flags.target_timer = INVALID_TIMER;
			increment_limit(sd->bg_score.flag_capture, 1, USHRT_MAX);
			
			// output messages
			sprintf(output, msg_txt(sd,(BG_MSG_TXT+21)), nd->name);
			clif_messagecolor(&sd->bl, 0x00BFFF, output, true, SELF);

			if( nd->battleground.capture_event[0] ) {
				set_var(sd, (char *)"@flag_name$", nd->exname);
				npc_event(sd, nd->battleground.capture_event, 0);
			}
			return 1;
		}
	}
	else {
		if( x >= nd->battleground.x-xs && x <= nd->battleground.x+ys
			&& y >= nd->battleground.y-ys && y <= nd->battleground.y+ys)
		{
			// Move Npc
			nd->bl.x = nd->battleground.x;
			nd->bl.y = nd->battleground.y;
			nd->ud.dir = nd->battleground.dir;
			nd->battleground.status = 0;
			npc_enable(nd->exname, 1);
			npc_movenpc(nd,nd->bl.x,nd->bl.y);

			// Clear Player Event
			sd->bg_flags.nd = NULL;
			sd->bg_flags.target_timer = INVALID_TIMER;
			increment_limit(sd->bg_score.flag_recapture, 1, USHRT_MAX);
			clif_viewpoint(sd,nd->battleground.npc_id,2,nd->battleground.x,nd->battleground.y,nd->battleground.n,0xFFFF00);

			// output messages
			sprintf(output, msg_txt(sd,(BG_MSG_TXT+22)), nd->name);
			clif_messagecolor(&sd->bl, 0x00BFFF, output, true, SELF);

			if( nd->battleground.recapture_event[0] ) {
				set_var(sd, (char *)"@flag_name$", nd->exname);
				npc_event(sd, nd->battleground.recapture_event, 0);
			}
			return 1;
		}
	}
	return 0;
}

int bg_flag_respawn(int m, int bg1_id, int bg2_id)
{
	struct battleground_data *bg;
	struct npc_data *nd;
	int i;

	// Remove Liked Players
	if( bg1_id && (bg = bg_team_search(bg1_id)) != NULL )
	{
		struct map_session_data *sd = NULL;
		for( i=0; i < MAX_BG_MEMBERS; i++ )
		{
			if( (sd = bg->members[i].sd) == NULL )
				continue;

			if( sd->bg_flags.nd != NULL )
			{
				// Clear Player Events
				nd = sd->bg_flags.nd;
				sd->bg_flags.nd = NULL;
				sd->bg_flags.target_timer = INVALID_TIMER;
				clif_viewpoint(sd,nd->battleground.npc_id,2,nd->battleground.xs,nd->battleground.ys,nd->battleground.n,0xFFFF00);
			}
		}
	}

	// Remove Linked Players
	if( bg2_id && (bg = bg_team_search(bg2_id)) != NULL )
	{
		struct map_session_data *sd = NULL;
		for( i=0; i < MAX_BG_MEMBERS; i++ )
		{
			if( (sd = bg->members[i].sd) == NULL )
				continue;

			if( sd->bg_flags.nd != NULL )
			{
				// Clear Player Events
				nd = sd->bg_flags.nd;
				sd->bg_flags.nd = NULL;
				sd->bg_flags.target_timer = INVALID_TIMER;
				clif_viewpoint(sd,nd->battleground.npc_id,2,nd->battleground.xs,nd->battleground.ys,nd->battleground.n,0xFFFF00);
			}
		}
	}

	for( i=0; i<map[m].npc_num; i++ )
	{
		nd = map[m].npc[i];
		if( !nd->battleground.bg_id || !nd->battleground.xs || !nd->battleground.ys )
			continue;

		// Disable to move...
		if( nd->sc.option&OPTION_INVISIBLE )
			npc_enable(nd->exname, 0);

		nd->battleground.status = 1; // prev capture
		nd->bl.x = nd->battleground.x;
		nd->bl.y = nd->battleground.y;
		nd->ud.dir = nd->battleground.dir;
		npc_movenpc(nd,nd->bl.x,nd->bl.y);
		npc_enable(nd->exname, 1);
		nd->battleground.status = 0; // status dropped
		
	}
	return 1;	
}

int bg_guild_refresh_all(struct battleground_data *bg)
{
	int i;

	if( bg == NULL )
		return 0;

	for( i=0; i < MAX_BG_MEMBERS; i++ )
	{
		struct map_session_data* sd = bg->members[i].sd;
		if( sd == NULL )
			continue;
		
		clif_bg_basicinfo(sd);
		clif_guild_emblem(sd,bg->g);
		clif_bg_memberlist(sd);
	}
	bg_refresh_patent(bg->bg_id);
	return 0;
}

void do_bg_reward_reload(void)
{
	bg_reward_db->clear(bg_reward_db, NULL);
	bg_load_rewards_from_sql();
}

void do_init_bgwarfare(void)
{
	bg_reward_db = idb_alloc(DB_OPT_RELEASE_DATA);
	add_timer_func_list(bg_report_afk, "bg_report_afk");
	add_timer_func_list(bg_respawn_timer, "bg_respawn_timer");
	add_timer_func_list(bg_digit_timer, "bg_digit_timer");
	add_timer_func_list(bg_flag_alarm_npc, "bg_flag_alarm_npc");
	add_timer_func_list(bg_flag_alarm_player, "bg_flag_alarm_player");

	// Create Fake Guilds
	bg_create_guild();

	// Load Rewards from SQL
	bg_load_rewards_from_sql();

	ShowMessage(CL_WHITE"[BattleGround]: " CL_RESET " BattleGround Warfare (version: %s) successfully initialized.\n", bg_version);
	ShowMessage(CL_WHITE"[BattleGround]: " CL_RESET " by (c) CreativeSD, suport in www.creativesd.com.br\n");
}

void do_final_bgwarfare()
{
	bg_reward_db->destroy(bg_reward_db, NULL);
}


// script.cpp functions
//
int buildin_bg_create_team(struct script_state* st)
{
	struct battleground_data *bg;
	const char *map_name, *ev, *dev, *wev;
	int bg_id, x, y, rx, ry, army, map_index = 0;

	bg_id = script_getnum(st, 2);

	if (bg_id <= 0)
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	map_name = script_getstr(st, 3);
	if (strcmp(map_name, "-") != 0) {
		map_index = mapindex_name2id(map_name);
		if (map_index == 0) { // Invalid Map
			script_pushint(st, 0);
			return SCRIPT_CMD_SUCCESS;
		}
	}

	x = script_getnum(st, 4);
	y = script_getnum(st, 5);
	rx = script_getnum(st, 6);
	ry = script_getnum(st, 7);
	army = script_getnum(st, 8);
	ev = script_getstr(st, 9);
	dev = script_getstr(st, 10);
	wev = script_getstr(st, 11);

	if ((bg = bg_team_search(bg_id)) != NULL)
	{
		bg->x = x;
		bg->y = y;
		bg->respawn_x = rx;
		bg->respawn_y = ry;
		safestrncpy(bg->logout_event, ev, sizeof(bg->logout_event));
		safestrncpy(bg->die_event, dev, sizeof(bg->die_event));
		safestrncpy(bg->without_event, wev, sizeof(bg->without_event));

		if (bg->army != army) {
			bg->army = army;
			bg->g = &bg_guild[army];
			bg_guild_refresh_all(bg);
		}

		script_pushint(st, bg_id);
		return SCRIPT_CMD_SUCCESS;
	}

	if ((bg_id = bg_create(bg_id, map_index, x, y, rx, ry, army, ev, dev, wev)) == 0) { // Creation failed
		script_pushint(st, -1);
	}
	else
		script_pushint(st, bg_id);

	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_join_team(struct script_state* st)
{
	TBL_PC* sd = NULL;
	int team_id = script_getnum(st, 2);
	int flag = 0;

	if (script_hasdata(st, 3))
		flag = script_getnum(st, 3);

	if (script_hasdata(st, 4))
		sd = map_id2sd(script_getnum(st, 4));
	else
		script_rid2sd(sd);

	if (!sd)
		script_pushint(st, 0);
	else
		script_pushint(st, bg_team_join(team_id, sd, flag) ? 0 : 1);

	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_kick_team(struct script_state* st)
{
	int i, bg_id = script_getnum(st, 2);
	struct map_session_data *sd = NULL;
	struct battleground_data *bg = bg_team_search(bg_id);
	int delay = -1;

	if (script_hasdata(st, 3))
		delay = script_getnum(st, 3);

	if (bg == NULL)
		script_pushint(st, 0); // Equipe n�o foi encontrada ou a id � inv�lida.
	else {
		for (i = 0; i < MAX_BG_MEMBERS; i++)
		{
			if ((sd = bg->members[i].sd) == NULL)
				continue;
			else {
				bg_team_leave(sd, BGTL_LEFT);
				if (delay >= 0) sd->status.queue_delay = (int)time(NULL) + delay;
			}
		}
	}

	script_pushint(st, 1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_team_search(struct script_state* st)
{
	struct battleground_data *bg;
	int bg_id = script_getnum(st, 0);

	if (bg_id <= 0 || (bg = bg_team_search(bg_id)))
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, bg->bg_id);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_team_size(struct script_state* st)
{
	struct battleground_data *bg;
	int bg_id = script_getnum(st, 2);
	int i, j = 0;

	if ((bg = bg_team_search(bg_id)) == NULL)
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	for (i = 0; i < MAX_BG_MEMBERS; i++) {
		struct map_session_data *sd = bg->members[i].sd;
		if (sd == NULL)
			continue;

		j++;
	}
	script_pushint(st, j);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_get_team(struct script_state* st)
{
	struct battleground_data *bg;
	int bg_id = script_getnum(st, 2);
	int i, j = 0;

	if ((bg = bg_team_search(bg_id)) == NULL)
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	for (i = 0; i < MAX_BG_MEMBERS; i++) {
		struct map_session_data *sd = bg->members[i].sd;
		if (sd == NULL)
			continue;

		set_reg(st, NULL, reference_uid(add_str(".@bg_members_cid"), j), ".@bg_members_cid", (void*)__64BPRTSIZE(sd->status.char_id), NULL);
		set_reg(st, NULL, reference_uid(add_str(".@bg_members_aid"), j), ".@bg_members_aid", (void*)__64BPRTSIZE(sd->status.account_id), NULL);
		j++;
	}

	set_reg(st, NULL, add_str(".@bg_members_count"), ".@bg_members_count", (void*)__64BPRTSIZE(j), NULL);
	script_pushint(st, j);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_set_respawn(struct script_state* st)
{
	struct battleground_data *bg = bg_team_search(script_getnum(st, 2));

	if (bg == NULL)
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	bg->respawn_x = script_getnum(st, 3);
	bg->respawn_y = script_getnum(st, 4);
	script_pushint(st, 1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_attach_respawn(struct script_state* st)
{
	TBL_PC* sd = NULL;
	struct battleground_data *bg = NULL;
	int timer = 1;

	if (script_hasdata(st, 2))
		timer = script_getnum(st, 2);

	if (script_hasdata(st, 3))
		sd = map_charid2sd(script_getnum(st, 3));
	else
		script_rid2sd(sd);

	if (sd == NULL || !sd->bg_id || (bg = bg_team_search(sd->bg_id)) == NULL)
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	sd->bg_respawn_timer_count = 10;
	sd->bg_respawn_timer = add_timer(gettick() + timer, bg_respawn_timer, sd->bl.id, 0);
	script_pushint(st, 1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_attach_respawn_all(struct script_state* st)
{
	struct map_session_data *sd = NULL;
	struct battleground_data *bg = bg_team_search(script_getnum(st, 2));
	int timer = 1, i;

	if (bg == NULL)
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	if (script_hasdata(st, 3))
		timer = script_getnum(st, 3);

	for (i = 0; i < MAX_BG_MEMBERS; i++) {
		sd = bg->members[i].sd;
		if (sd == NULL)
			continue;

		sd->bg_respawn_timer_count = 10;
		sd->bg_respawn_timer = add_timer(gettick() + timer, bg_respawn_timer, sd->bl.id, 0);
	}
	script_pushint(st, 1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_viewpoint(struct script_state* st)
{
	struct battleground_data *bg;
	int bg_id, type, x, y, id, color, i;

	bg_id = script_getnum(st, 2);
	type = script_getnum(st, 3);
	x = script_getnum(st, 4);
	y = script_getnum(st, 5);
	id = script_getnum(st, 6);
	color = script_getnum(st, 7);

	bg = bg_team_search(bg_id);

	if (bg == NULL)
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	for (i = 0; i < MAX_BG_MEMBERS; i++) {
		struct map_session_data *sd = bg->members[i].sd;
		if (sd == NULL)
			continue;

		clif_viewpoint(sd, st->oid, type, x, y, id, color);
	}

	script_pushint(st, 1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_areapercentheal(struct script_state* st)
{
	const char *str;
	int16 m, x0, y0, x1, y1;
	int bg_id, hp, sp;
	int i = 0;
	struct battleground_data *bg = NULL;

	bg_id = script_getnum(st, 2);
	str = script_getstr(st, 3);

	if ((bg = bg_team_search(bg_id)) == NULL || (m = map_mapname2mapid(str)) < 0) {
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	x0 = script_getnum(st, 4);
	y0 = script_getnum(st, 5);
	x1 = script_getnum(st, 6);
	y1 = script_getnum(st, 7);
	hp = script_getnum(st, 8);
	sp = script_getnum(st, 9);

	for (i = 0; i < MAX_BG_MEMBERS; i++) {
		struct map_session_data *sd = bg->members[i].sd;
		if (sd == NULL)
			continue;
		if (sd->bl.m != m || sd->bl.x < x0 || sd->bl.y < y0 || sd->bl.x > x1 || sd->bl.y > y1)
			continue;

		pc_percentheal(sd, hp, sp);
	}
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_team_getxy(struct script_state* st)
{
	struct battleground_data *bg = bg_team_search(script_getnum(st, 2));

	if (bg == NULL)
		return SCRIPT_CMD_SUCCESS;

	if (!script_getnum(st, 3))
		script_pushint(st, bg->x);
	else
		script_pushint(st, bg->y);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_respawn_getxy(struct script_state* st)
{
	struct battleground_data *bg = bg_team_search(script_getnum(st, 2));

	if (bg == NULL)
		return SCRIPT_CMD_SUCCESS;

	if (!script_getnum(st, 3))
		script_pushint(st, bg->respawn_x);
	else
		script_pushint(st, bg->respawn_y);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_refresh_patent(struct script_state* st)
{
	struct battleground_data *bg;
	int bg_id = script_getnum(st, 2);

	if (!battle_config.bg_name_position)
		return SCRIPT_CMD_SUCCESS;

	bg = bg_team_search(bg_id);

	if (bg == NULL)
		return SCRIPT_CMD_SUCCESS;

	bg_refresh_patent(bg_id);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_message(struct script_state* st)
{
	const char *msg;
	int i;
	struct map_session_data *sd;
	struct battleground_data *bg = bg_team_search(script_getnum(st, 2));

	if (bg == NULL)
		return SCRIPT_CMD_SUCCESS;

	msg = script_getstr(st, 3);
	for (i = 0; i < MAX_BG_MEMBERS; i++)
	{
		if ((sd = bg->members[i].sd) == NULL)
			continue;

		clif_messagecolor(&sd->bl, BG_COLOR, msg, true, SELF);
	}
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_digit_timer(struct script_state* st)
{
	struct battleground_data *bg = bg_team_search(script_getnum(st, 2));
	int value = 0;

	if (bg == NULL)
		return SCRIPT_CMD_SUCCESS;

	if (script_hasdata(st, 3))
		value = script_getnum(st, 3);

	if (value > 0) {
		bg->timerdigit_count = value + 1;
		bg->timerdigit = add_timer(gettick() + 1, bg_digit_timer, bg->bg_id, 0);
	}
	else {
		bg->timerdigit_count = 0;
		bg->timerdigit = add_timer(gettick(), bg_digit_timer, bg->bg_id, 0);
	}
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_reward(struct script_state* st)
{
	TBL_PC* sd = NULL;
	struct battleground_rewards *bgr;
	int arena_id = script_getnum(st, 2), type = script_getnum(st, 3), count;

	if (script_hasdata(st, 4))
		sd = map_charid2sd(script_getnum(st, 4));
	else
		script_rid2sd(sd);

	if (sd == NULL)
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	if ((bgr = bg_search_reward(arena_id)) == NULL)
	{
		ShowError("script:bg_reward: Battleground (ID: %d) not found!\n", arena_id);
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	if (type < BGRT_WIN || type > BGRT_WO)
	{
		ShowError("script:bg_reward: Flag type (%d) incompatible.\n", type);
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	count = bg_reward(sd, arena_id, (enum bg_reward_type)type);
	script_pushint(st, count);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_get_score(struct script_state* st)
{
	TBL_PC* sd = NULL;
	int type = script_getnum(st, 2), value = 0;

	if (script_hasdata(st, 3))
		sd = map_charid2sd(script_getnum(st, 3));
	else
		script_rid2sd(sd);

	if (sd == NULL)
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	switch (type)
	{
	case 0:  value = sd->bg_score.skills_support_success; break;
	case 1:  value = sd->bg_score.skills_support_fail; break;
	case 2:  value = sd->bg_score.heal_hp; break;
	case 3:  value = sd->bg_score.heal_sp; break;
	case 4:  value = sd->bg_score.item_heal_hp; break;
	case 5:  value = sd->bg_score.item_heal_sp; break;
	case 6:  value = sd->bg_score.item_heal; break;
	case 7:  value = sd->bg_score.ammos; break;
	case 8:  value = sd->bg_score.poison_bottle; break;
	case 9:  value = sd->bg_score.fire_bottle; break;
	case 10: value = sd->bg_score.acid_bottle; break;
	case 11: value = sd->bg_score.oridecon; break;
	case 12: value = sd->bg_score.elunium; break;
	case 13: value = sd->bg_score.steel; break;
	case 14: value = sd->bg_score.emveretarcon; break;
	case 15: value = sd->bg_score.wooden_block; break;
	case 16: value = sd->bg_score.stone; break;
	case 17: value = sd->bg_score.yellow_gemstone; break;
	case 18: value = sd->bg_score.red_gemstone; break;
	case 19: value = sd->bg_score.blue_gemstone; break;
	case 20: value = sd->bg_score.player_kills; break;
	case 21: value = sd->bg_score.player_deaths; break;
	case 22: value = sd->bg_score.player_damage_given; break;
	case 23: value = sd->bg_score.player_damage_taken; break;
	case 24: value = sd->bg_score.runestone_kills; break;
	case 25: value = sd->bg_score.runestone_damage; break;
	case 26: value = sd->bg_score.runestone_repair; break;
	case 27: value = sd->bg_score.emperium_kills; break;
	case 28: value = sd->bg_score.emperium_damage; break;
	case 29: value = sd->bg_score.barrier_kills; break;
	case 30: value = sd->bg_score.barrier_damage; break;
	case 31: value = sd->bg_score.barrier_repair; break;
	case 32: value = sd->bg_score.objective_kills; break;
	case 33: value = sd->bg_score.objective_damage; break;
	case 34: value = sd->bg_score.flag_kills; break;
	case 35: value = sd->bg_score.flag_damage; break;
	case 36: value = sd->bg_score.flag_capture; break;
	case 37: value = sd->bg_score.flag_recapture; break;
	case 38: value = sd->bg_score.crystal_kills; break;
	case 39: value = sd->bg_score.crystal_damage; break;
	case 40: value = sd->bg_score.guardian_kills; break;
	case 41: value = sd->bg_score.guardian_deaths; break;
	case 42: value = sd->bg_score.guardian_damage_given; break;
	case 43: value = sd->bg_score.guardian_damage_taken; break;
	default:
		script_pushint(st, 0);
		ShowError("script:bg_get_score: Unknow type (%d)\n", type);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, value);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_set_score(struct script_state* st)
{
	TBL_PC* sd = NULL;
	int type = script_getnum(st, 2), value = script_getnum(st, 3);

	if (script_hasdata(st, 4))
		sd = map_charid2sd(script_getnum(st, 4));
	else
		script_rid2sd(sd);

	if (sd == NULL)
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	if (value > USHRT_MAX)
		value = USHRT_MAX;

	switch (type)
	{
	case 0:  sd->bg_score.skills_support_success = value; break;
	case 1:  sd->bg_score.skills_support_fail = value; break;
	case 2:  sd->bg_score.heal_hp = value; break;
	case 3:  sd->bg_score.heal_sp = value; break;
	case 4:  sd->bg_score.item_heal_hp = value; break;
	case 5:  sd->bg_score.item_heal_sp = value; break;
	case 6:  sd->bg_score.item_heal = value; break;
	case 7:  sd->bg_score.ammos = value; break;
	case 8:  sd->bg_score.poison_bottle = value; break;
	case 9:  sd->bg_score.fire_bottle = value; break;
	case 10:  sd->bg_score.acid_bottle = value; break;
	case 11: sd->bg_score.oridecon = value; break;
	case 12: sd->bg_score.elunium = value; break;
	case 13: sd->bg_score.steel = value; break;
	case 14: sd->bg_score.emveretarcon = value; break;
	case 15: sd->bg_score.wooden_block = value; break;
	case 16: sd->bg_score.stone = value; break;
	case 17: sd->bg_score.yellow_gemstone = value; break;
	case 18: sd->bg_score.red_gemstone = value; break;
	case 19: sd->bg_score.blue_gemstone = value; break;
	case 20: sd->bg_score.player_kills = value; break;
	case 21: sd->bg_score.player_deaths = value; break;
	case 22: sd->bg_score.player_damage_given = value; break;
	case 23: sd->bg_score.player_damage_taken = value; break;
	case 24: sd->bg_score.runestone_kills = value; break;
	case 25: sd->bg_score.runestone_damage = value; break;
	case 26: sd->bg_score.runestone_repair = value; break;
	case 27: sd->bg_score.emperium_kills = value; break;
	case 28: sd->bg_score.emperium_damage = value; break;
	case 29: sd->bg_score.barrier_kills = value; break;
	case 30: sd->bg_score.barrier_damage = value; break;
	case 31: sd->bg_score.barrier_repair = value; break;
	case 32: sd->bg_score.objective_kills = value; break;
	case 33: sd->bg_score.objective_damage = value; break;
	case 34: sd->bg_score.flag_kills = value; break;
	case 35: sd->bg_score.flag_damage = value; break;
	case 36: sd->bg_score.flag_capture = value; break;
	case 37: sd->bg_score.flag_recapture = value; break;
	case 38: sd->bg_score.crystal_kills = value; break;
	case 39: sd->bg_score.crystal_damage = value; break;
	case 40: sd->bg_score.guardian_kills = value; break;
	case 41: sd->bg_score.guardian_deaths = value; break;
	case 42: sd->bg_score.guardian_damage_given = value; break;
	case 43: sd->bg_score.guardian_damage_taken = value; break;
	default:
		script_pushint(st, 0);
		ShowError("script:bg_set_score: Unknow type (%d)\n", type);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, 1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_set_npc(struct script_state* st)
{
	TBL_NPC* nd;
	int bg_id = script_getnum(st, 2), x = 0, y = 0;
	const char *ev = "", *rev = "";

	if (bg_id < 0)
		return SCRIPT_CMD_SUCCESS;

	if (script_hasdata(st, 3))
		x = script_getnum(st, 3);
	if (script_hasdata(st, 4))
		y = script_getnum(st, 4);
	if (script_hasdata(st, 5))
		ev = script_getstr(st, 5);
	if (script_hasdata(st, 6))
		rev = script_getstr(st, 6);

	check_event(st, ev);
	check_event(st, rev);

	nd = (TBL_NPC*)map_id2nd(st->oid);
	if (nd == NULL)
		ShowError("script:bg_set_npc: npc %d not found\n", st->oid);
	//else if (nd->subtype != NPCTYPE_SCRIPT)
	//	ShowError("script:bg_set_npc: unexpected subtype %d -> %d for npc %d '%s'\n", nd->subtype, NPCTYPE_SCRIPT, st->oid, nd->exname);
	else if (bg_set_npc(bg_id, nd, st->oid, x, y, ev, rev)) {
		clif_guild_emblem_area(&nd->bl);
		script_pushint(st, 1);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, 0);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_npc_getid(struct script_state* st)
{
	TBL_NPC* nd = map_id2nd(st->oid);

	if (!nd) {
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, nd->battleground.bg_id);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_flag_click(struct script_state* st)
{
	TBL_PC* sd;
	TBL_NPC *nd = (TBL_NPC*)map_id2nd(st->oid);

	if (nd == NULL)
		ShowError("script:bg_flag_click: npc %d not found\n", st->oid);
	else if (!script_rid2sd(sd))
		ShowError("script:bg_flag_click: Player not attached!\n");
	else if (nd->battleground.bg_id && (nd->battleground.xs > 0 || nd->battleground.ys > 0) && sd->bg_flags.nd == NULL)
	{
		bg_flag_capture_area(sd, sd->bl.m, sd->bl.x, sd->bl.y);
		script_pushint(st, 1);
		return SCRIPT_CMD_SUCCESS;
	}
	script_pushint(st, 0);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_flag_respawn(struct script_state* st)
{
	const char *map_name;
	int16 m;
	int bg1_id = 0, bg2_id = 0;

	map_name = script_getstr(st, 2);
	if (script_hasdata(st, 3))
		bg1_id = script_getnum(st, 3);
	if (script_hasdata(st, 4))
		bg2_id = script_getnum(st, 4);

	if ((m = map_mapname2mapid(map_name)) < 0)
	{ // Invalid Map
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	bg_flag_respawn(m, bg1_id, bg2_id);
	script_pushint(st, 1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_bg_console(struct script_state* st)
{
	const char *str;
	str = script_getstr(st, 2);
	ShowMessage(CL_WHITE "[BattleGround]:" CL_RESET " %s\n", str);
	return SCRIPT_CMD_SUCCESS;
}

// atcommand.cpp
//
int atcommand_reloadbg(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("BattleGround#queue_main");
	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, (BG_MSG_TXT + 23)));
		return -1;
	}

	npc_event(sd, "BattleGround#queue_main::OnCmdReload", 0);
	clif_messagecolor(&sd->bl, BG_COLOR, msg_txt(sd, (BG_MSG_TXT + 24)), true, SELF);
	return 0;
}

int atcommand_bgregister(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("BattleGround#cmd_register");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, (BG_MSG_TXT + 25)));
		return -1;
	}

	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_bgjoin(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("BattleGround#cmd_join");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, (BG_MSG_TXT + 26)));
		return -1;
	}

	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_bgleave(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	int16 m = sd->bl.m;
	if (sd->bg_id)
	{
		if (map_getmapflag(m, MF_BATTLEGROUND))
		{
			bg_team_leave(sd, BGTL_QUIT);
			if (!sd->bg_id)
			{
				if (battle_config.bg_afk_warp_save_point)
					pc_setpos(sd, sd->status.save_point.map, sd->status.save_point.x, sd->status.save_point.y, CLR_TELEPORT);
				else {
					unsigned short map_index = mapindex_name2id("bat_room");
					if (map_index)
						pc_setpos(sd, map_index, 154, 150, CLR_TELEPORT);
				}
				return 0;
			}
			else {
				clif_displaymessage(sd->fd, msg_txt(sd, (BG_MSG_TXT + 27)));
				return 0;
			}
		}
		else {
			// N�o ativar labeis de eventos por estar fora da arena.
			bg_team_leave(sd, BGTL_LEFT);
			if (sd->bg_id)
			{
				clif_displaymessage(sd->fd, msg_txt(sd, (BG_MSG_TXT + 28)));
				return 1;
			}
			else {
				clif_displaymessage(sd->fd, msg_txt(sd, (BG_MSG_TXT + 27)));
				return -1;
			}
		}
	}
	else {
		clif_displaymessage(sd->fd, msg_txt(sd, (BG_MSG_TXT + 29)));
		return -1;
	}
}

int atcommand_bgunregister(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("BattleGround#cmd_unregister");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, (BG_MSG_TXT + 25)));
		return -1;
	}

	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_bgreportafk(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct map_session_data *pl_sd = NULL;
	char output[128];
	
	memset(atcmd_player_name, '\0', sizeof(atcmd_player_name));
	if (!*message || !*message || sscanf(message, "%23[^\n]", atcmd_player_name) < 1)
	{
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+30)));
		return -1;
	}

	if ((pl_sd = map_nick2sd(atcmd_player_name,true)) == NULL && (pl_sd = map_charid2sd(atoi(atcmd_player_name))) == NULL)
	{
		sprintf(atcmd_output, msg_txt(sd,(BG_MSG_TXT+31)), atcmd_player_name);
		clif_displaymessage(fd, atcmd_output);
		return -1;
	}

	if (!pl_sd->bg_id)
	{
		sprintf(atcmd_output, msg_txt(sd,(BG_MSG_TXT+32)), message);
		clif_displaymessage(fd, atcmd_output);
		return -1;
	}

	if (battle_config.bg_afk_team_report && sd->bg_id != pl_sd->bg_id)
	{
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+33)));
		return -1;
	}

	if (pl_sd->npc_id)
	{
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+34)));
		return -1;
	}

	if (!DIFF_TICK(last_tick, pl_sd->idletime))
	{
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+35)));
		return -1;
	}

	if (pl_sd->bg_afk_timer && pl_sd->bg_afk_timer != INVALID_TIMER)
	{
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+36)));
		return -1;
	}

	clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+37)));
	sprintf(output, msg_txt(sd,(BG_MSG_TXT+38)), battle_config.bg_afk_timer);
	clif_messagecolor(&pl_sd->bl, BG_COLOR, output, true, SELF);

	pl_sd->bg_afk_timer = add_timer(gettick() + (battle_config.bg_afk_timer * 1000), bg_report_afk, pl_sd->bl.id, 0);
	return 0;
}

int atcommand_bgwaiting(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd;
	struct script_state *st;
	int arg1 = 0, arg2 = 0, count = 0, nargs = 0;

	if (strcmpi("bgwaiting",command))
	{
		nd = npc_name2id("BattleGround#cmd_waiting");
		if (nd == NULL)
		{
			clif_displaymessage(sd->fd, msg_txt(sd,(BG_MSG_TXT+39)));
			return -1;
		}
	}
	else
	{
		nd = npc_name2id("BattleGround#cmd_playing");
		if (nd == NULL)
		{
			clif_displaymessage(sd->fd, msg_txt(sd,(BG_MSG_TXT+40)));
			return -1;
		}
	}

	if (*message)
		count = sscanf(message, "%2d %1d", &arg1, &arg2);

	st = script_alloc_state(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);

	if (count > 0)
		setd_sub(st, NULL, ".@atcmd_parameters", nargs++, (void*)__64BPRTSIZE(arg1), NULL);

	if (count > 1)
		setd_sub(st, NULL, ".@atcmd_parameters", nargs++, (void*)__64BPRTSIZE(arg2), NULL);

	setd_sub(st, NULL, ".@atcmd_numparameters", 0, (void*)__64BPRTSIZE(nargs), NULL);
	run_script_main(st);
	return 0;
}

int atcommand_bgranking(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd;
	struct script_state *st;
	int arg1 = 0, arg2 = 0, arg3 = 0, count = 0, nargs = 0;

	nd = npc_name2id("BattleGround#cmd_ranking");
	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd,(BG_MSG_TXT+41)));
		return -1;
	}

	if (*message)
		count = sscanf(message, "%2d %1d %2d", &arg1, &arg2, &arg3);

	st = script_alloc_state(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);

	if (count > 0)
		setd_sub(st, NULL, ".@atcmd_parameters", nargs++, (void*)__64BPRTSIZE(arg1), NULL);

	if (count > 1)
		setd_sub(st, NULL, ".@atcmd_parameters", nargs++, (void*)__64BPRTSIZE(arg2), NULL);

	if (count > 2)
		setd_sub(st, NULL, ".@atcmd_parameters", nargs++, (void*)__64BPRTSIZE(arg3), NULL);

	setd_sub(st, NULL, ".@atcmd_numparameters", 0, (void*)__64BPRTSIZE(nargs), NULL);
	run_script_main(st);
	return 0;
}

int atcommand_bgorder(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct battleground_data *bg = NULL;
	nullpo_retr(-1,sd);

	if( !message || !*message )
	{
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+42)));
		return -1;
	}

	if( map_getmapflag(sd->bl.m,MF_BATTLEGROUND) && sd->bg_id )
	{
		if( (bg = bg_team_search(sd->bg_id)) == NULL && bg->master_id != sd->status.char_id )
		{
			clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+43)));
			return -1;
		}
		clif_broadcast2(&sd->bl, message, (int)strlen(message)+1, BG_COLOR, 0x190, 12, 0, 0, BG);
	}
	else {
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+44)));
		return -1;
	}

	return 0;
}

int atcommand_bgchangeleader(const int fd, struct map_session_data* sd, const char* command, const char* message) {
	struct battleground_data *bg = NULL;
	struct map_session_data *pl_sd = NULL;
	struct map_session_data *tsd = NULL;
	char output[128];
	int i, showlist = 0;

	nullpo_retr(-1, sd);

	memset(atcmd_player_name, '\0', sizeof(atcmd_player_name));

	if (!sd->bg_id)
	{
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+45)));
		return -1;
	}

	bg = bg_team_search(sd->bg_id);
	if (bg == NULL || bg->master_id != sd->status.char_id )
	{
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+46)));
		return -1;
	}
	else if (bg->count <= 1)
	{
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+47)));
		return -1;
	}
	else if (!message || !*message || sscanf(message, "%23[^\n]", atcmd_player_name) < 1)
		showlist = 1;
	else if ((pl_sd = map_nick2sd(atcmd_player_name,true)) == NULL || pl_sd->bg_id != sd->bg_id) {
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+48)));
		showlist = 1;
	}
	else {
		if (bg_change_master(sd->bg_id, pl_sd->status.char_id, sd))
		{
			sprintf(output, msg_txt(sd,(BG_MSG_TXT+49)), pl_sd->status.name);
			clif_displaymessage(fd, output);
			return 0;
		}
		else {
			sprintf(output, msg_txt(sd,(BG_MSG_TXT+50)), pl_sd->status.name);
			clif_displaymessage(fd, output);
			showlist = 1;
		}
	}

	if (showlist)
	{
		clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+51)));
		for (i = 0; i < bg->count; i++)
		{
			if (bg->members[i].sd == sd)
				continue;

			tsd = bg->members[i].sd;
			if (tsd == NULL)
				continue;

			sprintf(output, msg_txt(sd,(BG_MSG_TXT+52)), tsd->status.name, tsd->status.base_level, tsd->status.job_level, job_name(tsd->status.class_));
			clif_displaymessage(fd, output);
		}
	}

	return -1;
}

int atcommand_bgreloadrewards(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	do_bg_reward_reload();
	clif_displaymessage(fd, msg_txt(sd,(BG_MSG_TXT+53)));
	return 0;
}
#endif

#ifdef QUEUE_SYSTEM_ENABLE

// Queue System
static DBMap* queue_db;
static unsigned int queue_counter = 0;

struct queue_data* queue_search(int queue_id)
{
	if( !queue_id ) return NULL;
	return (struct queue_data *)idb_get(queue_db, queue_id);
}

int queue_create(int queue_id, const char *name, int min_level, int max_level, const char *event)
{
	struct queue_data *queue;
	queue_counter++;
	
	CREATE(queue, struct queue_data, 1);
	queue->queue_id = queue_id;
	queue->count = 0;
	queue->min_level = min_level;
	queue->max_level = max_level;
	safestrncpy(queue->name, name, sizeof(queue->name));
	safestrncpy(queue->join_event, event, sizeof(queue->join_event));
	queue->first = queue->last = NULL;
	idb_put(queue_db, queue_id, queue);
	ShowStatus("queue_create: The queue (%d: %s) was successfully created.\n", queue->queue_id, queue->name);
	return true;
}

int queue_delete(int queue_id)
{
	struct queue_data *queue = queue_search(queue_id);
	char name[QUEUE_NAME_LENGTH];

	if( queue == NULL ) return 0;
	queue_counter--;
	queue_clean(queue,0,3);
	safestrncpy(name, queue->name, sizeof(name));
	idb_remove(queue_db, queue_id);
	ShowStatus("queue_delete: The row (%d: %s) was successfully removed.\n", queue->queue_id, name);
	return true;
}

int queue_join(struct map_session_data *sd, int queue_id, int flag) {
	struct queue_data *queue = queue_search(queue_id);
	struct queue_players *members = NULL;
	char output[200];

	if (queue == NULL || sd == NULL)
		return false;

	// Remove from the last queue and insert the new.
	if( flag < 2 && sd->queue_id )
		queue_leave(sd,0);

	if( flag < 2 && queue->min_level && sd->status.base_level < queue->min_level )
	{
		sprintf(output, msg_txt(sd,QUEUE_MSG_TXT), queue->name);
		clif_displaymessage(sd->fd, output);
		sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT)+1), queue->min_level);
		clif_displaymessage(sd->fd, output);
		return 0;
	}
	
	if( flag < 2 && queue->max_level && sd->status.base_level > queue->max_level )
	{
		sprintf(output, msg_txt(sd, QUEUE_MSG_TXT), queue->name);
		clif_displaymessage(sd->fd, output);
		sprintf(output, msg_txt(sd, (QUEUE_MSG_TXT)+2), queue->max_level);
		clif_displaymessage(sd->fd, output);
		return 0;
	}

	CREATE(members, struct queue_players, 1);
	queue->count++;
	members->sd = sd;
	members->position = queue->count;
	members->next = NULL;
	sd->queue_id = queue_id;

	if( queue->last == NULL )
		queue->first = queue->last = members;
	else {
		queue->last->next = members;
		queue->last = members;
	}
	sd->status.queue_delay = (int)time(NULL)+battle_config.queue_join_delay;
	ShowInfo("Player '%s' joined in queue (%d: %s).\n", sd->status.name, queue_id, queue->name);

	if( battle_config.queue_notify == 1 || battle_config.queue_notify == 3 )
		queue_join_notify(queue_id, sd);

	if( queue->join_event[0] && flag )
	{
		if( !npc_event_do(queue->join_event) )
			ShowError("queue_join: '%s' not found in (queue_id: %d)!\n", queue->join_event, queue_id);
	}
	return true;
}

int queue_leave(struct map_session_data *sd, int flag)
{
	struct queue_data *queue = NULL;
	struct queue_players *head, *previous;
	int queue_id;
	
	if( sd == NULL)
		return false;

	queue_id = sd->queue_id;
	queue = queue_search(queue_id);

	if( queue == NULL )
		return false;

	head = queue->first;
	previous = NULL;

	while( head != NULL )
	{
		if( head->sd && head->sd == sd )
		{
			struct queue_players *next = head->next;
		
			sd->queue_id = 0;
			queue->count--;

			if( previous )
				previous->next = head->next;
			else
				queue->first = head->next;

			if( head->next == NULL )
				queue->last = previous;

			while( next != NULL )
			{
				next->position--;
				next = next->next;
			}

			ShowInfo("Player '%s' removed from queue (%d: %s).\n", sd->status.name, queue_id, queue->name);
			if( flag && battle_config.queue_notify >= 2 )
				queue_leave_notify(queue_id, sd, flag);

			aFree(head);
			return true;
		}

		previous = head;
		head = head->next;
	}
	return false;
}

int queue_clean(struct queue_data *queue, int delay, int flag)
{
	struct queue_players *head, *next;
	struct map_session_data *sd;

	nullpo_ret(queue);
	
	if( queue == NULL )
		return false;

	head = queue->first;
	while( head != NULL )
	{
		if( (sd = head->sd) != NULL )
		{
			if( flag && battle_config.queue_notify >= 2 )
				queue_leave_notify(queue->queue_id, sd, flag);

			if( delay >= 0 )
				sd->status.queue_delay = (int)time(NULL)+delay;

			sd->queue_id = 0;
			ShowInfo("Player '%s' removed from queue (%d: %s).\n", sd->status.name, queue->queue_id, queue->name);
		}
		next = head->next;
		aFree(head);
		head = next;
	}

	queue->first = queue->last = NULL;
	queue->count = 0;
	return true;
}

int queue_atcommand_list(struct map_session_data *sd)
{
	DBIterator* iter = db_iterator(queue_db);
	struct queue_data* queue = NULL;
	char output[128];
	int count = 0;

	nullpo_ret(sd);

	for( queue = (struct queue_data*)dbi_first(iter); dbi_exists(iter); queue = (struct queue_data*)dbi_next(iter) )
	{
		sprintf(output, "    %d - %s", queue->queue_id, queue->name);
		clif_displaymessage(sd->fd, output);
		count++;
	}
	dbi_destroy(iter);
	return count;
}

void queue_join_notify(int queue_id, struct map_session_data *sd)
{
	struct queue_data *queue = NULL;
	struct queue_players *next;
	char output[256];

	nullpo_retv(sd);

	queue = queue_search(queue_id);
	if( queue == NULL )
		return;

	next = queue->first;
	while( next != NULL )
	{
		if( next->sd && next->sd != sd )
		{
			sprintf(output, msg_txt(next->sd,(QUEUE_MSG_TXT+3)), sd->status.name, queue->name);
			clif_messagecolor(&next->sd->bl, QUEUE_COLOR, output, true, SELF);
		}
		next = next->next;
	}

	sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+4)), queue->name);
	clif_messagecolor(&sd->bl, QUEUE_COLOR, output, true, SELF);
}

void queue_leave_notify(int queue_id, struct map_session_data *sd, int flag)
{
	struct queue_data *queue = NULL;
	struct queue_players *next;
	char output[256];

	nullpo_retv(sd);

	if( flag <= 0 || flag >= 9 )
		return;

	queue = queue_search(queue_id);
	if( queue == NULL )
		return;

	next = queue->first;
	while( next != NULL )
	{
		if( next->sd && next->sd != sd )
		{
			switch(flag)
			{
				case 1:
					sprintf(output, msg_txt(next->sd,(QUEUE_MSG_TXT+5)), sd->status.name, queue->name);
					clif_messagecolor(&next->sd->bl, QUEUE_COLOR, output, true, SELF);
					break;
				case 2:
					sprintf(output, msg_txt(next->sd,(QUEUE_MSG_TXT+6)), sd->status.name, queue->name);
					clif_messagecolor(&next->sd->bl, QUEUE_COLOR, output, true, SELF);
					break;
				case 3:
					sprintf(output, msg_txt(next->sd,(QUEUE_MSG_TXT+7)), sd->status.name, queue->name);
					clif_messagecolor(&next->sd->bl, QUEUE_COLOR, output, true, SELF);
					break;
				case 4:
					sprintf(output, msg_txt(next->sd,(QUEUE_MSG_TXT+8)), sd->status.name, queue->name);
					clif_messagecolor(&next->sd->bl, QUEUE_COLOR, output, true, SELF);
					break;
				default:
					break;
			}
		}
		next = next->next;
	}

	if( sd != NULL )
	{
		switch(flag)
		{
			case 1:
				sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+9)), queue->name);
				clif_messagecolor(&sd->bl, QUEUE_COLOR, output, true, SELF);
				break;
			case 2:
				sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+10)), queue->name);
				clif_messagecolor(&sd->bl, QUEUE_COLOR, output, true, SELF);
				break;
			case 3:
				sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+11)), queue->name);
				clif_messagecolor(&sd->bl, QUEUE_COLOR, output, true, SELF);
				break;
			case 5:
				sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+12)), queue->name);
				clif_messagecolor(&sd->bl, QUEUE_COLOR, output, true, SELF);
				break;
			case 6:
				sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+13)), queue->name, queue_delay(sd->status.queue_delay));
				clif_messagecolor(&sd->bl, QUEUE_COLOR, output, true, SELF);
				break;
			case 7:
				sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+14)), queue->name);
				clif_messagecolor(&sd->bl, QUEUE_COLOR, output, true, SELF);
				break;
			case 8:
				sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+15)), queue->name);
				clif_messagecolor(&sd->bl, QUEUE_COLOR, output, true, SELF);
				break;
			default:
				return; // Nenhuma mensagem para o jogador.
		}
	}
}

const char* queue_delay(int delay) {
	int timer = delay-(int)time(NULL);
	int sec = 0, min = 0, hour = 0, days = 0;
	static char output[128];

	if( timer < 60 ) {
		sprintf(output, msg_txt(NULL,(QUEUE_MSG_TXT+16)), timer);
		return ((const char*)output);
	}
	
	min = timer/60;
	sec = timer - (min*60);

	if( min < 60 ) {
		sprintf(output, msg_txt(NULL,(QUEUE_MSG_TXT+17)), min, sec);
		return ((const char*)output);
	}

	hour = min/60;
	min = min - (hour*60);

	if( hour < 24 ) {
		sprintf(output, msg_txt(NULL,(QUEUE_MSG_TXT+18)), hour, min, sec);
		return ((const char*)output);
	}

	days = hour/24;
	hour = hour - (days*24);

	sprintf(output, msg_txt(NULL,(QUEUE_MSG_TXT+19)), days, hour, min, sec);
	return ((const char*)output);
}

void do_init_queue(void)
{
	queue_db = idb_alloc(DB_OPT_RELEASE_DATA);
	ShowMessage(CL_WHITE"[Queue System]:" CL_RESET " System Queue successfully initialized.\n");
	ShowMessage(CL_WHITE"[Queue System]:" CL_RESET " by (c) CreativeSD, suport in " CL_GREEN "www.creativesd.com.br" CL_RESET "\n");
}

static int queue_db_final(DBKey key, DBData *data, va_list ap)
{
	struct queue_data *queue = (struct queue_data *)data;
	if( queue )
		queue_clean(queue,0,7);
	return 0;
}

void do_final_queue(void)
{
	queue_db->destroy(queue_db, queue_db_final);
}

// Script Command
int buildin_queue_create(struct script_state* st)
{
	struct queue_data *queue = NULL;
	const char *event = "", *name = "";
	int queue_id = script_getnum(st,2), min_level = 0, max_level = 0;

	if( queue_id <= 0 )
	{
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	name = script_getstr(st,3);
	if( script_hasdata(st,4) ) min_level = script_getnum(st,4);
	if( script_hasdata(st,5) ) max_level = script_getnum(st,5);
	if( script_hasdata(st,6) )
	{
		event = script_getstr(st,6);
		check_event(st, event);
	}

	if( (queue = queue_search(queue_id)) != NULL )
	{
		safestrncpy(queue->name, name, sizeof(queue->name));
		safestrncpy(queue->join_event, event, sizeof(queue->join_event));
		queue->min_level = min_level;
		queue->max_level = max_level;
		script_pushint(st,1);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st,queue_create(queue_id, name, min_level, max_level, event));
	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_delete(struct script_state* st)
{
	struct queue_data *queue = NULL;
	int queue_id = script_getnum(st, 2);

	if ((queue = queue_search(queue_id)) == NULL)
	{
		script_pushint(st, 0);
		ShowError("script:queue_delete: queue (%d) does not exist\n", queue_id);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, queue_delete(queue_id));
	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_check(struct script_state* st)
{
	struct queue_data *queue = queue_search(script_getnum(st,2));
	script_pushint(st,queue==NULL?0:1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_size(struct script_state* st)
{
	struct queue_data *queue = queue_search(script_getnum(st, 2));
	script_pushint(st, (queue == NULL ? 0 : queue->count));
	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_join(struct script_state* st)
{
	TBL_PC* sd;
	struct queue_data *queue = NULL;
	int queue_id = script_getnum(st,2);
	int flag = 1, check_delay = 1, delay = 0;

	if( (queue = queue_search(queue_id)) == NULL )
	{
		script_pushint(st,0);
		ShowError("script:queue_join: queue (%d) does not exist\n", queue_id);
		return SCRIPT_CMD_SUCCESS;
	}

	if( script_hasdata(st,3) )
		flag = script_getnum(st,3);

	// Delay
	if( script_hasdata(st,4) )
		check_delay = script_getnum(st,4);

	if (script_hasdata(st,5))
		sd = map_id2sd(script_getnum(st,5));
	else
		script_rid2sd(sd);

	if (!sd)
	{
		script_pushint(st,1);
		return SCRIPT_CMD_SUCCESS;
	}
	
	if (sd->queue_id && !queue_leave(sd,2) ) {
		script_pushint(st,-1);
		return SCRIPT_CMD_SUCCESS;
	}
	
	if( battle_config.queue_only_towns && !queue_check_town(sd->bl.m) ) {
		script_pushint(st,-2);
		return SCRIPT_CMD_SUCCESS;
	}

	if( check_delay )
	{
		delay = sd->status.queue_delay-(int)time(NULL);
		if( delay > battle_config.queue_join_delay ) {
			sd->status.queue_delay = (int)time(NULL)+battle_config.queue_join_delay;
			delay = sd->status.queue_delay-(int)time(NULL);
		}

		if( delay > 0 ) {
			script_pushint(st,-3);
			return SCRIPT_CMD_SUCCESS;
		}
	}
	
	script_pushint(st, queue_join(sd,queue_id,flag));
	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_leave(struct script_state* st)
{
	TBL_PC* sd = NULL;
	struct queue_data *queue = NULL;
	int queue_id = 0, flag = 2;

	if( script_hasdata(st,2) )
		flag = script_getnum(st,2);

	if( script_hasdata(st,3) )
		sd = map_id2sd(script_getnum(st,3));
	else
		script_rid2sd(sd);
	
	if( !sd || !sd->queue_id )
	{
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}
	
	queue_id = sd->queue_id;
	
	if( (queue = queue_search(queue_id)) == NULL )
	{
		if( sd->queue_id == queue_id ) sd->queue_id = 0;
		script_pushint(st,1);
		ShowError("script:queue_leave: queue (%d) does not exist\n", queue_id);
		return SCRIPT_CMD_SUCCESS;
	}
	
	script_pushint(st, queue_leave(sd,flag));
	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_clean(struct script_state* st)
{
	struct queue_data *queue = queue_search(script_getnum(st,2));
	int flag = 0, delay = -1;

	if( script_hasdata(st,3) )
		flag = script_getnum(st,3);
	
	if( script_hasdata(st,4) )
		delay = script_getnum(st,4);

	if( queue == NULL ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, queue_clean(queue,delay,flag));
	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_leave_notify(struct script_state* st)
{
	TBL_PC* sd = NULL;
	struct queue_data *queue;
	int queue_id, flag = 0;

	queue_id = script_getnum(st,2);
	if( script_hasdata(st,3) )
		flag = script_getnum(st,3);

	if( script_hasdata(st,4) )
		sd = map_id2sd(script_getnum(st,4));
	
	if( (queue = queue_search(queue_id)) == NULL )
	{
		script_pushint(st,1);
		ShowError("script:queue_leave_notify: queue (%d) does not exist\n", queue_id);
		return SCRIPT_CMD_SUCCESS;
	}
	
	queue_leave_notify(queue_id,sd,flag);
	script_pushint(st,1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_get_players(struct script_state* st)
{
	struct queue_data *queue = queue_search(script_getnum(st,2));
	struct queue_players *head;
	struct map_session_data *sd;
	int i = 0;

	if ( queue == NULL)
	{
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	head = queue->first;
	while( head )
	{
		if( (sd = head->sd) != NULL )
		{
			set_reg(st, NULL, reference_uid(add_str(".@queue_cid"), i), ".@queue_cid", (void*)__64BPRTSIZE(sd->status.char_id), NULL);
			set_reg(st, NULL, reference_uid(add_str(".@queue_aid"), i), ".@queue_aid", (void*)__64BPRTSIZE(sd->status.account_id), NULL);
			set_reg(st, NULL, reference_uid(add_str(".@queue_player$"), i), ".@queue_player$", (void*)__64BPRTSIZE(sd->status.name), NULL);
			i++;
		}
		head = head->next;
	}

	set_reg(st, NULL, add_str(".@queue_count"), ".@queue_count", (void*)__64BPRTSIZE(i), NULL);
	script_pushint(st, i);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_get_data(struct script_state* st)
{
	struct queue_data *queue = NULL;
	int queue_id = script_getnum(st,2), type = script_getnum(st,3);

	if( (queue = queue_search(queue_id)) == NULL )
	{
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	switch( type )
	{
		case 0: script_pushstrcopy(st, queue->name); break;
		case 1: script_pushstrcopy(st, queue->join_event); break;
		case 2: script_pushint(st, queue->min_level); break;
		case 3: script_pushint(st, queue->max_level); break;
		case 4: script_pushint(st, queue->count); break;
		default:
			ShowError("script:queue_get_data: invalid parameter (%d).\n", type);
			script_pushint(st,0);
			break;
	}

	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_set_data(struct script_state* st)
{
	struct queue_data *queue = NULL;
	int queue_id = script_getnum(st,2), type = script_getnum(st,3);
	
	if( (queue = queue_search(queue_id)) == NULL )
	{
		script_pushint(st,0);
		ShowError("script:queue_change_event: queue (%d) does not exist\n", queue_id);
		return SCRIPT_CMD_SUCCESS;
	}

	switch(type)
	{
		case 0:
			{
				const char* change = script_getstr(st,4);
				safestrncpy(queue->name, change, sizeof(queue->name));
			}
			break;
		case 1:
			{
				const char* change = script_getstr(st,4);
				check_event(st, change);
				safestrncpy(queue->join_event, change, sizeof(queue->name));
			}
			break;
		case 2:
			{
				int value = script_getnum(st,4);
				queue->min_level = value;
			}
			break;
		case 3:
			{
				int value = script_getnum(st,4);
				queue->max_level = value;
			}
			break;
		default:
			script_pushint(st,0);
			ShowError("script:queue_change: invalid type (%d).\n", type);
			return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st,1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_char_info(struct script_state* st)
{
	TBL_PC* sd = NULL;
	struct queue_data *queue = NULL;
	int type = script_getnum(st,2), delay = 0;

	if (script_hasdata(st,3))
		sd = map_id2sd(script_getnum(st,3));
	else
		script_rid2sd(sd);

	if( sd == NULL ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}
	
	if( sd->queue_id && (queue = queue_search(sd->queue_id)) == NULL )
		sd->queue_id = 0;

	// Update Delay.
	delay = sd->status.queue_delay-(int)time(NULL);
	if( delay > battle_config.queue_join_delay )
		sd->status.queue_delay = (int)time(NULL)+battle_config.queue_join_delay;

	switch( type )
	{
		case 0: script_pushint(st, sd->queue_id); break;
		case 1: script_pushint(st, sd->status.queue_delay); break;
		case 2:
			delay = sd->status.queue_delay-(int)time(NULL);
			script_pushint(st, (delay>0?delay:0));
			break;
		case 3: script_pushstrcopy(st, queue_delay(sd->status.queue_delay)); break;
		case 4: script_pushint(st, sd->status.base_level); break;
		case 5: script_pushint(st, sd->queue_nomove); break;
		default:
			ShowError("script:queue_getcharinfo: invalid parameter (%d).\n", type);
			script_pushint(st,0);
			break;
	}

	return SCRIPT_CMD_SUCCESS;
}

int buildin_queue_set_delay(struct script_state* st)
{
	TBL_PC* sd = NULL;
	int delay = script_getnum(st,2);

	if (script_hasdata(st,3))
		sd = map_id2sd(script_getnum(st,3));
	else
		script_rid2sd(sd);

	if( sd == NULL ) {
		script_pushint(st,-1);
		return SCRIPT_CMD_SUCCESS;
	}

	sd->status.queue_delay = (int)time(NULL)+delay;

	// Update Delay.
	delay = sd->status.queue_delay-(int)time(NULL);
	if( delay > battle_config.queue_join_delay )
		sd->status.queue_delay = (int)time(NULL)+battle_config.queue_join_delay;

	script_pushint(st,sd->status.queue_delay);
	return SCRIPT_CMD_SUCCESS;
}

// Atcommand
int atcommand_queue(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	int count = 0;
	char output[128];
	nullpo_retr(-1, sd);

	if( sd == NULL )
		return -1;

	clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+20)));
	count = queue_atcommand_list(sd);
	
	if( !count )
		clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+21)));
	else {
		sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+22)), count);
		clif_displaymessage(fd, output);
	}
	return 0;
}

int atcommand_queuelist(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct queue_data *queue = NULL;
	struct queue_players *next;
	int queue_id;
	char output[128];

	nullpo_retr(-1, sd);

	if( sd == NULL )
		return -1;

	if (!message || !*message || sscanf(message, "%d", &queue_id) < 1 || queue_id == 0 || (queue = queue_search(queue_id)) == NULL) {
		clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+23)));
		queue_atcommand_list(sd);
		return -1;
	}

	if( queue->count <= 0 )
	{
		sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+24)), queue->name);
		clif_displaymessage(fd,output);
		return 0;
	}

	next = queue->first;
	sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+25)), queue->name);
	clif_displaymessage(fd,output);
	while( next != NULL )
	{
		if( next->sd ) {
			sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+26)), next->sd->status.char_id, next->sd->status.name, job_name(next->sd->status.class_), next->sd->status.base_level, next->sd->status.job_level);
			clif_displaymessage(fd,output);
		}
		next = next->next;
	}
	return 0;
}

int atcommand_queuekick(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct queue_data *queue = NULL;
	struct map_session_data *pl_sd = NULL;
	char output[128];

	nullpo_retr(-1, sd);

	memset(atcmd_player_name, '\0', sizeof(atcmd_player_name));

	if (!message || !*message || sscanf(message, "%23[^\n]", atcmd_player_name) < 1) {
		clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+27)));
		return -1;
	}

	if((pl_sd=map_nick2sd(atcmd_player_name,true)) == NULL && (pl_sd=map_charid2sd(atoi(atcmd_player_name))) == NULL)
	{
		clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+28)));
		return -1;
	}

	if( !pl_sd->queue_id || (queue = queue_search(pl_sd->queue_id)) == NULL )
	{
		clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+29)));
		return -1;
	}

	if( queue_leave(pl_sd,3) ) {
		sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+30)), pl_sd->status.name, queue->name);
		clif_displaymessage(fd, output);
		return 0;
	}
	
	sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+31)), pl_sd->status.name, queue->name);
	clif_displaymessage(fd, output);
	return -1;
}

int atcommand_queuesetdelay(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	int value = 0, delay = 0;
	char output[128];

	nullpo_retr(-1, sd);

	if( sd == NULL )
		return -1;

	if (!message || !*message || sscanf(message, "%d", &value) < 1) {
		clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+32)));
		return -1;
	}

	if( value > battle_config.queue_join_delay )
		value = battle_config.queue_join_delay;

	sd->status.queue_delay = (int)time(NULL)+value;
	delay = sd->status.queue_delay-(int)time(NULL);
	clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+33)));
	if( delay > 0 ) {
		sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+34)), queue_delay(sd->status.queue_delay));
		clif_displaymessage(fd, output);
	}
	return 0;
}

int atcommand_queuedelay(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	int delay = 0;
	char output[128];

	nullpo_retr(-1, sd);

	if( sd == NULL )
		return -1;

	// Update Delay.
	delay = sd->status.queue_delay-(int)time(NULL);
	if( delay > battle_config.queue_join_delay ) {
		sd->status.queue_delay = (int)time(NULL)+battle_config.queue_join_delay;
		delay = sd->status.queue_delay-(int)time(NULL);
	}

	if( delay <= 0 )
		clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+35)));
	else {
		sprintf(output, msg_txt(sd,(QUEUE_MSG_TXT+34)), queue_delay(sd->status.queue_delay));
		clif_displaymessage(fd, output);
	}
	return 0;
}

int atcommand_queuenomove(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	nullpo_retr(-1, sd);

	if( sd == NULL )
		return -1;

	sd->queue_nomove = sd->queue_nomove ? 0 : 1;
	if( sd->queue_nomove )
		clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+36)));
	else
		clif_displaymessage(fd, msg_txt(sd,(QUEUE_MSG_TXT+37)));
	return 0;
}
#endif

#ifdef RESTOCK_SYSTEM_ENABLE
// Restock System
// Map Functions
int pc_restock_item(struct map_session_data *sd, int nameid, int reserved_id)
{
	int i, amount = 0;

	if( nameid < 0 )
		return 0;

	ARR_FIND(0, MAX_RESTOCK, i, sd->status.restock[i].nameid == nameid);
	if( i >= MAX_RESTOCK )
		return 0;

	amount = sd->status.restock[i].amount;

	if( reserved_id )
		ARR_FIND(0, MAX_STORAGE, i, sd->storage.u.items_storage[i].nameid == nameid && sd->storage.u.items_storage[i].card[0]==CARD0_CREATE && MakeDWord(sd->storage.u.items_storage[i].card[2], sd->storage.u.items_storage[i].card[3])==reserved_id);
	else
		ARR_FIND(0, MAX_STORAGE, i, sd->storage.u.items_storage[i].nameid == nameid);

	if( i >= MAX_STORAGE )
		return 0;

	if( sd->storage.u.items_storage[i].amount < amount )
		amount = sd->storage.u.items_storage[i].amount;

	if( amount )
		storage_storageget(sd, &sd->storage, i, amount);

	return amount;
}

// Script Command
int buildin_addrestock(struct script_state* st)
{
	TBL_PC *sd;
	struct script_data *data;
	struct item_data* id = NULL;
	int i = 0, amount = 0;
	unsigned short nameid;

	if (!script_rid2sd(sd))
	{
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	data = script_getdata(st,2);
	get_val(st,data);

	if (data_isstring(data))
	{
		const char *name = conv_str(st, data);
		id = itemdb_searchname(name);
		if (id == NULL){
			ShowError("buildin_addrestock: Invalid item '%s'.\n", name);
			return SCRIPT_CMD_FAILURE;
		}
		nameid = id->nameid;
	}
	else if (data_isint(data))
	{
		nameid = conv_num(st,data);
		if (!(id = itemdb_exists(nameid))) {
			ShowError("buildin_addrestock: Invalid item '%d'.\n", id);
			return SCRIPT_CMD_FAILURE; //No item created.
		}
	}
	else {
		ShowError("buildin_addrestock: invalid data type for argument #1 (%d).", data->type);
		return SCRIPT_CMD_FAILURE;
	}

	amount = script_getnum(st,3);
	ARR_FIND(0, MAX_RESTOCK, i, sd->status.restock[i].nameid == nameid);

	if( i >= MAX_RESTOCK )
		ARR_FIND(0, MAX_RESTOCK, i, sd->status.restock[i].nameid == 0);

	if( i < MAX_RESTOCK ) {
		sd->status.restock[i].nameid = nameid;
		sd->status.restock[i].amount = amount;
		script_pushint(st, 1);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, 0);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_delrestock(struct script_state* st)
{
	TBL_PC *sd;
	struct script_data *data;
	struct item_data* id = NULL;
	int i = 0;
	unsigned short nameid;

	if (!script_rid2sd(sd))
	{
		script_pushint(st,0);
		return true;
	}

	data = script_getdata(st,2);
	get_val(st, data);
	if (data_isstring(data))
	{
		const char *name = conv_str(st, data);
		id = itemdb_searchname(name);
		if (id == NULL){
			ShowError("buildin_delrestock: Invalid item '%s'.\n", name);
			return SCRIPT_CMD_FAILURE;
		}
		nameid = id->nameid;
	}
	else if (data_isint(data))
	{
		nameid = conv_num(st, data);
		if (!(id = itemdb_exists(nameid))) {
			ShowError("buildin_delrestock: Invalid item '%d'.\n", id);
			return SCRIPT_CMD_FAILURE; //No item created.
		}
	}
	else {
		ShowError("buildin_delrestock: invalid data type for argument #1 (%d).", data->type);
		return SCRIPT_CMD_FAILURE;
	}

	ARR_FIND(0, MAX_RESTOCK, i, sd->status.restock[i].nameid == nameid);

	if( i < MAX_RESTOCK ) {
		sd->status.restock[i].nameid = 0;
		sd->status.restock[i].amount = 0;
		script_pushint(st,1);
		return SCRIPT_CMD_SUCCESS;
	}
	script_pushint(st,0);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_getrestocklist(struct script_state* st)
{
	TBL_PC *sd;
	int i = 0, j = 0;

	if (!script_rid2sd(sd))
	{
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	for( i=0; i < MAX_RESTOCK; i++ )
	{
		if( sd->status.restock[i].nameid && sd->status.restock[i].amount ) {
			pc_setreg(sd,reference_uid(add_str("@restocklist_id"), j),sd->status.restock[i].nameid);
			pc_setreg(sd,reference_uid(add_str("@restocklist_amount"), j),sd->status.restock[i].amount);
			j++;
		}
	}
	pc_setreg(sd,add_str("@restocklist_count"),j);
	script_pushint(st,j);
	return SCRIPT_CMD_SUCCESS;
}

// Atcommand
int atcommand_restock(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("Restock#main");
	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd,1387));
		return -1;
	}
	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}
#endif

#ifdef STUFF_ITEMS_ENABLE
// Stuff System

// Script Command
int buildin_getstuffitem(struct script_state* st)
{
	int get_count, i, reserved_id, type;
	unsigned short nameid, amount;
	struct item it;
	TBL_PC *sd;
	struct script_data *data;
	unsigned char flag = 0;
	struct item_data *id;

	if (!script_rid2sd(sd))
	{	// No player attached.
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	amount = script_getnum(st,3);
	type = script_getnum(st,4);
	switch(type)
	{
		case 1:
			reserved_id = battle_config.stuff_gvg_reserved_id;
			break;
		case 2:
			reserved_id = battle_config.stuff_pvp_reserved_id;
			break;
		default:
			reserved_id = battle_config.stuff_bg_reserved_id;
			break;
	}

	if( reserved_id <= 0 ) {
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	data = script_getdata(st, 2);
	get_val(st, data);
	if (data_isstring(data)) {
		const char *name = conv_str(st, data);
		id = itemdb_searchname(name);
		if (id == NULL)
		{	//Failed
			script_pushint(st, 0);
			return SCRIPT_CMD_SUCCESS;
		}
		nameid = id->nameid;
	} else if( data_isint(data) ) {// <item id>
		nameid = conv_num(st,data);
		if( !(id = itemdb_exists(nameid)) ){
			ShowError("buildin_getstuffitem: Nonexistant item %d requested.\n", nameid);
			return SCRIPT_CMD_FAILURE; //No item created.
		}
	} else {
		ShowError("buildin_getstuffitem: invalid data type for argument #1 (%d).", data->type);
		return SCRIPT_CMD_FAILURE;
	}

	if (!itemdb_exists(nameid))
	{	// Item does not exist.
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	memset(&it, 0, sizeof(it));
	it.nameid = nameid;
	it.identify = 1;
	it.bound = BOUND_NONE;
	it.card[0] = CARD0_CREATE;
	it.card[2] = GetWord(reserved_id, 0);
	it.card[3] = GetWord(reserved_id, 1);
	//Check if it's stackable.
	if (!itemdb_isstackable2(id))
		get_count = 1;
	else
		get_count = amount;

	for (i = 0; i < amount; i += get_count)
	{
		if (pc_additem(sd, &it, get_count, LOG_TYPE_SCRIPT))
			clif_additem(sd, 0, 0, flag);
	}

	script_pushint(st, flag);
	return SCRIPT_CMD_SUCCESS;
}

// Atcommand
int atcommand_stuffitem(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	char item_name[100];
	int number = 0, type = 0, reserved_id = 0;
	char flag = 0;
	struct item item_tmp;
	struct item_data *item_data[10];
	int get_count, i, j=0;
	char *itemlist;

	nullpo_retr(-1, sd);
	memset(item_name, '\0', sizeof(item_name));

	if (!message || !*message || (
		sscanf(message, "\"%99[^\"]\" %d %11d", item_name, &type, &number) < 2 &&
		sscanf(message, "%99s %d %11d", item_name, &type, &number) < 2
	)) {
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+3))); // Please enter an item name or ID (usage: @stuffitem <item name/ID> <amount> <quantity>).
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+4)));
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+5)));
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+6)));
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+7)));
		return -1;
	}

	type--;
	if( type < 0 || type > 2 ) {
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+8)));
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+3))); // Please enter an item name or ID (usage: @stuffitem <item name/ID> <amount> <quantity>).
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+4)));
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+5)));
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+6)));
		clif_displaymessage(fd, msg_txt(sd,(STUFF_ITEMS_MSG_TXT+7)));
		return -1;
	}

	switch(type) {
		case 1:
			reserved_id = battle_config.stuff_pvp_reserved_id;
			break;
		case 2:
			reserved_id = battle_config.stuff_gvg_reserved_id;
			break;
		default:
			reserved_id = battle_config.stuff_bg_reserved_id;
	}

	if( reserved_id <= 0 ) {
		char output[128];
		sprintf(output, msg_txt(sd,STUFF_ITEMS_MSG_TXT+9), msg_txt(sd,STUFF_ITEMS_MSG_TXT+type));
		clif_displaymessage(fd, output);
		return 0;
	}

	itemlist = strtok(item_name, ":");
	while (itemlist != NULL && j<10) {
		if ((item_data[j] = itemdb_searchname(itemlist)) == NULL &&
		    (item_data[j] = itemdb_exists(atoi(itemlist))) == NULL){
			clif_displaymessage(fd, msg_txt(sd,19)); // Invalid item ID or name.
			return -1;
		}
		itemlist = strtok(NULL, ":"); //next itemline
		j++;
	}

	if (number <= 0)
		number = 1;
	get_count = number;

	for(j--; j>=0; j--){ //produce items in list
		unsigned short item_id = item_data[j]->nameid;

		//Check if it's stackable.
		if (!itemdb_isstackable2(item_data[j]))
			get_count = 1;

		for (i = 0; i < number; i += get_count) {
			// if not pet egg
			if (!pet_create_egg(sd, item_id)) {
				memset(&item_tmp, 0, sizeof(item_tmp));
				item_tmp.nameid = item_id;
				item_tmp.identify = 1;
				item_tmp.card[0] = CARD0_CREATE;
				item_tmp.card[2] = GetWord(reserved_id, 0);
				item_tmp.card[3] = GetWord(reserved_id, 1);
				item_tmp.bound = BOUND_NONE;
				if ((flag = pc_additem(sd, &item_tmp, get_count, LOG_TYPE_COMMAND)))
					clif_additem(sd, 0, 0, flag);
			}
		}
	}

	if (flag == 0)
		clif_displaymessage(fd, msg_txt(sd,18)); // Item created.
	return 0;
}
#endif

#ifdef SHOP_EXTENDED_ENABLE
// Shop Extended

// Map Functions
int npc_cashshop_buylist_sub(struct map_session_data* sd, unsigned short itemreq, const char *reg, int discount, int n, unsigned short* item_list, int point, struct npc_data* nd) {
	char npc_ev[EVENT_NAME_LENGTH];
	int i, nameid, amount, key_nameid = 0, key_amount = 0;

	// discard old contents
	script_cleararray_pc(sd, "@bought_nameid", (void*)0);
	script_cleararray_pc(sd, "@bought_quantity", (void*)0);

	// save list of bought items
	for( i = 0; i < n; i++ ) {
		nameid = item_list[i*2+1];
		amount = item_list[i*2+0];
		script_setarray_pc(sd, "@bought_nameid", i, (void*)(intptr_t)nameid, &key_nameid);
		script_setarray_pc(sd, "@bought_quantity", i, (void*)(intptr_t)amount, &key_amount);
	}

	script_setarray_pc(sd, "@bought_itempay", 0, (void*)(intptr_t)itemreq, NULL);
	script_setarray_pc(sd, "@bought_reg$", 0, (void*)(intptr_t)reg, NULL);
	script_setarray_pc(sd, "@bought_discount", 0, (void*)(intptr_t)discount, NULL);
	script_setarray_pc(sd, "@bought_cost", 0, (void*)(intptr_t)point, NULL);

	// invoke event
	snprintf(npc_ev, ARRAYLENGTH(npc_ev), "%s::%s", nd->exname, script_config.onbuy_event_name);
	npc_event(sd, npc_ev, 0);

	return 0;
}
#endif

#ifdef COSTUME_SYSTEM_ENABLE
// Costume System

// Script Command
int buildin_costume(struct script_state* st)
{
	short i = -1;
	int num;
	unsigned int ep;
	TBL_PC *sd;

	num = script_getnum(st, 2); // Equip Slot

	if (!script_rid2sd(sd))
		return SCRIPT_CMD_FAILURE;

	if (equip_index_check(num))
		i = pc_checkequip(sd, equip_bitmask[num]);
	if (i < 0)
		return SCRIPT_CMD_FAILURE;

	ep = sd->inventory.u.items_inventory[i].equip;
	if (!(ep&EQP_HEAD_LOW) && !(ep&EQP_HEAD_MID) && !(ep&EQP_HEAD_TOP) && !(ep&EQP_GARMENT)) {
		ShowError("buildin_costume: Attempted to convert non-cosmetic item to costume.");
		return SCRIPT_CMD_FAILURE;
	}
	log_pick_pc(sd, LOG_TYPE_SCRIPT, -1, &sd->inventory.u.items_inventory[i]);
	pc_unequipitem(sd, i, 2);
	clif_delitem(sd, i, 1, 3);
	// --------------------------------------------------------------------
	sd->inventory.u.items_inventory[i].refine = 0;
	sd->inventory.u.items_inventory[i].attribute = 0;
	sd->inventory.u.items_inventory[i].card[0] = CARD0_CREATE;
	sd->inventory.u.items_inventory[i].card[1] = 0;
	sd->inventory.u.items_inventory[i].card[2] = GetWord(battle_config.costumeitem_reserved_id, 0);
	sd->inventory.u.items_inventory[i].card[3] = GetWord(battle_config.costumeitem_reserved_id, 1);

	if (ep&EQP_HEAD_TOP) { ep &= ~EQP_HEAD_TOP; ep |= EQP_COSTUME_HEAD_TOP; }
	if (ep&EQP_HEAD_LOW) { ep &= ~EQP_HEAD_LOW; ep |= EQP_COSTUME_HEAD_LOW; }
	if (ep&EQP_HEAD_MID) { ep &= ~EQP_HEAD_MID; ep |= EQP_COSTUME_HEAD_MID; }
	if (ep&EQP_GARMENT) { ep &= EQP_GARMENT; ep |= EQP_COSTUME_GARMENT; }
	// --------------------------------------------------------------------
	log_pick_pc(sd, LOG_TYPE_SCRIPT, 1, &sd->inventory.u.items_inventory[i]);

	clif_additem(sd, i, 1, 0);
	pc_equipitem(sd, i, ep);
	clif_misceffect(&sd->bl, 3);

	return SCRIPT_CMD_SUCCESS;
}

int buildin_getcostumeitem(struct script_state* st)
{
	int get_count, i;
	unsigned short nameid, amount;
	struct item it;
	TBL_PC *sd;
	struct script_data *data;
	unsigned char flag = 0;
	struct item_data *id;

	if (!script_rid2sd(sd))
	{	// No player attached.
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	if (battle_config.costumeitem_reserved_id <= 0) {
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	amount = script_getnum(st, 3);
	data = script_getdata(st, 2);
	get_val(st, data);
	if (data_isstring(data)) {
		int ep;
		const char *name = conv_str(st, data);
		id = itemdb_searchname(name);
		if (id == NULL)
		{	//Failed
			script_pushint(st, 0);
			return SCRIPT_CMD_SUCCESS;
		}
		ep = id->equip;
		if (!(ep&EQP_HEAD_LOW) && !(ep&EQP_HEAD_MID) && !(ep&EQP_HEAD_TOP) && !(ep&EQP_GARMENT)) {
			ShowError("buildin_getcostumeitem: Attempted to convert non-cosmetic item to costume.");
			return SCRIPT_CMD_FAILURE;
		}
		nameid = id->nameid;
	}
	else if (data_isint(data)) {// <item id>
		nameid = conv_num(st, data);
		if (!(id = itemdb_exists(nameid))) {
			ShowError("buildin_getcostumeitem: Nonexistant item %d requested.\n", nameid);
			return SCRIPT_CMD_FAILURE; //No item created.
		}
	}
	else {
		ShowError("buildin_getcostumeitem: invalid data type for argument #1 (%d).", data->type);
		return SCRIPT_CMD_FAILURE;
	}

	if (!itemdb_exists(nameid))
	{	// Item does not exist.
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	memset(&it, 0, sizeof(it));
	it.nameid = nameid;
	it.identify = 1;
	it.bound = BOUND_NONE;
	it.card[0] = CARD0_CREATE;
	it.card[2] = GetWord(battle_config.costumeitem_reserved_id, 0);
	it.card[3] = GetWord(battle_config.costumeitem_reserved_id, 1);

	//Check if it's stackable.
	if (!itemdb_isstackable2(id))
		get_count = 1;
	else
		get_count = amount;

	for (i = 0; i < amount; i += get_count)
	{
		if (pc_additem(sd, &it, get_count, LOG_TYPE_SCRIPT))
			clif_additem(sd, 0, 0, flag);
	}

	script_pushint(st, flag);
	return SCRIPT_CMD_SUCCESS;
}

// Atcommand
int atcommand_costumeitem(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	char item_name[100];
	int number = 0;
	char flag = 0;
	struct item item_tmp;
	struct item_data *item_data[10];
	int get_count, i, j = 0;
	char *itemlist;

	nullpo_retr(-1, sd);

	memset(item_name, '\0', sizeof(item_name));
	if (!message || !*message || (
		sscanf(message, "\"%99[^\"]\" %11d", item_name, &number) < 1 &&
		sscanf(message, "%99s %11d", item_name, &number) < 1
		)) {
		clif_displaymessage(fd, msg_txt(sd, 983)); // Please enter an item name or ID (usage: @costumeitem <item name/ID> <quantity>).
		return -1;
	}
	itemlist = strtok(item_name, ":");
	while (itemlist != NULL && j<10) {
		if ((item_data[j] = itemdb_searchname(itemlist)) == NULL &&
			(item_data[j] = itemdb_exists(atoi(itemlist))) == NULL) {
			clif_displaymessage(fd, msg_txt(sd, 19)); // Invalid item ID or name.
			return -1;
		}
		itemlist = strtok(NULL, ":"); //next itemline
		j++;
	}

	if (number <= 0)
		number = 1;
	get_count = number;

	for (j--; j >= 0; j--) { //produce items in list
		unsigned short item_id = item_data[j]->nameid;

		if (!battle_config.costumeitem_reserved_id)
		{
			clif_displaymessage(fd, msg_txt(sd, COSTUME_SYSTEM_MSG_TXT));
			return -1;
		}

		if (!(item_data[j]->equip&EQP_HEAD_LOW) &&
			!(item_data[j]->equip&EQP_HEAD_MID) &&
			!(item_data[j]->equip&EQP_HEAD_TOP) &&
			!(item_data[j]->equip&EQP_COSTUME_HEAD_LOW) &&
			!(item_data[j]->equip&EQP_COSTUME_HEAD_MID) &&
			!(item_data[j]->equip&EQP_COSTUME_HEAD_TOP) &&
			!(item_data[j]->equip&EQP_GARMENT) &&
			!(item_data[j]->equip&EQP_COSTUME_GARMENT))
		{
			clif_displaymessage(fd, msg_txt(sd, (COSTUME_SYSTEM_MSG_TXT + 1)));
			return -1;
		}

		//Check if it's stackable.
		if (!itemdb_isstackable2(item_data[j]))
			get_count = 1;

		for (i = 0; i < number; i += get_count) {
			// if not pet egg
			if (!pet_create_egg(sd, item_id)) {
				memset(&item_tmp, 0, sizeof(item_tmp));
				item_tmp.nameid = item_id;
				item_tmp.identify = 1;
				item_tmp.card[0] = CARD0_CREATE;
				item_tmp.card[2] = GetWord(battle_config.costumeitem_reserved_id, 0);
				item_tmp.card[3] = GetWord(battle_config.costumeitem_reserved_id, 1);
				item_tmp.bound = BOUND_NONE;
				if ((flag = pc_additem(sd, &item_tmp, get_count, LOG_TYPE_COMMAND)))
					clif_additem(sd, 0, 0, flag);
			}
		}
	}

	if (flag == 0)
		clif_displaymessage(fd, msg_txt(sd, 18)); // Item created.
	return 0;
}
#endif

#ifdef OSHOPS_ENABLE
// Organized Shops

// Map Functions
// Get a free block.
int organized_shops_get_block(int m)
{
	int i;

	for( i=0; i < MAX_OSHOPS_BLOCK; i++ ) {
		if( map[m].organized_shops[i].max == 0 )
			continue;

		if( map[m].organized_shops[i].count < map[m].organized_shops[i].max )
			return (i+1);
	}
	return 0;
}

// Grab a free block and return with the index.
void organized_shops_xy(int x1, int y1, int x2, int y2, int type, int *x, int *y)
{
	if( type == 0 || type == 2 )
		*x = x1+x2;
	else if( type == 1 || type == 3 )
		*x = x1-x2;
	else
		*x = 0;

	if( type == 0 || type == 1 )
		*y = y1+y2;
	else if( type == 2 || type == 3 )
		*y = y1-y2;
	else
		*y = 0;
}

int organized_shops_rellocate(int m) {
	struct map_session_data *sd;
	struct s_mapiterator* iter;
	int i, row, x, y, c = 0;

	if( !map_getmapflag(m,MF_ORGANIZED_SHOPS) )
		return -1;

	for( i=0; i < MAX_OSHOPS_BLOCK; i++ ) {
		map[m].organized_shops[i].count = 0;
		map[m].organized_shops[i].col = 0;
		map[m].organized_shops[i].row = 0;
	}
	
	i = organized_shops_get_block(m);
	if( i == 0 )
		return -2;
	i--;

	iter = mapit_getallusers();
	for( sd = (TBL_PC*)mapit_first(iter); mapit_exists(iter); sd = (TBL_PC*)mapit_next(iter) )
	{
		if( sd == NULL || sd->bl.m != m || !sd->organized_shops.flag || !sd->state.vending )
			continue;

		// Re-check Free block
		if( map[m].organized_shops[i].count >= map[m].organized_shops[i].max ) {
			i = organized_shops_get_block(m);
			if( i == 0 )
				continue;
			i--;
		}

		row = map[m].organized_shops[i].amount*map[m].organized_shops[i].cell;
		organized_shops_xy(map[m].organized_shops[i].x, map[m].organized_shops[i].y, map[m].organized_shops[i].row, map[m].organized_shops[i].col, map[m].organized_shops[i].dir, &x, &y);
		
		pc_disguise(sd,map[m].organized_shops[i].class_ ? map[m].organized_shops[i].class_ : 0);

		if( battle_config.organized_shops_ballon )
			sd->fakename[0] = '\0';
		else
			safestrncpy(sd->fakename, sd->message, sizeof(sd->fakename));

		clif_name_self(&sd->bl);
		clif_name_area(&sd->bl);

		if( sd->bl.x != x || sd->bl.y != y ) {
			sd->organized_shops.reallocate = 1;
			pc_setpos(sd,map[m].index,x,y,CLR_TELEPORT);
			clif_openvending(sd,sd->bl.id,sd->vending);
			sd->organized_shops.reallocate = 0; // Reset Rellocate Flag
		}

		if( !sd->organized_shops.flag || battle_config.organized_shops_ballon )
			clif_showvendingboard(&sd->bl,sd->message,0);

		map[m].organized_shops[i].count++;
		map[m].organized_shops[i].row += map[m].organized_shops[i].cell;
		if( map[m].organized_shops[i].row >= row ) {
			map[m].organized_shops[i].row = 0;
			map[m].organized_shops[i].col -= map[m].organized_shops[i].cell;
		}
		c++;
	}
	mapit_free(iter);
	return c;
}

// Script Command
int buildin_oshops_getinfo(struct script_state* st)
{
	const char *str;
	int type, m;
	int i = 0, n = 0, c = 0;

	str=script_getstr(st,2);
	if( (m=map_mapname2mapid(str)) < 0 || !map_getmapflag(m,MF_ORGANIZED_SHOPS) )
	{
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	type = script_getnum(st,3);
	switch(type)
	{
		case 0:
			for( i=0; i < MAX_OSHOPS_BLOCK; i++ )
			{
				if( map[m].organized_shops[i].max != 0 && map[m].organized_shops[i].x != 0 && map[m].organized_shops[i].y != 0 )
					c++;
			}
			script_pushint(st,c);
			break;
		case 1:
			for( i=0; i < MAX_OSHOPS_BLOCK; i++ )
			{
				if( map[m].organized_shops[i].max != 0 && map[m].organized_shops[i].x != 0 && map[m].organized_shops[i].y != 0 )
					c += map[m].organized_shops[i].max;
			}
			script_pushint(st,c);
			break;
		case 2:
			for( i=0; i < MAX_OSHOPS_BLOCK; i++ )
			{
				if( map[m].organized_shops[i].max != 0 && map[m].organized_shops[i].x != 0 && map[m].organized_shops[i].y != 0 )
					c += map[m].organized_shops[i].count;
			}
			script_pushint(st,c);
			break;
		case 3:
			if( script_hasdata(st,4) )
				n = script_getnum(st,4);

			n = n < MAX_OSHOPS_BLOCK ? map[m].organized_shops[n].max : 0;
			script_pushint(st,n);
			break;
		case 4:
			if( script_hasdata(st,4) )
				n = script_getnum(st,4);

			n = n < MAX_OSHOPS_BLOCK ? map[m].organized_shops[n].count : 0;
			script_pushint(st,n);
			break;
		default:
			script_pushint(st,0);
			break;
	}
	return SCRIPT_CMD_SUCCESS;
}

int buildin_oshops_set(struct script_state* st)
{
	const char *str;
	int m, n, class_, x, y, dir, amount, cell, max;

	str=script_getstr(st,2);
	n = script_getnum(st,3);
	class_ = script_getnum(st,4);
	x = script_getnum(st,5);
	y = script_getnum(st,6);
	dir = script_getnum(st,7);
	amount = script_getnum(st,8);
	cell = script_getnum(st,9);
	max = script_getnum(st,10);
	m = map_mapname2mapid(str);

	if( (m = map_mapname2mapid(str)) < 0 ) {
		ShowError("script:oshops_set: Map not found!\n");
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	if( n >= MAX_OSHOPS_BLOCK )
		ShowError("script:oshops_set: You have exceeded the limit of organized shops blocks!\n");
	if( class_ > 0 && !npcdb_checkid(class_) )
		ShowError("script:oshops_set: You must use a valid npc viewid in organized shops mapflag!");
	else if( x <= 0 || y <= 0 )
		ShowError("script:oshops_set: You can not use random coordinates in organized shops mapflag!");
	else if( dir < 0 || dir > 3 )
		ShowError("script:oshops_set: You must use a valid direction in organized shops mapflag!");
	else if( amount <= 0 )
		ShowError("script:oshops_set: You must set a quantity for column in organized shops mapflags!");
	else if( cell <= 0 )
		ShowError("script:oshops_set: You must set a cell for column in organized shops mapflags!");
	else if( max <= 0 )
		ShowError("script:oshops_set: You should set the sales limit on the organized shops mapflag!");
	else  {
		map_setmapflag_sub(m, MF_ORGANIZED_SHOPS, true, NULL);
		map[m].organized_shops[n].class_ = class_;
		map[m].organized_shops[n].x = x;
		map[m].organized_shops[n].y = y;
		map[m].organized_shops[n].dir = dir;
		map[m].organized_shops[n].amount = amount;
		map[m].organized_shops[n].cell = cell;
		map[m].organized_shops[n].max = max;
		script_pushint(st,1);
		return SCRIPT_CMD_SUCCESS;
	}
	script_pushint(st,0);
	return SCRIPT_CMD_FAILURE;
}

int buildin_oshops_remove(struct script_state* st)
{
	const char *str;
	int i, m, n, flag = 0, len = 0;

	str=script_getstr(st,2);
	n = script_getnum(st,3);

	if( (m = map_mapname2mapid(str)) < 0 ) {
		ShowError("script:oshops_remove: Map not found!\n");
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	if( n >= MAX_OSHOPS_BLOCK )
		ShowError("script:oshops_remove: You have exceeded the limit of organized shops blocks!\n");
	else  {
		map[m].organized_shops[n].class_ = 0;
		map[m].organized_shops[n].x = 0;
		map[m].organized_shops[n].y = 0;
		map[m].organized_shops[n].dir = 0;
		map[m].organized_shops[n].amount = 0;
		map[m].organized_shops[n].max = 0;
		
		for( i=0; i < len; i++ ) {
			if( map[m].organized_shops[i].max != 0 )
				flag = 1;
		}

		map_setmapflag_sub(m, MF_ORGANIZED_SHOPS, flag ? true : false, NULL);
		script_pushint(st,1);
		return SCRIPT_CMD_SUCCESS;
	}
	script_pushint(st,0);
	return SCRIPT_CMD_FAILURE;
}

int buildin_oshops_remove_all(struct script_state* st)
{
	const char *str;
	int i, m;

	str=script_getstr(st,2);
	if( (m=map_mapname2mapid(str)) < 0 )
		ShowError("script:oshops_remove: Map not found!\n");
	else  {
		for( i=0; i < MAX_OSHOPS_BLOCK; i++ ) {
			map[m].organized_shops[i].class_ = 0;
			map[m].organized_shops[i].x = 0;
			map[m].organized_shops[i].y = 0;
			map[m].organized_shops[i].dir = 0;
			map[m].organized_shops[i].amount = 0;
			map[m].organized_shops[i].max = 0;
		}
		map_setmapflag_sub(m, MF_ORGANIZED_SHOPS, false, NULL);
		script_pushint(st,1);
		return SCRIPT_CMD_SUCCESS;
	}
	script_pushint(st,0);
	return SCRIPT_CMD_FAILURE;
}

int buildin_oshops_rellocate(struct script_state* st)
{
	const char *str;
	int m;

	str=script_getstr(st,2);
	if( (m=map_mapname2mapid(str)) < 0 )
		script_pushint(st,0);
	else
		script_pushint(st,organized_shops_rellocate(m));
	return SCRIPT_CMD_SUCCESS;
}

// Atcommand
int atcommand_oshopsreorder(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	int count;
	nullpo_retr(-1,sd);

	count = organized_shops_rellocate(sd->bl.m);
	switch(count) {
		case -2:
			clif_displaymessage(fd, msg_txt(sd,OSHOP_MSG_TXT+2));
			break;
		case -1:
			clif_displaymessage(fd, msg_txt(sd,OSHOP_MSG_TXT+3));
			break;
		case 0:
			clif_displaymessage(fd, msg_txt(sd,OSHOP_MSG_TXT+4));
			break;
		default:
			{
				char output[128];
				sprintf(output, msg_txt(sd,OSHOP_MSG_TXT+5), count);
				clif_displaymessage(fd, output);
				return 0;
			}
			break;
	}
	return -1;
}
#endif

#ifdef BIND_CLOCK_ENABLE
// Bind Clock

struct npc_bind_clock_list npc_bind_clock[MAX_BIND_CLOCK];

// Map Functions
//
int npc_event_bind_clock(const char* timestr)
{
	int i, c = 0;

	for (i = 0; i < MAX_BIND_CLOCK; i++)
	{
		if (strcmp(npc_bind_clock[i].clock, timestr) == 0)
			c += npc_event_do(npc_bind_clock[i].event);
	}
	return c;
}

bool npc_create_bind_clock(int hour, int minute, const char* ev)
{
	char strtimer[5];
	int i;

	sprintf(strtimer, "%02d%02d", hour, minute);
	ARR_FIND(0, MAX_BIND_CLOCK, i, strcmp(npc_bind_clock[i].clock, strtimer) == 0 && strcmp(npc_bind_clock[i].event, ev) == 0);
	if (i < MAX_BIND_CLOCK)
		return 0; // It is already in use.
	else {
		ARR_FIND(0, MAX_BIND_CLOCK, i, npc_bind_clock[i].clock[0] == '\0');
		if (i < MAX_BIND_CLOCK) {
			safestrncpy(npc_bind_clock[i].clock, strtimer, 5);
			safestrncpy(npc_bind_clock[i].event, ev, EVENT_NAME_LENGTH);
		}
		return 1;
	}
	return 0;
}

bool npc_remove_bind_clock(int hour, int minute, const char* ev)
{
	char strtimer[5];
	int i;

	sprintf(strtimer, "%02d%02d", hour, minute);
	ARR_FIND(0, MAX_BIND_CLOCK, i, strcmp(npc_bind_clock[i].clock, strtimer) == 0 && strcmp(npc_bind_clock[i].event, ev) == 0);
	if (i < MAX_BIND_CLOCK)
	{
		safestrncpy(npc_bind_clock[i].clock, "", 5);
		safestrncpy(npc_bind_clock[i].event, "", EVENT_NAME_LENGTH);
		return 1;
	}
	return 0;
}

bool npc_check_bind_clock(int hour, int minute, const char* ev)
{
	char strtimer[5];
	int i;

	sprintf(strtimer, "%02d%02d", hour, minute);
	ARR_FIND(0, MAX_BIND_CLOCK, i, strcmp(npc_bind_clock[i].clock, strtimer) == 0 && strcmp(npc_bind_clock[i].event, ev));
	if (i < MAX_BIND_CLOCK)
		return 1;
	return 0;
}

void npc_clear_bind_clock(void)
{
	int i = 0;

	for (i = 0; i < MAX_BIND_CLOCK; i++)
	{
		safestrncpy(npc_bind_clock[i].clock, "", 5);
		safestrncpy(npc_bind_clock[i].event, "", EVENT_NAME_LENGTH);
	}
}

// Script Command
int buildin_bindclock(struct script_state* st)
{
	const char* timer = script_getstr(st,2);
	const char* ev = script_getstr(st,3);
	int hour, minute;

	sscanf(timer, "%d:%d", &hour, &minute);

	if( hour < 0 || hour > 23 )
	{
		ShowError("script_bind_clock: Time '%d' incorrect. The time should be between 00~24.\n", hour);
		script_pushint(st,0);
		return SCRIPT_CMD_FAILURE;
	}

	if( minute < 0 || minute > 59 )
	{
		ShowError("script_bind_clock: Minute '%d' incorrect. The minute should be between 00~59.\n", minute);
		script_pushint(st,0);
		return SCRIPT_CMD_FAILURE;
	}

	check_event(st, ev);
	script_pushint(st, npc_create_bind_clock(hour,minute,ev) != 0);
	return SCRIPT_CMD_FAILURE;
}

int buildin_unbindclock(struct script_state* st)
{
	const char* timer = script_getstr(st,2);
	const char* ev = script_getstr(st,3);
	int hour, minute;

	sscanf(timer, "%d:%d", &hour, &minute);

	if( hour < 0 || hour > 23 )
	{
		ShowError("script_unbind_clock: Time '%d' incorrect. The time should be between 00~24.\n", hour);
		script_pushint(st,0);
		return SCRIPT_CMD_FAILURE;
	}

	if( minute < 0 || minute> 59 )
	{
		ShowError("script_unbind_clock: Minute '%d' incorrect. The minute should be between 00~59.\n", minute);
		script_pushint(st,0);
		return SCRIPT_CMD_FAILURE;
	}
	script_pushint(st, npc_remove_bind_clock(hour,minute,ev) != 0);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_checkbindclock(struct script_state* st)
{
	const char* timer = script_getstr(st,2);
	const char* ev = script_getstr(st,3);
	int hour, minute;

	sscanf(timer, "%d:%d", &hour, &minute);

	if( hour < 0 || hour > 23 )
	{
		ShowError("script_unbind_clock: Time '%d' incorrect. The time should be between 00~24.\n", hour);
		script_pushint(st,0);
		return SCRIPT_CMD_FAILURE;
	}

	if( minute < 0 || minute> 59 )
	{
		ShowError("script_unbind_clock: Minute '%d' incorrect. The minute should be between 00~59.\n", minute);
		script_pushint(st,0);
		return SCRIPT_CMD_FAILURE;
	}
	script_pushint(st, npc_check_bind_clock(hour,minute,ev) != 0);
	return SCRIPT_CMD_SUCCESS;
}
#endif

#ifdef AREALOOT_ENABLE
// Arealoot

// Map Functions
int pc_arealoot_item(struct map_session_data *sd, struct flooritem_data *fitem)
{
	int range = battle_config.arealoot_range;

	if( range > AREA_SIZE )
		range = AREA_SIZE;

	if (sd->area_loot.active && sd->area_loot.in_progress == false) {
		sd->area_loot.in_progress = true;
		map_foreachinrange(skill_greed, &fitem->bl, range, BL_ITEM, &sd->bl);
		sd->area_loot.in_progress = false;
	}
	return 1;
}

// Atcommand
int atcommand_arealoot(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	nullpo_retr(-1, sd);

	if (sd->area_loot.active) {
		sd->area_loot.active = false;
		clif_displaymessage(fd, msg_txt(sd,AREALOOT_MSG_TXT));
		return 0;
	}
	sd->area_loot.active = true;
	clif_displaymessage(fd, msg_txt(sd,AREALOOT_MSG_TXT+1));
	return 0;
}
#endif

#ifdef CREATIVE_SCRIPTCMD_ENABLE
// Scripts Command
int buildin_getmapxyunit(struct script_state* st)
{
	struct block_list* bl;
	TBL_PC *sd = NULL;
	int64 num;
	int x,y;
	const char *name;
	char prefix;
	char mapname[MAP_NAME_LENGTH];

	bl = map_id2bl(script_getnum(st,2));
	if( bl == NULL ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}
	if( !data_isreference(script_getdata(st,3)) )
		ShowWarning("script: buildin_getmapxyunit: not mapname variable\n");
	if( !data_isreference(script_getdata(st,4)) )
		ShowWarning("script: buildin_getmapxyunit: not mapx variable\n");
	if( !data_isreference(script_getdata(st,5)) )
		ShowWarning("script: buildin_getmapxyunit: not mapy variable\n");
	else {
		x = bl->x;
		y = bl->y;
		safestrncpy(mapname, map[bl->m].name, MAP_NAME_LENGTH);

		//Set MapName$
		num=st->stack->stack_data[st->start+3].u.num;
		name=get_str(num&0x00ffffff);
		prefix=*name;

		if(not_server_variable(prefix))
			script_rid2sd(sd);
		else
			sd=NULL;
		set_reg(st,sd,num,name,(void*)mapname,script_getref(st,3));

		//Set MapX
		num=st->stack->stack_data[st->start+4].u.num;
		name=get_str(num&0x00ffffff);
		prefix=*name;

		if(not_server_variable(prefix))
			script_rid2sd(sd);
		else
			sd=NULL;
		set_reg(st,sd,num,name,(void*)__64BPRTSIZE(x),script_getref(st,4));

		//Set MapY
		num=st->stack->stack_data[st->start+5].u.num;
		name=get_str(num&0x00ffffff);
		prefix=*name;

		if(not_server_variable(prefix))
			script_rid2sd(sd);

		set_reg(st,sd,num,name,(void*)__64BPRTSIZE(y),script_getref(st,5));

		//Return Success value
		script_pushint(st,1);
		return SCRIPT_CMD_SUCCESS;
	}
	script_pushint(st,0);
	return SCRIPT_CMD_FAILURE;
}

int buildin_alive(struct script_state* st)
{
	TBL_PC *sd;

	if( script_hasdata(st,2) )
		sd = map_nick2sd(script_getstr(st,2),false);
	else
		script_rid2sd(sd);

	if( sd == NULL || !status_revive(&sd->bl, 100, 100) )
		script_pushint(st,0);
	else
		script_pushint(st,1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_countguildmap(struct script_state* st)
{
	int *guild_id;
	const char *mapname;
	int m, i, size, count=0;
	struct s_mapiterator *iter = NULL;
	struct map_session_data *sd;

	mapname = script_getstr(st,2);
	if( (m = map_mapname2mapid(mapname)) < 0 || (size = map[m].users) <= 0 ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	guild_id = (int*)aMalloc(size*12+1);
	iter = mapit_getallusers();
	for( sd = (TBL_PC*)mapit_first(iter); mapit_exists(iter); sd = (TBL_PC*)mapit_next(iter) )
	{
		struct guild *g;
		if( sd->bl.m != m )
			continue;

		g = guild_search(sd->status.guild_id);
		if( g == NULL )
			continue;

		ARR_FIND(0, size, i, guild_id[i]==sd->status.guild_id);
		if( i < size )
			continue;

		guild_id[count] = sd->status.guild_id;
		count++;
	}
	mapit_free(iter);
	script_pushint(st,count);
	aFree(guild_id);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_getcharisdead(struct script_state* st)
{
	TBL_PC *sd;

	if (script_hasdata(st, 2))
		sd = map_nick2sd(script_getstr(st, 2), true);
	else
		script_rid2sd(sd);

	if (sd == NULL)
	{
		script_pushint(st, -1);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, sd->state.dead_sit);
	return SCRIPT_CMD_SUCCESS;
}

static int buildin_getplayersarea_sub(struct block_list *bl, va_list ap)
{
	struct map_session_data *sd = (struct map_session_data *)bl;
	struct script_state* st = va_arg(ap, struct script_state*);
	int j;

	if (sd == NULL || st == NULL)
		return SCRIPT_CMD_SUCCESS;

	j = script_array_highest_key(st, NULL, ".@area_players_cid", NULL);
	set_reg(st, NULL, reference_uid(add_str(".@area_players_cid"), j), ".@area_players_cid", (void*)__64BPRTSIZE(sd->status.char_id), NULL);
	set_reg(st, NULL, reference_uid(add_str(".@area_players_aid"), j), ".@area_players_aid", (void*)__64BPRTSIZE(sd->status.account_id), NULL);
	set_reg(st, NULL, reference_uid(add_str(".@area_players_name$"), j), ".@area_players_name$", (void *)__64BPRTSIZE(sd->status.name), NULL);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_getplayersarea(struct script_state* st)
{
	const char *str;
	int16 m, x1, y1, x2, y2, count = 0;
	int i;

	str = script_getstr(st, 2);
	x1 = script_getnum(st, 3);
	y1 = script_getnum(st, 4);
	x2 = script_getnum(st, 5);
	y2 = script_getnum(st, 6);

	if ((m = map_mapname2mapid(str)) < 0) {
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	// Clean up any leftovers that weren't overwritten
	count = script_array_highest_key(st, NULL, ".@area_players_cid", NULL);
	for (i = 0; i < count; i++) {
		set_reg(st, NULL, reference_uid(add_str(".@area_players_cid"), i), ".@area_players_cid", (void*)0, NULL);
		set_reg(st, NULL, reference_uid(add_str(".@area_players_aid"), i), ".@area_players_aid", (void*)0, NULL);
		set_reg(st, NULL, reference_uid(add_str(".@area_players_name$"), i), ".@area_players_name$", (void *)"", NULL);
	}

	// Enter new values
	map_foreachinallarea(buildin_getplayersarea_sub, m, x1, y1, x2, y2, BL_PC, st);
	count = script_array_highest_key(st, NULL, ".@area_players_cid", NULL);
	set_reg(st, NULL, add_str(".@area_players_count"), ".@area_players_count", (void*)__64BPRTSIZE(count), NULL);
	script_pushint(st, count);
	return SCRIPT_CMD_SUCCESS;
}

static int buildin_getplayersrange_sub(struct block_list *bl, va_list ap)
{
	struct map_session_data *sd = (struct map_session_data *)bl;
	struct script_state* st = va_arg(ap, struct script_state*);
	int j;

	if (sd == NULL || st == NULL)
		return SCRIPT_CMD_SUCCESS;

	j = script_array_highest_key(st, NULL, ".@area_players_cid", NULL);
	set_reg(st, NULL, reference_uid(add_str(".@area_players_cid"), j), ".@area_players_cid", (void*)__64BPRTSIZE(sd->status.char_id), NULL);
	set_reg(st, NULL, reference_uid(add_str(".@area_players_aid"), j), ".@area_players_aid", (void*)__64BPRTSIZE(sd->status.account_id), NULL);
	set_reg(st, NULL, reference_uid(add_str(".@area_players_name$"), j), ".@area_players_name$", (void *)__64BPRTSIZE(sd->status.name), NULL);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_getplayersrange(struct script_state* st)
{
	const char *str;
	int16 m, x, y, range, count = 0;
	int i;

	str = script_getstr(st,2);
	x = script_getnum(st,3);
	y = script_getnum(st,4);
	range = script_getnum(st,5);

	if ((m = map_mapname2mapid(str)) < 0) {
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	// Clean up any leftovers that weren't overwritten
	count = script_array_highest_key(st, NULL, ".@area_players_cid", NULL);
	for (i = 0; i < count; i++) {
		set_reg(st, NULL, reference_uid(add_str(".@area_players_cid"), i), ".@area_players_cid", (void*)0, NULL);
		set_reg(st, NULL, reference_uid(add_str(".@area_players_aid"), i), ".@area_players_aid", (void*)0, NULL);
		set_reg(st, NULL, reference_uid(add_str(".@area_players_name$"), i), ".@area_players_name$", (void *)"", NULL);
	}

	// Enter new values
	map_foreachinallrange2(buildin_getplayersrange_sub, m, x, y, range, BL_PC, st);
	count = script_array_highest_key(st, NULL, ".@area_players_cid", NULL);
	set_reg(st, NULL, add_str(".@area_players_count"), ".@area_players_count", (void*)__64BPRTSIZE(count), NULL);
	script_pushint(st, count);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_setlangtype(struct script_state *st)
{
	TBL_PC *sd;
	int lang = script_getnum(st,2);

	if( script_hasdata(st,3) )
		sd = map_nick2sd(script_getstr(st,3),false);
	else
		script_rid2sd(sd);

	if( msg_checklangtype(lang,false) == 1 ){ //Verify it's enabled and set it
		char output[100];
		pc_setaccountreg(sd, add_str(LANGTYPE_VAR), lang); //For login/char
		sd->langtype = lang;
		sprintf(output,msg_txt(sd,461),msg_langtype2langstr(lang)); // Language is now set to %s.
		script_pushint(st, 1);
	} else if (lang != -1) { //defined langage but failed check
		script_pushint(st, 0);
	}
	return SCRIPT_CMD_SUCCESS;
}

int buildin_save_all_char(struct script_state* st)
{
	struct map_session_data *pl_sd = NULL;
	struct s_mapiterator *iter = NULL;

	iter = mapit_getallusers();
	for (pl_sd = (TBL_PC*)mapit_first(iter); mapit_exists(iter); pl_sd = (TBL_PC*)mapit_next(iter))	{
		chrif_save(pl_sd, CSAVE_NORMAL);
	}
	mapit_free(iter);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_getusersonline(struct script_state* st)
{
	struct map_session_data *sd = NULL;
	struct s_mapiterator *iter = NULL;
	int j = 0;

	iter = mapit_getallusers();
	for (sd = (TBL_PC*)mapit_first(iter); mapit_exists(iter); sd = (TBL_PC*)mapit_next(iter)) {
		set_reg(st, NULL, reference_uid(add_str(".@users_id"), j), ".@users_id", (void*)__64BPRTSIZE(sd->status.char_id), NULL);
		set_reg(st, NULL, reference_uid(add_str(".@users_aid"), j), ".@users_aid", (void*)__64BPRTSIZE(sd->status.account_id), NULL);
		set_reg(st, NULL, reference_uid(add_str(".@users_name$"), j), ".@users_name$", (void *)__64BPRTSIZE(sd->status.name), NULL);
		j++;
	}
	mapit_free(iter);

	set_reg(st, NULL, add_str(".@users_count"), ".@users_count", (void*)__64BPRTSIZE(j), NULL);
	script_pushint(st, j);
	return SCRIPT_CMD_SUCCESS;
}
#endif

#ifdef CREATIVE_ATCMD_ENABLE
// Atcommand
int atcommand_woemanager(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("#woe_castle_manager");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, 1387));
		return -1;
	}
	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_vipcontrol(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("Vip Control#manager");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, 1387));
		return -1;
	}
	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_vipcontrolreload(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("Vip Control#controller");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, 1387));
		return -1;
	}
	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_viptimer(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("Vip Control#display");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, 1387));
		return -1;
	}
	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_cashcontrol(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("Cash Control#manager");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, 1387));
		return -1;
	}
	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_cashinfo(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("Cash Control#main");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, 1387));
		return -1;
	}
	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_presence(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("Presence#display");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, 1387));
		return -1;
	}
	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_bepresence(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("Presence#mark");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd, 1387));
		return -1;
	}
	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}

int atcommand_goevent(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	nullpo_retr(-1, sd);

	if (pc_setpos(sd, mapindex_name2id(EVENTROOM_MAP), EVENTROOM_X, EVENTROOM_Y, CLR_OUTSIGHT))
		clif_displaymessage(fd, msg_txt(sd, EVENTROOM_MSG_TXT+1));
	return 0;
}

int atcommand_evmanager(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	struct npc_data *nd = npc_name2id("Event#manager");

	if (nd == NULL)
	{
		clif_displaymessage(sd->fd, msg_txt(sd,1387));
		return -1;
	}
	run_script(nd->u.scr.script, 0, sd->bl.id, nd->bl.id);
	return 0;
}
#endif

#ifdef PET_EXTENDED_ENABLE
// Pet Extended

bool pet_set_autofeed(struct map_session_data *sd, bool flag)
{
	nullpo_retr(false, sd);

	bool allowed = battle_config.feature_pet_auto_feed && sd->pd && (battle_config.feature_pet_auto_feed_force || sd->pd->get_pet_db()->allow_autofeed);

	if (allowed) {
		sd->pet_auto_feed = flag;
	}

	return sd->pet_auto_feed;
}

void pet_autofeed(struct map_session_data *sd)
{
	struct pet_data *pd;
	short idx;

	pd = sd->pd;

	if (sd->pet_auto_feed && pd->pet.hungry < 90 &&
		(idx = pc_search_inventory(sd, sd->pd->get_pet_db()->FoodID)) != -1) {
		pet_food(sd, sd->pd);
	}
}

bool pet_evolution_check_req(struct map_session_data *sd)
{
	struct pet_data *pd;
	int i, c;

	nullpo_retr(false, sd);

	if (sd->pd == nullptr )
		return false;

	pd = sd->pd;

	if( pd->get_pet_db()->evolution.mob_class <= 0 )
		return false;

	c = pd->get_pet_db()->evolution.count;

	for( i=0; i < PET_EVO_MAX_ITEMS; i++ ) {
		unsigned short nameid = pd->get_pet_db()->evolution.nameid[i], amount = pd->get_pet_db()->evolution.amount[i], k = 0;
		int j = 0;

		if( nameid <= 0 || amount <= 0 )
			continue;

		for( int j=0; j < MAX_INVENTORY; j++ ) {
			if( sd->inventory.u.items_inventory[j].nameid == nameid && sd->inventory.u.items_inventory[j].amount > 0 )
				k += sd->inventory.u.items_inventory[j].amount;
		}

		if( k < amount )
			return false;
	}
	return true;
}

int pet_egg_search(struct map_session_data* sd, int pet_id)
{
	for (int i = 0; i < MAX_INVENTORY; i++) {
		if (sd->inventory.u.items_inventory[i].card[0] == CARD0_PET &&
			pet_id == MakeDWord(sd->inventory.u.items_inventory[i].card[1], sd->inventory.u.items_inventory[i].card[2]))
			return i;
	}
	return -1;
}

void pet_evolution_egg_clean(struct map_session_data *sd)
{
	if( sd->pd ) {
		int i = pet_egg_search(sd, sd->pd->pet.pet_id);
		if( i != -1 )
			pc_delitem(sd, i, 1, 0, 0, LOG_TYPE_OTHER);
	}
}

int pet_evolution(struct map_session_data *sd, int16 pet_id)
{
	struct s_pet_db *pd = pet_db(pet_id);
	int i, idx = -1;

	nullpo_ret(sd);

	if( sd->pd == nullptr )
		return POV_UNKNOWN;

	if( !battle_config.feature_petevolution )
		return POV_UNKNOWN;

	if( sd->pd->pet.intimate < 910 )
		return POV_INTIMATE;

	if( sd->pd->pet.equip )
		return POV_INTIMATE;

	if( pd == NULL )
		return POV_RECIPE;

	if( sd->pd->get_pet_db()->evolution.mob_class <= 0 )
		return POV_NOT_EXIST;

	if( !pet_evolution_check_req(sd) )
		return POV_REQUERIMENT;

	for( i=0; i < PET_EVO_MAX_ITEMS; i++ ) {
		int nameid = sd->pd->get_pet_db()->evolution.nameid[i],
			amount = sd->pd->get_pet_db()->evolution.amount[i],
			j = 0;

		if( nameid <= 0 || amount <= 0 )
			continue;

		for( int j=0; j < MAX_INVENTORY; j++ ) {
			if( sd->inventory.u.items_inventory[j].nameid == nameid )
				pc_delitem(sd, j, amount, 0, 0, LOG_TYPE_OTHER);
		}
	}

	idx = pet_egg_search(sd, sd->pd->pet.pet_id);

	if (idx != -1) {
		item tmp_item{};
		uint8 flag = 0;
		tmp_item.nameid = pd->EggID;
		tmp_item.identify = 1;
		tmp_item.card[0] = CARD0_PET;
		tmp_item.card[1] = GetWord(sd->pd->pet.pet_id,0);
		tmp_item.card[2] = GetWord(sd->pd->pet.pet_id,1);
		tmp_item.card[3] = sd->pd->pet.rename_flag;

		pc_delitem(sd, idx, 1, 0, 0, LOG_TYPE_OTHER);
		if ((flag = pc_additem(sd, &tmp_item, 1, LOG_TYPE_OTHER))) {
			clif_additem(sd, 0, 0, flag);
			map_addflooritem(&tmp_item, 1, sd->bl.m, sd->bl.x, sd->bl.y, 0, 0, 0, 0, 0);
		}
	}

	sd->pd->pet.class_ = pet_id;
	sd->pd->pet.egg_id = pd->EggID;
	sd->pd->pet.intimate = pd->intimate;
	if (!sd->pd->pet.rename_flag)
		safestrncpy(sd->pd->pet.name, pd->jname, NAME_LENGTH);
	status_set_viewdata(&sd->pd->bl, pet_id);
	unit_remove_map(&sd->pd->bl, CLR_OUTSIGHT);

	intif_save_petdata(sd->status.account_id, &sd->pd->pet);
	if (save_settings&CHARSAVE_PET)
		chrif_save(sd, CSAVE_INVENTORY);

	if (map_addblock(&sd->pd->bl))
		return POV_SUCCESS;

	clif_spawn(&sd->pd->bl);
	clif_send_petdata(sd, sd->pd, 0, 0);
	clif_send_petdata(sd, sd->pd, 5, battle_config.pet_hair_style);
	clif_pet_equip_area(sd->pd);
	clif_send_petstatus(sd);
	clif_emotion(&sd->bl, ET_BEST);
	clif_specialeffect(&sd->pd->bl, EF_HO_UP, AREA);
	clif_inventorylist(sd);
	return POV_SUCCESS;
}

static bool pet_extended_parse_dbrow(char* str[], int columns, int current)
{
	struct s_pet_db *pet = NULL;
	//struct s_pet_evolution_data evodata;
	struct item_data *id = NULL;
	struct mob_db *mob;
	int i, mob_id = atoi(str[0]),
		mob_evo = atoi(str[1]),
		autofeed = atoi(str[2]);

	if( !(mob = mob_db(mob_id)) ) {
		ShowWarning("pet_extended_parse_dbrow: Monster (id: %d) not found for pet (pet_id: %d) in extended, skipping...\n", mob_id, mob_id);
		return true;
	}

	if( (pet = pet_db(mob_id)) == NULL ) {
		ShowWarning("pet_extended_parse_dbrow: Pet (id: %d) not found in extended, skipping...\n", mob_id);
		return true;
	}

	// Set Autofeed
	pet->allow_autofeed = autofeed;

	// Reset Data Pet Evolution
	pet->evolution.count = 0;
	memset(&pet->evolution, 0, sizeof(pet->evolution));
	memset(&pet->evolution.nameid, 0, sizeof(pet->evolution.nameid));
	memset(&pet->evolution.amount, 0, sizeof(pet->evolution.amount));

	if( mob_evo ) {
		struct s_pet_db *evo_pet;

		if( !(mob = mob_db(mob_evo)) )
			ShowWarning("pet_extended_parse_dbrow: Monster (id: %d) not found in evolution for pet (pet_id: %d), skipping...\n", mob_evo, mob_id);
		else if( (evo_pet = pet_db(mob_evo)) == NULL )
			ShowWarning("pet_extended_parse_dbrow: Pet (id: %d) not found in evolution for pet (pet_id: %d), skipping...\n", mob_evo, mob_id);
		else {
			pet->evolution.mob_class = mob_evo;
			for( i = 3; i < (PET_EVO_MAX_ITEMS*2); i += 2 ) {
				int nameid = atoi(str[i]),
					amount = atoi(str[i+1]),
					j;

				if( nameid <= 0 || amount <= 0 )
					continue;

				ARR_FIND(0, PET_EVO_MAX_ITEMS, j, pet->evolution.nameid[j] == 0);
				if( j >= PET_EVO_MAX_ITEMS )
					break;

				pet->evolution.nameid[j] = nameid;
				pet->evolution.amount[j] = amount;
				pet->evolution.count++;
			}
		}
	}
	return true;
}

void do_init_pet_extended()
{
	char dbsubpath1[256];

	sprintf(dbsubpath1, "%s/creativesd", db_path);
	sv_readdb(dbsubpath1, "pet_extended_db.txt", ',', 3+(PET_EVO_MAX_ITEMS*2), 3+(PET_EVO_MAX_ITEMS*2), -1, &pet_extended_parse_dbrow, false);
}

void do_final_pet_extended()
{
	//empty
}

// Clif

/**
 * Process the pet evolution request (CZ_PET_EVOLUTION)
 * 09fb <packetType>.W <packetLength>.W <evolutionPetEggITID>.W
 */
void clif_parse_pet_evolution(int fd, struct map_session_data *sd)
{
#if PACKETVER > 20141008
	uint16 egg_id = RFIFOW(fd, packet_db[0x9fb].pos[1]);
	struct s_pet_db *pet = pet_db_search(egg_id, PET_EGG);

	if (!pet) {
		clif_pet_evolution_result(fd, POV_NOT_PET_EGG);
		return;
	}

	clif_pet_evolution_result(fd, (enum s_pet_evolution_result)pet_evolution(sd, pet->class_));
#endif
}

void clif_pet_evolution_result(int fd, enum s_pet_evolution_result result) {
#if PACKETVER >= 20141008
	WFIFOHEAD(fd, packet_len(0x9fc));
	WFIFOW(fd, 0) = 0x9fc;
	WFIFOL(fd, 2) = result;
	WFIFOSET(fd, packet_len(0x9fc));
#endif
}

// Script Commands
int buildin_pet_evolution(struct script_state* st)
{
	TBL_PC *sd;
	TBL_PET *pd;
	int pet_id;

	if( !script_charid2sd(2, sd) || !(pd = sd->pd) ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	pet_id = pd->get_pet_db()->evolution.mob_class;

	if( !pet_id ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, (int)pet_evolution(sd, pet_id));
	return SCRIPT_CMD_SUCCESS;
}

int buildin_pet_set_autofeed(struct script_state* st)
{
	TBL_PC *sd;
	int flag = script_getnum(st,2);

	if( !script_charid2sd(3, sd) ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, pet_set_autofeed(sd,flag));
	return SCRIPT_CMD_SUCCESS;
}

int buildin_pet_get_autofeed(struct script_state* st)
{
	TBL_PC *sd;

	if( !script_charid2sd(2, sd) ) {
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	script_pushint(st, sd->pet_auto_feed);
	return SCRIPT_CMD_SUCCESS;
}

// Atcommand
int atcommand_petevolution(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	TBL_PET *pd;
	struct mob_db *mob;
	int result;
	int pet_id;

	nullpo_retr(-1, sd);

	if( !(pd = sd->pd) ) {
		clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+1));
		return -1;
	}

	pet_id = pd->get_pet_db()->evolution.mob_class;

	if( !pet_id || !mobdb_checkid(pet_id) ) {
		clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+6));
		return -1;
	}

	mob = mob_db(pet_id);
	result = (int)pet_evolution(sd, pet_id);
	switch(result)
	{
		default:
			clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT));
			break;
		case POV_NOT_EXIST:
			clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+1));
			break;
		case POV_NOT_PET_EGG:
			clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+2));
			break;
		case POV_REQUERIMENT:
			{
				int i;
				clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+3));
				clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+4));
				sprintf(atcmd_output, msg_txt(sd,PET_EXTENDED_MSG_TXT+7), mob->jname);
				clif_displaymessage(fd, atcmd_output);

				for( i=0; i < PET_EVO_MAX_ITEMS; i++ ) {
					struct item_data *i_data;
					int nameid = pd->get_pet_db()->evolution.nameid[i],
						amount = pd->get_pet_db()->evolution.amount[i];

					if( nameid <= 0 || amount <= 0 )
						continue;

					if ((i_data = itemdb_exists(nameid)) == NULL)
						continue;

					sprintf(atcmd_output, " (%d) %s", amount, i_data->jname);
					clif_displaymessage(fd, atcmd_output);
				}
			}
			break;
		case POV_INTIMATE:
			clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+5));
			break;
		case POV_SUCCESS:
			clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+8));
			return 0;
	}
	return -1;
}

int atcommand_petevolutioninfo(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	TBL_PET *pd;
	struct mob_db *mob;
	int i, pet_id;

	nullpo_retr(-1, sd);

	if( !(pd = sd->pd) ) {
		clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+1));
		return -1;
	}

	pet_id = pd->get_pet_db()->evolution.mob_class;

	if( !pet_id || !mobdb_checkid(pet_id) ) {
		clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+6));
		return -1;
	}

	mob = mob_db(pet_id);
	clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+3));
	clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+4));
	sprintf(atcmd_output, msg_txt(sd,PET_EXTENDED_MSG_TXT+7), mob->jname);
	clif_displaymessage(fd, atcmd_output);

	for( i=0; i < PET_EVO_MAX_ITEMS; i++ ) {
		struct item_data *i_data;
		int nameid = pd->get_pet_db()->evolution.nameid[i],
			amount = pd->get_pet_db()->evolution.amount[i];

		if( nameid <= 0 || amount <= 0 )
			continue;

		if ((i_data = itemdb_exists(nameid)) == NULL)
			continue;

		sprintf(atcmd_output, " (%d) %s", amount, i_data->jname);
		clif_displaymessage(fd, atcmd_output);
	}
	return 0;
}

int atcommand_petautofeed(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	int flag;

	nullpo_retr(-1, sd);

	flag = pet_set_autofeed(sd,(sd->pet_auto_feed?0:1));
	if( flag )
		clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+9));
	else
		clif_displaymessage(fd, msg_txt(sd,PET_EXTENDED_MSG_TXT+10));
	return 0;
}
#endif

#ifdef PACK_GUILD_ENABLE
// Pack Guild

// PC Functions
int pc_remove_items_guild(struct map_session_data *sd, enum packguild_flag flag)
{
	int i, count = 0;

	nullpo_ret(sd);

	// Remove from Inventory
	for(i = 0; i < MAX_INVENTORY; i++) {
		if( sd->inventory.u.items_inventory[i].nameid > 0 && sd->inventory.u.items_inventory[i].amount > 0 && sd->inventory.u.items_inventory[i].card[0] == CARD0_CREATE && MakeDWord(sd->inventory.u.items_inventory[i].card[2], sd->inventory.u.items_inventory[i].card[3]) == battle_config.packguild_reserved_id ) {
			pc_delitem(sd, i, sd->inventory.u.items_inventory[i].amount, 0, 0, LOG_TYPE_BOUND_REMOVAL);
			count++;
		}
	}

	// Remove from Storage
	for(i = 0; i < MAX_STORAGE; i++) {
		if( sd->storage.u.items_storage[i].nameid > 0 && sd->storage.u.items_storage[i].amount > 0 && sd->storage.u.items_storage[i].card[0] == CARD0_CREATE && MakeDWord(sd->storage.u.items_storage[i].card[2], sd->storage.u.items_storage[i].card[3]) == battle_config.packguild_reserved_id ) {
			storage_delitem(sd, &sd->storage, i, sd->storage.u.items_storage[i].amount);
			count++;
		}
	}

	// Remove from Cart
	for(i = 0; i < MAX_CART; i++) {
		if( sd->cart.u.items_cart[i].nameid > 0 && sd->cart.u.items_cart[i].amount > 0 && sd->cart.u.items_cart[i].card[0] == CARD0_CREATE && MakeDWord(sd->cart.u.items_cart[i].card[2], sd->cart.u.items_cart[i].card[3]) == battle_config.packguild_reserved_id ) {
			pc_cart_delitem(sd, i, sd->cart.u.items_cart[i].amount, 0, LOG_TYPE_BOUND_REMOVAL);
			count++;
		}
	}

	if( count ) {
		if( flag != PACKGUILD_SCRIPT ) {
			char output[255];

			if( flag )
				sprintf(output, "%s", msg_txt(sd,PACK_GUILD_MSG_TXT+2));
			else
				sprintf(output, "%s", msg_txt(sd,PACK_GUILD_MSG_TXT+1));

			clif_messagecolor(&sd->bl, color_table[COLOR_RED], output, false, SELF);
		}

		// Save Char
		chrif_save(sd, CSAVE_INVENTORY);
	}

	return count;
}

// Script Command
int buildin_getguilditem(struct script_state* st)
{
	int get_count, i, seconds = 0;
	unsigned short nameid, amount;
	struct item it;
	TBL_PC *sd;
	struct script_data *data;
	unsigned char flag = 0;
	struct item_data *id;

	if (!script_charid2sd(5,sd))
	{	// No player attached.
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	amount = script_getnum(st,3);

	if( script_hasdata(st,4) )
		seconds = script_getnum(st,4);

	if( battle_config.packguild_reserved_id <= 0 ) {
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	data = script_getdata(st, 2);
	get_val(st, data);
	if (data_isstring(data)) {
		const char *name = conv_str(st, data);
		id = itemdb_searchname(name);
		if (id == NULL)
		{	//Failed
			script_pushint(st, 0);
			return SCRIPT_CMD_SUCCESS;
		}
		nameid = id->nameid;
	} else if( data_isint(data) ) {// <item id>
		nameid = conv_num(st,data);
		if( !(id = itemdb_exists(nameid)) ){
			ShowError("buildin_getguilditem: Nonexistant item %d requested.\n", nameid);
			return SCRIPT_CMD_FAILURE; //No item created.
		}
	} else {
		ShowError("buildin_getguilditem: invalid data type for argument #1 (%d).", data->type);
		return SCRIPT_CMD_FAILURE;
	}

	if (!itemdb_exists(nameid))
	{	// Item does not exist.
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	if( itemdb_type(nameid) == IT_PETEGG )
	{	// Pet Item
		script_pushint(st,0);
		return SCRIPT_CMD_SUCCESS;
	}

	memset(&it, 0, sizeof(it));
	it.nameid = nameid;
	it.identify = 1;
	if( seconds ) it.expire_time = (unsigned int)(time(NULL) + seconds);
	it.bound = battle_config.packguild_bound?BOUND_GUILD:BOUND_NONE;
	it.card[0] = CARD0_CREATE;
	it.card[2] = GetWord(battle_config.packguild_reserved_id, 0);
	it.card[3] = GetWord(battle_config.packguild_reserved_id, 1);

	//Check if it's stackable.
	if (!itemdb_isstackable2(id))
		get_count = 1;
	else
		get_count = amount;

	for (i = 0; i < amount; i += get_count)
	{
		if ((flag = pc_additem(sd, &it, get_count, LOG_TYPE_SCRIPT)))
			clif_additem(sd, 0, 0, flag);
	}

	script_pushint(st, flag);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_delguilditem(struct script_state* st)
{
	TBL_PC *sd;
	int i, type = 0;
	unsigned short nameid, qnt = 0, amount = 0;
	struct script_data *data;
	struct item_data *id;


	if (!script_rid2sd(sd))
	{	// No player attached.
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	amount = script_getnum(st,3);

	if( script_hasdata(st,4) )
		type = script_getnum(st,4);

	data = script_getdata(st, 2);
	get_val(st, data);
	if (data_isstring(data)) {
		const char *name = conv_str(st, data);
		id = itemdb_searchname(name);
		if (id == NULL)
		{	//Failed
			script_pushint(st, 0);
			return SCRIPT_CMD_SUCCESS;
		}
		nameid = id->nameid;
	} else if( data_isint(data) ) {// <item id>
		nameid = conv_num(st,data);
		if( !(id = itemdb_exists(nameid)) ){
			ShowError("buildin_delguilditem: Nonexistant item %d requested.\n", nameid);
			return SCRIPT_CMD_FAILURE; //No item created.
		}
	} else {
		ShowError("buildin_delguilditem: invalid data type for argument #1 (%d).", data->type);
		return SCRIPT_CMD_FAILURE;
	}

	switch (type) {
	case 0:
		// Remove from Inventory
		for (i = 0; i < MAX_INVENTORY; i++) {
			int tmp_amount = 0;

			if (qnt >= amount)
				break;

			if (sd->inventory.u.items_inventory[i].nameid == nameid && sd->inventory.u.items_inventory[i].amount > 0 && sd->inventory.u.items_inventory[i].card[0] == CARD0_CREATE && MakeDWord(sd->inventory.u.items_inventory[i].card[2], sd->inventory.u.items_inventory[i].card[3]) == battle_config.packguild_reserved_id) {
				tmp_amount = amount - qnt;
				if( sd->inventory.u.items_inventory[i].amount < tmp_amount )
					tmp_amount = sd->inventory.u.items_inventory[i].amount;

				pc_delitem(sd, i, tmp_amount, 0, 0, LOG_TYPE_SCRIPT);
				qnt += tmp_amount;
			}
		}
		break;
	case 1:
		// Remove from Storage
		for (i = 0; i < MAX_STORAGE; i++) {
			int tmp_amount = 0;

			if (qnt >= amount)
				break;

			if (sd->storage.u.items_storage[i].nameid == nameid && sd->storage.u.items_storage[i].amount > 0 && sd->storage.u.items_storage[i].card[0] == CARD0_CREATE && MakeDWord(sd->storage.u.items_storage[i].card[2], sd->storage.u.items_storage[i].card[3]) == battle_config.packguild_reserved_id) {
				if( sd->storage.u.items_storage[i].amount < tmp_amount )
					tmp_amount = sd->storage.u.items_storage[i].amount;

				tmp_amount = amount - qnt;
				storage_delitem(sd, &sd->storage, i, tmp_amount);
				qnt += tmp_amount;
			}
		}
		break;
	case 2:
		// Remove from Cart
		for (i = 0; i < MAX_CART; i++) {
			int tmp_amount = 0;

			if (qnt >= amount)
				break;

			if (sd->cart.u.items_cart[i].nameid == nameid && sd->cart.u.items_cart[i].amount > 0 && sd->cart.u.items_cart[i].card[0] == CARD0_CREATE && MakeDWord(sd->cart.u.items_cart[i].card[2], sd->cart.u.items_cart[i].card[3]) == battle_config.packguild_reserved_id) {
				if(sd->cart.u.items_cart[i].amount < tmp_amount )
					tmp_amount = sd->cart.u.items_cart[i].amount;

				tmp_amount = amount - qnt;
				pc_cart_delitem(sd, i, sd->cart.u.items_cart[i].amount, 0, LOG_TYPE_SCRIPT);
				qnt += tmp_amount;
			}
		}
		break;
	default:
		script_pushint(st,0);
		return SCRIPT_CMD_FAILURE;
	}

	script_pushint(st, 1);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_countguilditem(struct script_state* st)
{
	TBL_PC *sd;
	int i, type = 0, expire = 2;
	unsigned short nameid, amount = 0;
	struct script_data *data;
	struct item_data *id;

	if (!script_charid2sd(5,sd))
	{	// No player attached.
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	if( script_hasdata(st,3) )
		type = script_getnum(st,3);

	if( script_hasdata(st,4) )
		expire = script_getnum(st,4);

	data = script_getdata(st, 2);
	get_val(st, data);
	if (data_isstring(data)) {
		const char *name = conv_str(st, data);
		id = itemdb_searchname(name);
		if (id == NULL)
		{	//Failed
			script_pushint(st, 0);
			return SCRIPT_CMD_SUCCESS;
		}
		nameid = id->nameid;
	} else if( data_isint(data) ) {// <item id>
		nameid = conv_num(st,data);
		if( !(id = itemdb_exists(nameid)) ){
			ShowError("buildin_countguilditem: Nonexistant item %d requested.\n", nameid);
			return SCRIPT_CMD_FAILURE; //No item created.
		}
	} else {
		ShowError("buildin_countguilditem: invalid data type for argument #1 (%d).", data->type);
		return SCRIPT_CMD_FAILURE;
	}

	switch (type) {
	case 0:
		// In Inventory
		for (i = 0; i < MAX_INVENTORY; i++) {
			if (sd->inventory.u.items_inventory[i].nameid == nameid && sd->inventory.u.items_inventory[i].amount > 0 && sd->inventory.u.items_inventory[i].card[0] == CARD0_CREATE && MakeDWord(sd->inventory.u.items_inventory[i].card[2], sd->inventory.u.items_inventory[i].card[3]) == battle_config.packguild_reserved_id) {
				if (expire == 1 && sd->inventory.u.items_inventory[i].expire_time <= 0 || expire == 0 && sd->inventory.u.items_inventory[i].expire_time > 0 )
					continue;
				amount += sd->inventory.u.items_inventory[i].amount;
			}
		}
		break;
	case 1:
		// In Storage
		for (i = 0; i < MAX_STORAGE; i++) {
			if (sd->storage.u.items_storage[i].nameid == nameid && sd->storage.u.items_storage[i].amount > 0 && sd->storage.u.items_storage[i].card[0] == CARD0_CREATE && MakeDWord(sd->storage.u.items_storage[i].card[2], sd->storage.u.items_storage[i].card[3]) == battle_config.packguild_reserved_id) {
				if (expire == 1 && sd->storage.u.items_storage[i].expire_time <= 0 || expire == 0 && sd->storage.u.items_storage[i].expire_time > 0)
					continue;
				amount += sd->storage.u.items_storage[i].amount;
			}
		}
		break;
	case 2:
		// In Cart
		for (i = 0; i < MAX_CART; i++) {
			if (sd->cart.u.items_cart[i].nameid == nameid && sd->cart.u.items_cart[i].amount > 0 && sd->cart.u.items_cart[i].card[0] == CARD0_CREATE && MakeDWord(sd->cart.u.items_cart[i].card[2], sd->cart.u.items_cart[i].card[3]) == battle_config.packguild_reserved_id) {
				if (expire == 1 && sd->cart.u.items_cart[i].expire_time <= 0 || expire == 0 && sd->cart.u.items_cart[i].expire_time > 0)
					continue;
				amount += sd->cart.u.items_cart[i].amount;
			}
		}
		break;
	default:
		script_pushint(st,0);
		return SCRIPT_CMD_FAILURE;
	}

	script_pushint(st, amount);
	return SCRIPT_CMD_SUCCESS;
}

int buildin_removeguilditems(struct script_state* st)
{
	TBL_PC *sd;
	int flag = 0;

	if (!script_charid2sd(2,sd))
	{	// No player attached.
		script_pushint(st, 0);
		return SCRIPT_CMD_SUCCESS;
	}

	flag = pc_remove_items_guild(sd, PACKGUILD_SCRIPT);
	script_pushint(st, flag);
	return SCRIPT_CMD_SUCCESS;
}

// Atcommand
int atcommand_guilditem(const int fd, struct map_session_data* sd, const char* command, const char* message)
{
	char item_name[100];
	int number = 0, seconds = 0;
	char flag = 0;
	struct item item_tmp;
	struct item_data *item_data[10];
	int get_count, i, j=0;
	char *itemlist;

	nullpo_retr(-1, sd);
	memset(item_name, '\0', sizeof(item_name));

	if (!message || !*message || (
		sscanf(message, "\"%99[^\"]\" %d %11d", item_name, &number, &seconds) < 2 &&
		sscanf(message, "%99s %d %11d", item_name, &number, &seconds) < 2
	)) {
		clif_displaymessage(fd, msg_txt(sd,(PACK_GUILD_MSG_TXT+3))); // Please enter an item name or ID (usage: @getguilditem <item name/ID> <amount> <quantity>).
		return -1;
	}

	if( battle_config.packguild_reserved_id <= 0 ) {
		clif_displaymessage(fd, msg_txt(sd,PACK_GUILD_MSG_TXT+5));
		return 0;
	}

	itemlist = strtok(item_name, ":");
	while (itemlist != NULL && j<10) {
		if ((item_data[j] = itemdb_searchname(itemlist)) == NULL &&
		    (item_data[j] = itemdb_exists(atoi(itemlist))) == NULL){
			clif_displaymessage(fd, msg_txt(sd,19)); // Invalid item ID or name.
			return -1;
		}
		itemlist = strtok(NULL, ":"); //next itemline
		j++;
	}

	if (number <= 0)
		number = 1;
	get_count = number;

	for(j--; j>=0; j--){ //produce items in list
		unsigned short item_id = item_data[j]->nameid;

		//Check if it's stackable.
		if (!itemdb_isstackable2(item_data[j]))
			get_count = 1;

		for (i = 0; i < number; i += get_count) {
			// if not pet egg
			if (!pet_create_egg(sd, item_id)) {
				memset(&item_tmp, 0, sizeof(item_tmp));
				item_tmp.nameid = item_id;
				item_tmp.identify = 1;
				item_tmp.card[0] = CARD0_CREATE;
				item_tmp.card[2] = GetWord(battle_config.packguild_reserved_id, 0);
				item_tmp.card[3] = GetWord(battle_config.packguild_reserved_id, 1);
				item_tmp.bound = battle_config.packguild_bound?BOUND_GUILD:BOUND_NONE;
				if( seconds ) item_tmp.expire_time = (unsigned int)(time(NULL)+seconds);
				if ((flag = pc_additem(sd, &item_tmp, get_count, LOG_TYPE_COMMAND)))
					clif_additem(sd, 0, 0, flag);
			}
		}
	}

	if (flag == 0)
		clif_displaymessage(fd, msg_txt(sd,18)); // Item created.
	return 0;
}
#endif
