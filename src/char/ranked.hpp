#ifndef _RANKED_CHAR_HPP_
#define _RANKED_CHAR_HPP_

#include "../common/cbasetypes.hpp"

// [CreativeSD]: Ranked System
enum ranked_storage {
	RANKED_STORAGE_CHAR,
	RANKED_STORAGE_ACCOUNT
};

bool ranked_load(uint32 id, enum ranked_storage tableswitch, struct ranked_status *rdata);
bool ranked_save(uint32 id, enum ranked_storage tableswitch, struct ranked_status *p, struct ranked_status *cp);

#endif /* _RANKED_CHAR_HPP_ */
