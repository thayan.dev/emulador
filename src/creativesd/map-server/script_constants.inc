#ifdef BG_WARFARE_ENABLE
	// Battleground Warfare
	export_constant(MF_NOBGSKILLCALL);
	export_constant(MF_NOBGRESPAWN);
	export_constant(CELL_NOBATTLEGROUND);
	export_constant(CELL_CHKNOBATTLEGROUND);
#endif

#ifdef STUFF_ITEMS_ENABLE
	// Stuff Items
	export_constant(MF_BG_CONSUME);
	export_constant(MF_GVG_CONSUME);
	export_constant(MF_PVP_CONSUME);
#endif

#ifdef OSHOPS_ENABLE
	// Oshops
	export_constant(MF_ORGANIZED_SHOPS);
#endif

#ifdef TEST_EMPERIUM_ENABLE
	// Test Emperium
	export_constant(MF_TEST_EMPERIUM);
#endif

#ifdef COSTUME_SYSTEM_ENABLE
	// Costume System
	export_constant(MF_COSTUMEON);
	export_constant(MF_COSTUMEOFF);
#endif

#ifdef MF_NOSTORAGE_ENABLE
	// NoStorage/NoGuildStorage
	export_constant(MF_NOSTORAGE);
	export_constant(MF_NOGUILDSTORAGE);
#endif

#ifdef PACK_GUILD_ENABLE
	export_constant(MF_PACKGUILDON);
	export_constant(MF_PACKGUILDOFF);
#endif

#ifdef RESTOCK_SYSTEM_ENABLE
	// Restock System
	export_constant2("MAX_RESTOCK", MAX_RESTOCK);
#endif

#ifdef CELL_PVP_ENABLE
	// [brAthena]: Cell PvP
	export_constant(CELL_PVP);
	export_constant(CELL_CHKPVP);
#endif

	/* refine information types */
#ifdef REFINE_UI_ENABLE
	export_constant(REFINE_MATERIAL_ID);
	export_constant(REFINE_ZENY_COST);
	export_constant(REFINE_BREAKABLE);
#endif

#ifdef PET_EXTENDED_ENABLE
	export_constant(PETINFO_EQUIPID);
	export_constant(PETINFO_EVOCLASS);
	export_constant(PETINFO_EVOREQ);
	export_constant(PETINFO_AUTOFFED);
#endif

	// NPC's
	export_constant2("FAKE_NPC", -1);

	/* [CreativeSD]: Herc Importants Constants */
	// strcharinfo
	export_constant2("PC_NAME", 0);
	export_constant2("PC_PARTY", 1);
	export_constant2("PC_GUILD", 2);
	export_constant2("PC_MAP", 3);
	export_constant2("PC_CLAN", 4);
	// getcharid
	export_constant2("CHAR_ID_CHAR", 0);
	export_constant2("CHAR_ID_PARTY", 1);
	export_constant2("CHAR_ID_GUILD", 2);
	export_constant2("CHAR_ID_ACCOUNT", 3);
	export_constant2("CHAR_ID_BG", 4);
	export_constant2("CHAR_ID_CLAN", 5);
	// Gettime Types
	export_constant2("GETTIME_SECOND", DT_SECOND);
	export_constant2("GETTIME_MINUTE", DT_MINUTE);
	export_constant2("GETTIME_HOUR", DT_HOUR);
	export_constant2("GETTIME_WEEKDAY", DT_DAYOFWEEK);
	export_constant2("GETTIME_DAYOFMONTH", DT_DAYOFMONTH);
	export_constant2("GETTIME_MONTH", DT_MONTH);
	export_constant2("GETTIME_YEAR", DT_YEAR);
	export_constant2("GETTIME_DAYOFYEAR", DT_DAYOFYEAR);
	// gettimer
	export_constant2("TIMER_COUNT", 0);
	export_constant2("TIMER_TICK_NEXT", 1);
	export_constant2("TIMER_TICK_LAST", 2);
