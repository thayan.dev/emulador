que_temsky,48,126,4	script	Reseter Oposi��p	644,{
   cutin "arang01",2;
   mes "[sG Resetter]";
   mes "O que voc� deseja resetar?";
   menu "Resetar Oposi��o",feeling,"Resetar Hatred",hatred;

feeling:
   atcommand "@feelreset";
   mes "Done.";
   close2;
   cutin "",255;
   end;

hatred:
   set PC_HATE_MOB_MOON, 0;
   set PC_HATE_MOB_STAR, 0;
   set PC_HATE_MOB_SUN, 0;
   mes "Done.";
   next;
   mes "[sG Resetter]";
   mes "Hatred ser� reiniciado ao relogar.";
   mes "Deseja relogar agora?";
   menu "Relogar agora",relog,"Relogar depois",later;

relog:
   atcommand "@kick "+strcharinfo(0);
   end;

later:
   next;
   mes "[sG Resetter]";
   mes "Ok, ent�o, mas n�o se esque�a de que as altera��es n�o ter�o efeito at� voc� relogar.";
   close2;
   cutin "",255;
   end;

}